# Alice Fight STG
On a clear summer day what does Alice mean to do by sneaking into Marisa's house?

## Comments from me
Gosh, forgive me for the name "Alice Fight STG" I casually came up with, thanks. If my passion fades I may not update this repo anymore, so I just upload my codes here. Who knows if others would push me in the issues?
And, I am in great need for some sprite drawer and music composer. Probably I will do these myself, if time permits.

*If you, who is looking at this, have any idea on bullet/spellcard design, or discovered some bug, or have something to say about whatever related topic, do not hesitate to contact me through my email 1286482110@qq.com or in the gitlab issues. 中文/日本語/English OK.*

## Environment
Just list here, fyi.
* Unity 2020.3.32f1c1 => C# 8

## Copyright
I see no need to withhold any copyright for myself, but
* the bullets, and sprites in game for fairies and heroines are ZUN's. By his rules I should not have used them. Probably in the future I would draw these myself;
* the Tachies are from dairi and they are free; (Twitter dairi155)