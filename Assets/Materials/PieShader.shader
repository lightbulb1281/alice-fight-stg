Shader "Unlit/PieShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _SpriteRect ("Sprite Rect", Vector) = (0, 0, 1, 1)
        _Angle("Angle", Float) = 0
        _GlobalAlpha("Global Alpha", Range(0, 1)) = 1
    }
    SubShader
    {
        Tags { 
            "Queue"="Transparent"
            "RenderType"="Transparent" 
        }
        LOD 100
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _SpriteRect;
            float _Angle;
            float _GlobalAlpha;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                float x = (i.uv.x - _SpriteRect.x) / (_SpriteRect.z - _SpriteRect.x);
                float y = (i.uv.y - _SpriteRect.y) / (_SpriteRect.w - _SpriteRect.y);
                float theta = atan2(x-0.5, y-0.5);
                if (theta < 0) theta += 6.283185307179586;
                if (theta < _Angle) col.w = 0;
                col.w *= _GlobalAlpha;
                return col;
            }
            ENDCG
        }
    }
}
