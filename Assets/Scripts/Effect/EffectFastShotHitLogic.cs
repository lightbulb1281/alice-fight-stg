using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectFastShotHitLogic : MonoBehaviour
{
    private static readonly int EFFECT_LENGTH = 20;
    private int timer;
    void Start() {
        this.transform.localRotation = Utils.RandomRotation2D();
        this.transform.localScale = new Vector3(2f, 0.7f, 1);
        timer = 0;
    }
    void FixedUpdate() {
        timer++;
        if (timer == EFFECT_LENGTH) Destroy(gameObject);
        this.transform.Rotate(new Vector3(0, 0, 5), Space.Self);
        var r = this.GetComponent<SpriteRenderer>();
        r.color = Utils.ColorAlpha(r.color, Mathf.Lerp(0.8f, 0, (float)timer / EFFECT_LENGTH));
    }
}
