using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectGrazeLogic : MonoBehaviour
{
    float scale;
    Vector2 direction;
    float velocity;
    float rotation;
    uint timer;
    uint length;

    void Start() {
        var r = GameController.random;
        scale = r.Float(0.3f, 0.8f);
        length = (uint)(r.Int(20, 40));
        direction = r.Unit2();
        velocity = r.Float(0.03f, 0.06f);
        rotation = r.Float(-1.5f, 1.5f);
        this.transform.Rotate(new Vector3(0, 0, r.Float(0, 360)), Space.Self);
        timer = 0;
    }

    void FixedUpdate() {
        timer++; float progress = (float)(timer) / length;
        this.transform.localScale = scale * (1-progress) * Vector3.one;
        this.transform.Translate(velocity * direction * (1-Utils.CurveNSin(progress)));
        this.transform.Rotate(new Vector3(0, 0, rotation), Space.Self);
        if (timer == length) Destroy(gameObject); 
    }
}
