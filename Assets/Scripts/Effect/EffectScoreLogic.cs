using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectScoreLogic : MonoBehaviour
{
    private const float ASCENT_DISTANCE = 36*Constants.PixelToUnit;
    private const float DISAPPEAR_TIME = 40;

    private uint score;
    private Color color;
    private Vector2 position;

    private TMPro.TextMeshPro textComponent;

    private uint timer;

    public void SetParameters(Vector2 position, uint score, Color color) {
        this.position = position; this.score = score; this.color = color;
    }

    void Start() {
        this.transform.position = Utils.VectorZ(position, Constants.DepthEffect);
        textComponent = this.GetComponent<TMPro.TextMeshPro>();
        textComponent.text = score.ToString();
        textComponent.color = Utils.ColorAlpha(color, 0.7f);
        timer = 0;
    }

    void FixedUpdate() {
        timer++; if (timer == DISAPPEAR_TIME) Destroy(gameObject);
        else {
            float progress = (float)(timer) / DISAPPEAR_TIME;
            textComponent.color = Utils.ColorAlpha(color, Mathf.Lerp(0.7f, 0, progress));
            this.transform.Translate(Vector3.up * ASCENT_DISTANCE / DISAPPEAR_TIME, Space.World);
        }
    }
}
