using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEraseLogic : MonoBehaviour
{
    private static readonly uint TIME = 20; // frames;
    private Vector3 startPosition;
    private float baseScale, rotation;
    private uint colorID;

    // rotation: radians
    public void SetParameters(Vector2 startPosition, float baseScale, uint colorID, float rotation = 0) {
        this.startPosition = Utils.VectorZ(startPosition, 0);
        this.baseScale = baseScale; this.colorID = colorID; this.rotation = rotation;
    }

    private uint timer;
    void Start() {
        timer = 0;
        this.transform.position = startPosition;
        this.transform.localScale = new Vector3(baseScale, baseScale, 1);
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
        this.GetComponentInChildren<SpriteColorPicker>().SetColor(colorID);
    }

    void FixedUpdate() {
        timer ++;
        float progress = (float)(timer) / TIME;
        float alpha = 1 - progress, scale = (1 + progress) * baseScale;
        this.transform.localScale = new Vector3(scale, scale, 1);
        var renderer = this.GetComponentInChildren<SpriteRenderer>(); renderer.color = Utils.ColorAlpha(renderer.color, alpha);
        if (timer == TIME) Destroy(gameObject);
    }
}
