using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectBreakLogic0 : MonoBehaviour
{
    private static readonly int EFFECT_LENGTH = 20; // frames
    private int timer;
    private uint color;
    public void SetParameters(uint color) {
        this.color = color;
    }
    bool checkChildCount() {
        if (transform.childCount < 2) {
            Debug.LogWarning("Effect Break Logic 0: Does not have 2 children.");
            return false;
        }
        return true;
    }
    void setChildrenShapes(float progress) {
        if (!checkChildCount()) return;
        var ea = transform.GetChild(0); var eb = transform.GetChild(1);
        float alpha = Mathf.Lerp(0.5f, 0, Mathf.Sin(progress * Mathf.PI / 2));
        float scale = Mathf.Lerp(0.6f, 1.5f, Mathf.Sin(progress * Mathf.PI / 2));
        ea.localScale = new Vector3(scale, scale, 1);
        eb.localScale = new Vector3(1.4f*scale, 0.3f, 1);
        var ra = ea.GetComponent<SpriteRenderer>(); ra.color = Utils.ColorAlpha(ra.color, alpha);
        var rb = eb.GetComponent<SpriteRenderer>(); rb.color = Utils.ColorAlpha(rb.color, alpha);
    }
    void Start() {
        if (!checkChildCount()) return;
        var ea = transform.GetChild(0); var eb = transform.GetChild(1);
        ea.transform.localRotation = Utils.RandomRotation2D();
        eb.transform.localRotation = Utils.RandomRotation2D();
        ea.GetComponent<SpriteColorPicker>().SetColor(color);
        eb.GetComponent<SpriteColorPicker>().SetColor(color);
        setChildrenShapes(0);
        timer = 0;
    }
    void FixedUpdate() {
        timer++;
        setChildrenShapes((float)(timer) / EFFECT_LENGTH);
        if (timer == EFFECT_LENGTH) Destroy(gameObject);
    }
}
