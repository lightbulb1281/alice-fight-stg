using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class for a path. The parameter t for At(t) and DirectionAt(t), etc. indicates the length from the startpoint.
/// </summary>
public abstract class Path {

    public abstract float Length();
    public abstract Vector2 At(float t);
    public abstract Vector2 DirectionAt(float t);
    
    public virtual Vector2 Startpoint() {
        return At(0);
    }
    public virtual Vector2 Endpoint() {
        return At(Length());
    }
    public virtual Vector2 EndDirection() {
        return DirectionAt(Length());
    }

    public PiecewisePath AsPiecewise() {
        return new PiecewisePath(this);
    }
}

/// <summary>
/// A line segment, with p1, p2 specified. Very ordinary.
/// </summary>
public sealed class LineSegmentPath : Path {
    private Vector2 p1, p2;
    public LineSegmentPath(Vector2 p1, Vector2 p2) {
        this.p1 = p1; this.p2 = p2;
    }
    public override Vector2 Startpoint() {return p1;}
    public override Vector2 Endpoint() {return p2;}
    public override float Length() {
        return (p2-p1).magnitude;
    }
    public override Vector2 At(float t) {
        return Vector2.Lerp(p1, p2, t / Length());
    }
    public override Vector2 DirectionAt(float t) {
        return (p2-p1).normalized;
    }
    public RayPath AsRay() {return new RayPath(p1, p2-p1);}
}

/// <summary>
/// A straight ray, with origin and direction specified. 
/// Can also be created through LineSegmentPath.AsRay().
/// </summary>
public sealed class RayPath : Path {
    private Vector2 o, dir;
    public RayPath(Vector2 o, Vector2 dir) {
        this.o = o; this.dir = dir.normalized;
    }
    public override Vector2 Startpoint() {return o;}
    public override Vector2 Endpoint() {
        throw new System.NotImplementedException("RaySegmentPath::Endpoint: Ray segment does not have an endpoint.");
    }
    public override float Length() {
        return float.PositiveInfinity;
    }
    public override Vector2 At(float t) {
        return o + dir * t;
    }
    public override Vector2 DirectionAt(float t) {
        return dir;
    }
}

/// <summary>
/// An arc, with center, radius, startAngle and endAngle specified. StartAngle can be greater or less than endAngle.
/// </summary>
public sealed class ArcPath : Path {
    private Vector2 center; 
    private float radius, startAngle, endAngle;
    public ArcPath(Vector2 center, float radius, float startAngle, float endAngle) {
        this.center = center;
        this.radius = radius;
        this.startAngle = startAngle;
        this.endAngle = endAngle;
    }
    public override Vector2 Startpoint() {
        return center + Utils.UnitByAngle(startAngle) * radius;
    }
    public override Vector2 Endpoint() {
        return center + Utils.UnitByAngle(endAngle) * radius;
    }
    public override float Length() {
        return radius * Mathf.Abs(endAngle - startAngle);
    }
    public override Vector2 At(float t) {
        float consumedAngle = t / radius; 
        float angle = startAngle + ((endAngle > startAngle) ? consumedAngle : -consumedAngle);
        return center + Utils.UnitByAngle(angle) * radius;
    }
    public override Vector2 DirectionAt(float t) {
        float consumedAngle = t / radius; 
        float angle = startAngle + ((endAngle > startAngle) ? consumedAngle : -consumedAngle);
        if (endAngle > startAngle) return Utils.UnitByAngle(angle + Mathf.PI / 2);
        return Utils.UnitByAngle(angle - Mathf.PI / 2);
    }
}

public class PiecewisePath: Path {

    private List<Path> pieces;
    private List<float> lengths;
    private void calculateLengths() {
        lengths = pieces.ConvertAll(piece => {return piece.Length();});
        for (int i=1; i<lengths.Count; i++) lengths[i] += lengths[i-1];
    }

    public PiecewisePath(List<Path> pieces) {
        this.pieces = pieces;
        calculateLengths();
    }
    public PiecewisePath(params Path[] p) {
        this.pieces = new List<Path>(p);
        calculateLengths();
    }
    public PiecewisePath() {
        this.pieces = new List<Path>();
        calculateLengths();
    }

    public override Vector2 Startpoint() {
        if (pieces.Count < 1) throw new System.Exception("PiecewisePath::Startpoint: No piece.");
        return pieces[0].Startpoint();
    }
    public override Vector2 Endpoint() {
        if (pieces.Count < 1) throw new System.Exception("PiecewisePath::Endpoint: No piece.");
        return pieces[pieces.Count-1].Endpoint();
    }
    public override float Length() {
        if (pieces.Count < 1) return 0;
        return lengths[lengths.Count-1];
    }
    public (Path, float) PathAt(float t) {
        if (pieces.Count < 1) throw new System.Exception("PiecewisePath::PathAt: No piece.");
        int n = lengths.Count;
        for (int i=0; i<n; i++)
            if (lengths[i] > t || i == n-1) return (pieces[i], t - (i==0 ? 0 : lengths[i-1]));
        throw new System.Exception("PiecewisePath::At: Unreachable code.");
    }
    public override Vector2 At(float t) {
        var p = PathAt(t);
        return p.Item1.At(p.Item2);
    }
    public override Vector2 DirectionAt(float t) {
        var p = PathAt(t);
        return p.Item1.DirectionAt(p.Item2);
    }
    public override Vector2 EndDirection() {
        if (pieces.Count < 1) throw new System.Exception("PiecewisePath::Endpoint: No piece.");
        return pieces[pieces.Count-1].EndDirection();
    }

    public PiecewisePath Extend(Path p) {
        pieces.Add(p);
        lengths.Add(p.Length() + (lengths.Count == 0 ? 0 : lengths[lengths.Count-1]));
        return this;
    }
    public PiecewisePath Advance(float length) {
        var ep = Endpoint(); var ed = EndDirection();
        return Extend(new LineSegmentPath(ep, ep + ed * length));
    }
    public PiecewisePath Advance() {
        var ep = Endpoint(); var ed = EndDirection();
        return Extend(new RayPath(ep, ed));
    }
    public PiecewisePath TurnLeft(float radius, float angle) {
        var ep = Endpoint(); var ed = EndDirection();
        var r = Utils.Rotate(ed, -Mathf.PI / 2) * radius;
        var theta = Utils.Atan2(r);
        ArcPath n = new ArcPath(ep - r, radius, theta, theta + angle);
        return Extend(n);
    }
    public PiecewisePath TurnRight(float radius, float angle) {
        var ep = Endpoint(); var ed = EndDirection();
        var r = Utils.Rotate(ed, Mathf.PI / 2) * radius;
        var theta = Utils.Atan2(r);
        ArcPath n = new ArcPath(ep - r, radius, theta, theta - angle);
        return Extend(n);
    }
    public PiecewisePath Turn(bool left, float radius, float angle) {
        if (left) return TurnLeft(radius, angle);
        else return TurnRight(radius, angle);
    }

}