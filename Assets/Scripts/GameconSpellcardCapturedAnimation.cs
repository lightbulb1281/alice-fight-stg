using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameconSpellcardCapturedAnimation : MonoBehaviour
{
    private const uint TIME = 90;

    public TMPro.TMP_Text uiPrompt;
    public TMPro.TMP_Text uiBonus;
    public TMPro.TMP_Text uiTime;

    private Timer timer;
    private bool shown;

    public void Show(bool captured, uint score, uint timeInFrames) {
        shown = true;
        uiPrompt.GetComponent<UIVisibilityController>().SetTarget(1);
        uiPrompt.text = captured ? "Get Spellcard Bonus" : "Bonus Failed"; // TODO: Localization 
        if (captured) {
            uiBonus.GetComponent<UIVisibilityController>().SetTarget(1);
            uiBonus.text = score.ToString();
        }
        uiTime.GetComponent<UIVisibilityController>().SetTarget(1);
        uiTime.text = string.Format("Time: {0:00.00}s", timeInFrames * Constants.f2s);
        timer.Reset();
    }

    void Start() {
        timer = new Timer(); shown = false;
    }

    void FixedUpdate() {
        if (shown && timer.TickCheck(TIME)) {
            shown = false;
            uiPrompt.GetComponent<UIVisibilityController>().SetTarget(0);
            uiBonus.GetComponent<UIVisibilityController>().SetTarget(0);
            uiTime.GetComponent<UIVisibilityController>().SetTarget(0);
        }
    }
}
