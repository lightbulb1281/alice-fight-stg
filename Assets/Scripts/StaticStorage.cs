using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class StaticStorage  {

    public static StaticStorage Instance = new StaticStorage();

    private float loadingOffsetX, loadingOffsetY;
    private bool isLoading;

    public bool showScoreBoard;
    public bool hasCurrentScore;
    public long currentScore;

    public string practice; 
    public Locale locale;

    private StaticStorage() {

        if (Instance == null) {
            Instance = this;
        } else if (Instance != this) {
            Debug.LogWarning("Storage instance recreated.");
            Instance = this;
        }

        showScoreBoard = false;
        practice = "None"; // "None"
        locale = Locale.FromFilename(PlayerPrefs.GetString("locale", "zh-CN"));

    }

    public static void LoadLocale(string name) {
        Instance.locale = Locale.FromFilename(name);
        Debug.LogFormat("LoadLocale: {0}", name);
    }

    public static void SaveLoadingOffset(float ox, float oy) {
        Instance.loadingOffsetX = ox;
        Instance.loadingOffsetY = oy;
    }

    public static void SaveIsLoading(bool p) {
        Instance.isLoading = p;
    }

    public static Vector2 LoadLoadingOffset() {
        return new Vector2(Instance.loadingOffsetX, Instance.loadingOffsetY);
    }

    public static bool LoadIsLoading() {
        return Instance.isLoading;
    }

    public static string GetLocaleString(string key) {
        return Instance.locale[key];
    }

    public static string GetLocaleName() {
        return Instance.locale.localeName;
    }

}

[Serializable]
public class PlayerdataScoreboard {

    public List<(string, long)> scores;

    public PlayerdataScoreboard() {
        scores = new List<(string, long)>() {
                ("2022.05.10", 100000),
                ("2022.05.09", 90000),
                ("2022.05.08", 80000),
                ("2022.05.07", 70000),
                ("2022.05.06", 60000),
                ("2022.05.05", 50000),
                ("2022.05.04", 40000),
                ("2022.05.03", 30000),
                ("2022.05.02", 20000),
                ("2022.05.01", 10000),
        };
    }

    public void Save(string filename = "scoreboard.dat") {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + filename);
        formatter.Serialize(file, this);
        file.Close();
    }

    public static PlayerdataScoreboard Load(string filename = "scoreboard.dat") {
        string totalPath = Application.persistentDataPath + "/" + filename;
        bool exists = File.Exists(totalPath);
        if (!exists) {
            Debug.Log("No saved scoreboard found.");
            return new PlayerdataScoreboard();
        }
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Open(totalPath, FileMode.Open);
        PlayerdataScoreboard data = (PlayerdataScoreboard)formatter.Deserialize(file);
        file.Close();
        return data;
    }

    public int TryInsertEntry(string name, long score) {
        Debug.Assert(scores.Count == 10);
        for (int i=0; i<10; i++) {
            if (scores[i].Item2 <= score) {
                scores.Insert(i, (name, score));
                scores.RemoveAt(scores.Count-1);
                Save(); return i;
            } 
        }
        return -1;
    }
    
}