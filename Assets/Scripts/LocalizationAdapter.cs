using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;

public class LocalizationAdapter : MonoBehaviour
{

    public string key;
    public Dictionary<string, (TMPro.TMP_FontAsset, Material)> fontDict;
    public bool updateFontWhenAwake = true;
    
    [System.Serializable]
    public struct LocaleFonts {
        public string key;
        public TMPro.TMP_FontAsset font;
        public Material material;
    };
    public LocaleFonts[] fonts;

    void Awake() {
        fontDict = new Dictionary<string, (TMPro.TMP_FontAsset, Material)>();
        foreach (var p in fonts) {
            fontDict[p.key] = (p.font, p.material);
        }
    }

    void updateFont(string locale) {
        var f = fontDict[locale];
        var p = this.GetComponent<TMPro.TMP_Text>();
        if (p) {
            p.font = f.Item1;
            p.fontMaterial = f.Item2;
        }
    }

    void updateFont() {
        string locale = StaticStorage.GetLocaleName();
        updateFont(locale);
    }

    void updateDisplay() {
        if (key != "") {
            var p = this.GetComponent<TMPro.TMP_Text>();
            if (p) {
                var str = StaticStorage.GetLocaleString(key);
                p.text = str;
            }
        }
    }

    public void Reset(string key) {
        this.key = key;
        updateDisplay();
    }

    void OnEnable() {
        if (updateFontWhenAwake)
            updateFont();
        updateDisplay();
    }

    public void Reload() {
        if (updateFontWhenAwake)
            updateFont();
        updateDisplay();
    }

}