using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLeftRightSwitcher : MonoBehaviour
{
    public float horizontalSpeedThreshold = 0.01f;
    private float previousX;
    private bool moving;
    void Start() {
        previousX = transform.position.x;
        moving = false;
    }
    void FixedUpdate() {
        float currentX = transform.position.x;
        if (!moving && Mathf.Abs(currentX - previousX) > horizontalSpeedThreshold) {
            this.GetComponent<Animator>().SetBool("Move", true);
            moving = true; 
            if (currentX > previousX) transform.localScale = new Vector3(-1, 1, 1);
            else transform.localScale = new Vector3(1, 1, 1);
        } else if (moving && Mathf.Abs(currentX - previousX) < horizontalSpeedThreshold) {
            this.GetComponent<Animator>().SetBool("Move", false);
            moving = false;
            transform.localScale = new Vector3(1, 1, 1);
        }
        previousX = currentX;
    }
}
