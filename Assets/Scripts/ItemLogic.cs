using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLogic : MonoBehaviour
{
    private const float THRESHOLD_Y = Constants.StageVisibleY + 8/64f;
    private const float INDICATOR_Y = Constants.StageVisibleY - 8/64f;
    private const float MAX_DROP_SPEED = 2f/64; 
    private const float GRAVITY = 5f/64/120;
    private const float ATTRACT_ACCELERATION = GRAVITY * 5;
    private const float ATTRACT_RADIUS = 1f;
    private const float START_VELOCITY = 2.5f/64;
    private const float AUTOCOLLECT_SPEED = 8f/64;

    public GameObject itemDisplayPrefab;
    public GameObject indicatorDisplayPrefab;
    public ItemType type;

    private GameObject itemDisplay;
    private GameObject indicatorDisplay;
    private Vector3 velocity;
    private bool isAutoCollecting;

    private Transform player;

    public void SetParameters(ItemType type) {
        this.type = type;
    }

    void Awake() {
        velocity = Vector3.up * START_VELOCITY;
        isAutoCollecting = false;
        player = Utils.Player().transform;
    }

    public void NotifyAutoCollect(bool autocollect = true) {
        isAutoCollecting = autocollect;
    }

    void FixedUpdate() {
        if (isAutoCollecting) {
            this.transform.Translate(AUTOCOLLECT_SPEED * Utils.VectorXY(player.position - this.transform.position).normalized);
        } else {
            Vector3 d = Utils.VectorXY(player.position - this.transform.position);
            if (d.magnitude < ATTRACT_RADIUS) velocity = velocity * 0.92f + (velocity.magnitude * 0.08f + ATTRACT_ACCELERATION) * d.normalized; 
            else velocity = new Vector3(velocity.x * 0.9f, velocity.y - GRAVITY, 0);
            if (velocity.y < -MAX_DROP_SPEED) velocity = new Vector3(velocity.x, -MAX_DROP_SPEED, 0);
            this.transform.Translate(velocity);
            if (this.transform.position.y < -Constants.StageMaxY) Destroy(gameObject);
        }
        float y = this.transform.position.y;
        if (y > THRESHOLD_Y) {
            if (itemDisplay) {Destroy(itemDisplay); itemDisplay = null;}
            Vector3 r = new Vector3(this.transform.position.x, INDICATOR_Y, Constants.DepthItem);
            if (!indicatorDisplay) {
                indicatorDisplay = Instantiate(indicatorDisplayPrefab, r, Quaternion.identity, transform);
                indicatorDisplay.GetComponent<SpriteColorPicker>().SetColor((uint)type);
            }
            else {indicatorDisplay.transform.position = r;}
        } else {
            if (indicatorDisplay) {Destroy(indicatorDisplay); indicatorDisplay = null;}
            Vector3 r = Utils.VectorZ(this.transform.position, Constants.DepthItem);
            if (!itemDisplay) {
                itemDisplay = Instantiate(itemDisplayPrefab, r, Quaternion.identity, transform);
                itemDisplay.GetComponent<SpriteColorPicker>().SetColor((uint)type);
            }
            else {itemDisplay.transform.position = r;}
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "PlayerCollectbox") {
            switch (type) {
                case ItemType.Power: { 
                    player.GetComponent<PlayerController>().NotifyCollectedPower(5); 
                    GameController.Instance.score += 20;
                    GameController.Instance.InstantiateEffectScore(Utils.VectorXY(transform.position), 20, new Color(1, 0.2f, 0.2f, 1));
                    GameController.soundPlayer.Play("item");
                    break;
                }
                case ItemType.BigPower: { 
                    player.GetComponent<PlayerController>().NotifyCollectedPower(50); 
                    GameController.Instance.score += 200;
                    GameController.Instance.InstantiateEffectScore(Utils.VectorXY(transform.position), 200, new Color(1, 0.2f, 0.2f, 1));
                    GameController.soundPlayer.Play("item");
                    break;
                }
                case ItemType.Point: { 
                    float y = this.transform.position.y;
                    float t = (y + Constants.StageMaxY) / (Constants.StageItemCollectY + Constants.StageMaxY);
                    int score = isAutoCollecting ? 50000 : Mathf.CeilToInt(Mathf.Lerp(20000, 50000, t));
                    // Debug.LogFormat("ac={0}, y={1}, t={2}", isAutoCollecting, y, t);
                    GameController.Instance.Notify(GameController.Notification.ItemPointCollected, score);
                    Color color = isAutoCollecting ? new Color(1, 1, 0.2f, 1) : Color.white;
                    GameController.Instance.InstantiateEffectScore(Utils.VectorXY(transform.position), (uint)score, color);
                    GameController.soundPlayer.Play("item");
                    break;
                }
                case ItemType.SmallPoint: { 
                    GameController.Instance.score += 1200;
                    GameController.soundPlayer.Play("item");
                    break;
                }
                case ItemType.Bomb: {
                    var game = GameController.Instance;
                    GameController.soundPlayer.Play("cardget");
                    game.bombs++; break;
                }
                case ItemType.Life: {
                    var game = GameController.Instance;
                    GameController.soundPlayer.Play("extend");
                    game.lives++; break;
                }
            }
            Destroy(gameObject);
        }
    }
}
