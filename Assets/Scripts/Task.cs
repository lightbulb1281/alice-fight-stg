using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// OnStart() should be called, and called only once before invoking any Update().
/// Calling Update() when isFinished is true is undefined. 
/// Clone() should be called before OnStart().
/// isFinished can be used before OnStart() to check if this is a immediate task.
/// </summary>
public abstract class Task {

    public abstract bool isFinished {get;}
    public abstract void OnStart();
    public abstract void Update();

    /// <summary>
    /// For stateless tasks this could return a pointer to self.
    /// For stateful ones this should return a copy.
    /// </summary>
    /// <returns>Cloned task.</returns>
    public abstract Task Clone();

    public SequencedTask AsSequenced() {return new SequencedTask(this);}
    public ParallelTask AsParallel() {return new ParallelTask(this);}
    public RepeatedTask AsRepeated(uint times) {return new RepeatedTask(this, times);}

    public static ParallelTask operator | (Task a, Task b) {
        return new ParallelTask(a, b);
    }
    public static SequencedTask operator +(Task a, Task b) {
        return new SequencedTask(a, b);
    }
    public static RepeatedTask operator *(uint a, Task b) {
        return new RepeatedTask(b, a);
    }

}

/// <summary>
/// isFinished is always true.
/// </summary>
public sealed class ImmediateTask : Task {

    private System.Action execution;

    public ImmediateTask(System.Action f) {
        this.execution = f;
    }

    public sealed override void Update() {
        // Immediate task should do nothing on update.
        // All is executed in OnStart.
    }

    public sealed override void OnStart() {
        execution();
    }

    public sealed override bool isFinished {
        get {return true;}
    }

    public override Task Clone() {
        return this;
    }
}

public sealed class SequencedTask: Task {

    private Queue<Task> tasks;
    public SequencedTask() {tasks = new Queue<Task>();}
    public SequencedTask(Task k) {tasks = new Queue<Task>(); tasks.Enqueue(k);}
    public SequencedTask(params Task[] tasks) {
        this.tasks = new Queue<Task>();
        foreach (var k in tasks) this.tasks.Enqueue(k);
    }

    public sealed override void OnStart() {
        while (true) {
            if (tasks.Count == 0) return;
            var t = tasks.Peek();
            t.OnStart();
            if (t.isFinished) tasks.Dequeue();
            else break;
        } 
    }

    public sealed override void Update() {
        var c = tasks.Peek();
        c.Update(); 
        if (c.isFinished) {
            tasks.Dequeue();
            while (true) {
                if (tasks.Count == 0) break;
                var t = tasks.Peek();
                t.OnStart();
                if (t.isFinished) tasks.Dequeue();
                else break;
            }
        }
    }

    public SequencedTask Append(Task k) {
        tasks.Enqueue(k); return this;
    }

    public override bool isFinished {
        get {return tasks.Count == 0;}
    }

    public override Task Clone() {
        List<Task> copied = new List<Task>();
        foreach (var k in tasks) copied.Add(k.Clone());
        return new SequencedTask(copied.ToArray());
    }

    public static SequencedTask operator +(SequencedTask a, Task b) {
        return a.Append(b);
    }

}

public sealed class ParallelTask: Task {
    
    private List<Task> tasks;
    public ParallelTask() {tasks = new List<Task>();}
    public ParallelTask(Task k) {tasks = new List<Task>{k};}
    public ParallelTask(params Task[] tasks) {
        this.tasks = new List<Task>(tasks);
    }

    public override void OnStart() {
        foreach (var k in tasks) k.OnStart();
    }

    public override void Update() {
        foreach (var k in tasks) {
            if (!k.isFinished) k.Update();
        }
    }

    public override bool isFinished {
        get {
            bool f = true; 
            foreach (var k in tasks) if (!k.isFinished) f = false;
            return f;
        }
    }

    public ParallelTask Parallel(Task k) {
        tasks.Add(k); return this;
    }

    public override Task Clone() {
        List<Task> copied = new List<Task>();
        foreach (var k in tasks) copied.Add(k.Clone());
        return new ParallelTask(copied.ToArray());
    }

    public static ParallelTask operator | (ParallelTask a, Task b) {
        return a.Parallel(b);
    }

}

/// <summary>
/// Times==0 means repeat for infinite times.
/// </summary>
public sealed class RepeatedTask: Task {
    private Task taskTemplate; 
    private Task taskInstance;
    private uint c, times; 

    public RepeatedTask(Task t, uint times) {
        if (times == 0 && t.isFinished) {
            throw new System.Exception("RepeatedTask::constructor: Immediate task repeated for infinite times.");
        }
        taskTemplate = t; this.times = times;
    }

    public override void OnStart() {
        c = 0;
        if (taskTemplate.isFinished) {
            for (int i=0; i<times; i++) {
                taskInstance = taskTemplate.Clone();
                taskInstance.OnStart();
            }
        } else {
            taskInstance = taskTemplate.Clone();
            taskInstance.OnStart();
        }
    }

    public override void Update() {
        taskInstance.Update();
        if (taskInstance.isFinished) {
            c++; if (c==times) return;
            taskInstance = taskTemplate.Clone();
            taskInstance.OnStart();
        }
    }

    public override bool isFinished {
        get {
            if (taskTemplate.isFinished) return true;
            if (times == 0) return false;
            return (c==times);
        }
    }

    public override Task Clone() {
        return new RepeatedTask(taskTemplate, times);
    }

}

/// <summary>
/// Derived should implement OnStart(), Move(), isFinished:get.
/// </summary>
public abstract class MotionTask: Task {

    protected Transform subject;
    public MotionTask(Transform subject) {this.subject = subject;}

    public abstract void Move();
    public sealed override void Update() {
        Move();
    }
    
}

/// <summary>
/// Motion along a path. 
/// Derived should implement StartPathCoordiate() (optional, default return 0),
/// UpdatePathCoordinate(), isFinished:get.
/// </summary>
public abstract class PathMotionTask : MotionTask {

    protected Path path;
    public PathMotionTask(Transform subject, Path path): base(subject) {
        this.path = path;
    }

    protected virtual float StartPathCoordinate() {return 0;}
    protected abstract float UpdatePathCoordinate(); 

    public sealed override void OnStart() {
        subject.transform.position = path.At(StartPathCoordinate());
    }

    public sealed override void Move() {
        subject.transform.position = path.At(UpdatePathCoordinate());
    }

}

public sealed class UniformSpeedPathMotionTask: PathMotionTask {

    private float t, v;
    public UniformSpeedPathMotionTask(Transform subject, Path path, float speed):
        base(subject, path) 
    {
        t = 0; v = speed;
    }

    protected override float UpdatePathCoordinate() {
        t += v; return t;
    }

    public override bool isFinished {
        get {return t >= path.Length();}
    }

    public override Task Clone() {
        return new UniformSpeedPathMotionTask(subject, path, v);
    }

}

public sealed class WaitTask : Task {
    Timeout timeout; uint t;
    public WaitTask(uint t) {this.t = t;}
    public override void OnStart() {
        this.timeout = new Timeout(t);
    }
    public override void Update() {
        timeout.Tick();
    }
    public override bool isFinished {get {return timeout.Check();}}
    public override Task Clone() {
        return new WaitTask(t);
    }
}

public sealed class LoopUntilTask : Task {
    System.Func<bool> predicate;
    private System.Action execution;
    public LoopUntilTask(System.Action f, System.Func<bool> p) {this.predicate = p; this.execution = f;}
    public override void OnStart() {
        // do nothing
    }
    public override void Update() {
        execution();
    }
    public override bool isFinished {get {return predicate();}}
    public override Task Clone() {
        return new LoopUntilTask(execution, predicate);
    }
}

public sealed class LoopTask : Task {
    private System.Action execution;
    Timeout timeout; uint t;
    public LoopTask(uint t, System.Action f) {this.t = t; this.execution = f;}
    public override void OnStart() {
        this.timeout = new Timeout(t);
    }
    public override void Update() {
        timeout.Tick(); execution();
    }
    public override bool isFinished {get {return timeout.Check();}}
    public override Task Clone() {
        return new LoopTask(t, execution);
    }
}

public sealed class LoopTimedTask: Task {
    private System.Action<Timeout> execution;
    Timeout timeout; uint t;
    public LoopTimedTask(uint t, System.Action<Timeout> f) {this.t = t; this.execution = f;}
    public override void OnStart() {
        this.timeout = new Timeout(t);
    }
    public override void Update() {
        timeout.Tick(); execution(timeout);
    }
    public override bool isFinished {get {return timeout.Check();}}
    public override Task Clone() {
        return new LoopTimedTask(t, execution);
    }
}

public sealed class LoopInfiniteTask : Task {
    private System.Action execution;
    public LoopInfiniteTask(System.Action f) {this.execution = f;}
    public override void OnStart() {
    }
    public override void Update() {
        execution();
    }
    public override bool isFinished {get {return false;}}
    public override Task Clone() {
        return new LoopInfiniteTask(execution);
    }
}

public sealed class MoveToTrigonometricTask : MotionTask {

    Vector2 from, to; Timeout timer; uint t;
    public MoveToTrigonometricTask(Transform subject, Vector2 to, uint duration) : base(subject) {
        this.t = duration; this.to = to;
    }
    public override void OnStart() {
        this.from = subject.transform.position;
        timer = new Timeout(t);
    }
    public override void Move() {
        timer.Tick(); float progress = timer.Progress();
        subject.transform.position = Vector2.Lerp(from, to, Utils.CurveSin(progress));
    }
    public override bool isFinished {
        get {return timer.Check();}
    }
    public override Task Clone() {
        return new MoveToTrigonometricTask(subject, to, t);
    }
}

public static class TaskUtilities {
    
    public static SequencedTask Wait(this SequencedTask o, uint k) {
        return o.Append(new WaitTask(k));
    }
    public static SequencedTask Destroy(this SequencedTask o, GameObject obj) {
        return o.Append(new ImmediateTask(() => {Object.Destroy(obj);}));
    }
    public static SequencedTask Do(this SequencedTask o, System.Action func) {
        return o.Append(new ImmediateTask(func));
    }
    public static SequencedTask Loop(this SequencedTask o, uint t, System.Action func) {
        return o.Append(new LoopTask(t, func));
    }
    public static SequencedTask Loop(this SequencedTask o, uint t, System.Action<Timeout> func) {
        return o.Append(new LoopTimedTask(t, func));
    }
    public static SequencedTask LoopUntil(this SequencedTask o, System.Action func, System.Func<bool> predicate) {
        return o.Append(new LoopUntilTask(func, predicate));
    }
    public static SequencedTask LoopInfinite(this SequencedTask o, System.Action func) {
        return o.Append(new LoopInfiniteTask(func));
    }
    public static SequencedTask MoveToTrigonometric(this SequencedTask o, Transform transform, Vector2 to, uint duration) {
        return o.Append(new MoveToTrigonometricTask(transform, to, duration));
    }
    public static SequencedTask Repeat(this SequencedTask o, uint times, Task k) {
        return o.Append(new RepeatedTask(k, times));
    }

}