using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameconEnemySpellcardAnimation : MonoBehaviour
{
    private const uint TACHIE_LENGTH = 90;
    private const uint TEXT_SCALE_LENGTH = 30;
    private const uint TEXT_WAIT_LENGTH = 30;
    private const uint TEXT_MOVE_LENGTH = 60;
    private const uint BACKGROUND_FADEIN_LENGTH = 30;
    private const uint SMALLTEXT_SHOW_LENGTH = 15;

    public SpriteRenderer backgroundObject;
    public RectTransform tachieObject; // local x: 192, y: -128, 128
    public RectTransform spellcardTextObject; // local y: -195, 406 x: 375
    public TMPro.TMP_Text bonusTextObject;
    // public TMPro.TMP_Text historyTextObject; // TODO: add history

    private Timer timer;
    private bool playing;

    public void HideImmediate() {
        playing = false;
        backgroundObject.gameObject.SetActive(false);
        tachieObject.gameObject.SetActive(false);
        spellcardTextObject.gameObject.SetActive(false);
        bonusTextObject.gameObject.SetActive(false);
    }

    public void Show(string spellcardName) {
        playing = true; 
        backgroundObject.gameObject.SetActive(true);
        tachieObject.gameObject.SetActive(true);
        spellcardTextObject.gameObject.SetActive(true);
        spellcardTextObject.GetComponentInChildren<TMPro.TMP_Text>().text = spellcardName;
        timer.Reset(); refresh();
    }

    void Start() {
        timer = new Timer(); 
        HideImmediate();
    }

    private void refresh() {
        if (timer <= TACHIE_LENGTH) {
            float progress = timer.Progress(TACHIE_LENGTH);
            float alpha = Mathf.Lerp(0.7f, 0f, Utils.CurveSin(2 * Mathf.Abs(progress - 0.5f)));
            var image = tachieObject.GetComponent<UnityEngine.UI.Image>();
            image.color = new Color(1,1,1,alpha);
            tachieObject.localPosition = new Vector3(192, Mathf.Lerp(-128, 128, progress), 0);
            if (timer.Check(TACHIE_LENGTH)) tachieObject.gameObject.SetActive(false);
        }
        if (timer <= BACKGROUND_FADEIN_LENGTH) {
            float progress = timer.Progress(BACKGROUND_FADEIN_LENGTH);
            backgroundObject.color = Utils.ColorAlpha(backgroundObject.color, progress);
        }
        if (timer <= TEXT_SCALE_LENGTH) {
            float progress = timer.Progress(TEXT_SCALE_LENGTH);
            float scale = 3 - progress * 2, alpha = progress;
            var stripe = spellcardTextObject.GetComponentInChildren<UnityEngine.UI.Image>();
            stripe.color = new Color(1,1,1,alpha);
            var prompt = spellcardTextObject.GetComponentInChildren<TMPro.TMP_Text>();
            prompt.color = new Color(1,1,1,alpha);
            spellcardTextObject.localPosition = new Vector3(375, -195, 0);
            spellcardTextObject.localScale = new Vector3(scale, scale, 1);
        } else if (timer <= TEXT_SCALE_LENGTH + TEXT_WAIT_LENGTH) {
            // do nothing
        } else if (timer <= TEXT_MOVE_LENGTH + TEXT_SCALE_LENGTH + TEXT_WAIT_LENGTH) {
            float progress = timer.SubstractProgress(TEXT_SCALE_LENGTH + TEXT_WAIT_LENGTH, TEXT_MOVE_LENGTH);
            spellcardTextObject.localPosition = new Vector3(375, Mathf.Lerp(-195, 406, Utils.CurveOSin(progress)), 0);
            if (timer.Check(TEXT_MOVE_LENGTH + TEXT_SCALE_LENGTH + TEXT_WAIT_LENGTH)) {
                bonusTextObject.gameObject.SetActive(true);
                bonusTextObject.color = Utils.ColorAlpha(bonusTextObject.color, 0);
                // historyTextObject.gameObject.SetActive(true);
            }
        } else if (timer <= TEXT_MOVE_LENGTH + TEXT_SCALE_LENGTH +  + TEXT_WAIT_LENGTH + SMALLTEXT_SHOW_LENGTH) {
            float progress = timer.SubstractProgress(TEXT_SCALE_LENGTH + TEXT_MOVE_LENGTH + TEXT_WAIT_LENGTH, SMALLTEXT_SHOW_LENGTH);
            bonusTextObject.color = Utils.ColorAlpha(bonusTextObject.color, progress);
        }
        if (timer >= TACHIE_LENGTH && timer >= BACKGROUND_FADEIN_LENGTH &&
            timer >= TEXT_SCALE_LENGTH + TEXT_MOVE_LENGTH + SMALLTEXT_SHOW_LENGTH + TEXT_WAIT_LENGTH) 
        {
            playing = false;
        }
    }

    void FixedUpdate() {
        if (playing)  
        timer.Tick();
        refresh();
    }
}