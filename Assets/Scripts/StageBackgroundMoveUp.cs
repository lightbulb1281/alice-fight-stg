using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageBackgroundMoveUp : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 p = this.transform.position;
        float y = this.transform.position.y;
        y -= 0.02f;
        if (y < -12.5f) y += 12.5f;
        this.transform.position = new Vector3(p.x, y, p.z);
    }
}
