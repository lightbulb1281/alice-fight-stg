using System.Collections;
using System.Collections.Generic;

public static class Constants {

  public const bool StartDebug = false;

  // units and unit conversions

  public const uint SecondToFrame = 60;
  public const float FrameToSecond = 1/60f;
  public const uint s2f = SecondToFrame;
  public const float f2s = FrameToSecond;

  public const float PixelToUnit = 10f/960f;
  public const float UnitToPixel = 960f/10f;
  public const float p2u = PixelToUnit;
  public const float u2p = UnitToPixel;

  public const float PixelsPerSecond = PixelToUnit * FrameToSecond;

  // game constants

  public const float StageVisibleX = 4.0f; // 768 pixels wide
  public const float StageVisibleY = 4.6875f; // 900 pixels wide

  public const float StageWidth = StageVisibleX * 2;
  public const float StageHeight = StageVisibleY * 2;

  public const float StageMaxX = StageVisibleX + 16 * p2u;
  public const float StageMaxY = StageVisibleY + 16 * p2u;

  public const float DepthBullet = -1f;
  public const float DepthShot = 2f;
  public const float DepthPlayer = 0f;
  public const float DepthEnemy = -0.5f;
  public const float DepthItem = -2f;
  public const float DepthBomb = DepthShot;
  public const float DepthEffect = -1f;

  public const float StageItemCollectY = 2.65f;

  public const uint MaxPower = 800;

  // enums

  public enum ColorE16 : uint {
    Black = 0, Grey = Black, Gray = Black,
    DarkRed = 1,
    Red = 2,
    Purple = 3, 
    Magenta = 4,
    DarkBlue = 5,
    Blue = 6,
    DarkCyan = 7, 
    Cyan = 8,
    DarkGreen = 9,
    Green = 10,
    YellowGreen = 11,
    DarkYellow = 12,
    Yellow = 13,
    Orange = 14,
    White = 15
  };

  public enum ColorE8: uint {
    Black = 0, Grey = Black, Gray = Black,
    Red = 1,
    Purple = 2,
    Blue = 3,
    Cyan = 4,
    Green = 5,
    Yellow = 6,
    White = 7
  }

  public enum ColorBreakEffect0 : uint {
    Red = 0, Blue = 1, Yellow = 2, Green = 3
  }

}