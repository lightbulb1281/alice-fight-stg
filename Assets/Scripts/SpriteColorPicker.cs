using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteColorPicker : MonoBehaviour
{
    public Sprite[] sprites; 
    public void SetColor(uint id) {
        var renderer = GetComponent<SpriteRenderer>();
        if (id < sprites.Length) renderer.sprite = sprites[id];
    } 
    public void SetColor(Constants.ColorE16 id) {
        SetColor((uint)(id));
    }
}
