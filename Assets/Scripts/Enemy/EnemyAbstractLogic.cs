using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAbstractLogic : MonoBehaviour
{
    protected int health;
    private bool acceptDamage;
    private Collider2D bodyCollider;

    protected void SetBodyCollider(bool enabled) {
        // print($"bodycollider {enabled}");
        if (bodyCollider) bodyCollider.enabled = enabled;
    }

    protected virtual void Initialize(int health) {
        // Set health
        this.health = health; acceptDamage = true;
        // Enable colliders
        var hitCollider = this.GetComponent<Collider2D>();
        if (hitCollider) hitCollider.enabled = true;
        bodyCollider = this.transform.Find("Body").GetComponent<Collider2D>();
        SetBodyCollider(true);
    }

    public void tryImposeDamage(int damage) {
        if (acceptDamage && health >= 0 && damage > 0) {
            health -= damage;
            OnHealthChanged(-damage);
            if (health < 0) OnHealthDrain();
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D other) {
        var damage = Utils.GetEnterDamageToEnemy(other.tag);
        tryImposeDamage(damage);
    }

    protected void SetAcceptDamage(bool e) {acceptDamage = e;}

    protected virtual void OnTriggerStay2D(Collider2D other) {
        var damage = Utils.GetStayDamageToEnemy(other.tag);
        tryImposeDamage(damage);
    }

    protected virtual void OnHealthChanged(int delta) {
        return;
    }
    
    protected virtual void OnHealthDrain() {
        Destroy(gameObject);
    }
}
