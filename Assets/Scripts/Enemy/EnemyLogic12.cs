using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic12 : EnemyAbstractLogic
{

    private Vector2 pos;
    bool randomized;
    
    public GameObject bulletPrefab; 
    
    private Task task;
    private Timer timer1, timer2;
    Vector2 shootPos; int sCount; float dr, dt; int n;
    SRandom robert;
    bool clockwise;
    uint color;

    public void SetParameters(Vector2 position, bool randomized, uint color, bool clockwise, int n) {
        this.pos = position; this.randomized = randomized;
        robert = new SRandom(G.random.Int());
        this.color = color; this.clockwise = clockwise;
        this.n = n;
    }

    void shoot(float angle, float dist, int wait) {
        Vector2 dir = U.UnitByAngle(angle + Mathf.PI * 2);
        Vector2 p = pos + U.UnitByAngle(angle) * dist;
        Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic1>().SetParameters(
            p, Vector2.zero, dir * 0.001f, 0.04f, U.Atan2(dir) * Mathf.Rad2Deg, 0, 0, 0, (uint)wait, color, color, false, 12 
        );
    }

    void Start() {

        base.Initialize(100);
        SetAcceptDamage(false);

        Task appearTask = new SequencedTask()
            .Do(() => {
                this.transform.position = pos.zed(C.DepthEnemy);
                SetBodyCollider(false);
                timer1 = new Timer();
                this.transform.localScale = Vector3.zero;
            })
            .Loop(24, () => {
                timer1.Tick(); float s = timer1.Progress(24);
                this.transform.localScale = Vector3.one * s;
            })
            .Do(() => {
                this.transform.localScale = Vector3.one;
                SetBodyCollider(true);
            })
            .Wait(240)
            .Do(() => {
                timer1.Reset();
            })
            .Loop(24, () => {
                timer1.Tick(); float s = 1 - timer1.Progress(24);
                this.transform.localScale = Vector3.one * s;
            })
            .Do(() => {
                Destroy(gameObject);
            });
        
        Task shootTask = new SequencedTask()
            .Wait(60)
            .Do(() => {
                dr = 20 * C.p2u; dt = robert.TwoPi(); sCount = 0;
            })
            .Repeat(30, new SequencedTask()
                .Wait(2)
                .Do(() => {
                    G.soundPlayer.Play("bullet0");
                    if (!randomized) {
                        for (int i=0; i<n; i++) {
                            shoot(dt + Mathf.PI * 2 * i / n, dr, 90 - sCount * 2);
                        }
                        dt += (Mathf.PI*2/n*0.52f) * U.Sign(clockwise);
                    } else {
                        for (int i=0; i<n; i++) {
                            shoot(robert.TwoPi(), dr, 90 - sCount * 2);
                        }
                    }
                    dr += 6 * C.p2u;
                    sCount++;
                })
            );

        task = appearTask | shootTask; task.OnStart();
    }

    void FixedUpdate() {
        task.Update();
    }

    protected override void OnHealthDrain() {
        Debug.LogWarning("Enemy 12 should never drain its health.");
        Destroy(gameObject);
    }
}
