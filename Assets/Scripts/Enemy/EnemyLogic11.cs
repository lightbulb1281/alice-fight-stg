using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic11 : EnemyAbstractLogic
{

    private Vector2 pos;
    private uint persistTime;

    private bool spriteSwitched;
    
    public GameObject bulletPrefab; 
    
    private Task task;
    private Timer timer1, timer2;
    Vector2 shootPos; int sCount; float dr;
    bool clockwise;

    public void SetParameters(Vector2 position, uint persistTime, bool clockwise) {
        this.pos = position; this.persistTime = persistTime; this.clockwise = clockwise;
    }

    void shoot(Vector2 dir, float vel) {
        Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
            this.transform.position, vel * dir, 
            (uint)(C.ColorE16.Blue), (uint)(C.ColorE16.Blue), 
            true, 12, U.Atan2(dir) * Mathf.Rad2Deg
        );
    }

    void Start() {

        base.Initialize(100);
        SetAcceptDamage(false);

        Task appearTask = new SequencedTask()
            .Do(() => {
                this.transform.position = pos.zed(C.DepthEnemy);
                SetBodyCollider(false);
                timer1 = new Timer();
                this.transform.localScale = Vector3.zero;
            })
            .Loop(24, () => {
                timer1.Tick(); float s = timer1.Progress(24);
                this.transform.localScale = Vector3.one * s;
            })
            .Do(() => {
                this.transform.localScale = Vector3.one;
                SetBodyCollider(true);
            })
            .Wait(persistTime)
            .Do(() => {
                timer1.Reset();
            })
            .Loop(24, () => {
                timer1.Tick(); float s = 1 - timer1.Progress(24);
                this.transform.localScale = Vector3.one * s;
            })
            .Do(() => {
                Destroy(gameObject);
            });

        Task shootTask = new SequencedTask()
            .Do(() => {sCount = 0; dr = 0;})
            .Repeat(0, new SequencedTask()
                .Wait(9)
                .Do(() => {
                    G.soundPlayer.Play("bullet0");
                    if (sCount > 10) dr += 10 * Mathf.Deg2Rad * U.Sign(clockwise);
                    float v = 0.05f;
                    shoot(U.UnitByAngle(dr), v);
                    shoot(U.UnitByAngle(dr + Mathf.PI / 2), v);
                    shoot(U.UnitByAngle(dr - Mathf.PI / 2), v);
                    shoot(U.UnitByAngle(dr + Mathf.PI), v);
                    sCount++;
                })
            );

        task = appearTask | shootTask; task.OnStart();
    }

    void FixedUpdate() {
        task.Update();
    }

    protected override void OnHealthDrain() {
        Debug.LogWarning("Enemy 11 should never drain its health.");
        Destroy(gameObject);
    }
}
