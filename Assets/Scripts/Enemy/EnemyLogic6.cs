using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic6 : EnemyAbstractLogic {

    private const float SPEED = 0.045f;
    private const int HEALTH = 24000;
    public const uint SHOOT_INTERVAL = 36;

    public GameObject bulletGeneratorPrefab;
    
    private bool moveRightwards;
    private float startY;
    private Timer timer; private Timeout shootTimeout;
    private uint id; 
    private SRandom robert;
    private uint shootOffset;

    public void SetParameters(uint id, float startY, bool moveRightwards, uint shootOffset) {
        this.startY = startY; this.moveRightwards = moveRightwards;
        this.shootOffset = shootOffset;
        this.id = id; 
    }

    void Start() {
        base.Initialize(HEALTH);
        robert = new SRandom($"el6{id}");
        float x = - Constants.StageVisibleX - 30 * C.p2u; 
        if (!moveRightwards) x = -x;
        this.transform.position = new Vector3(x, startY, C.DepthEnemy);
        timer = new Timer();
        U.SetMoveAnimation(this.GetComponentInChildren<Animator>(), moveRightwards);
        // print(shootOffset);
        shootTimeout = new Timeout(); shootTimeout.Reset(shootOffset + 1);
    }

    void FixedUpdate() {
        timer.Tick();
        this.transform.Translate((moveRightwards ? Vector3.right : Vector3.left) * SPEED, Space.World);
        shootTimeout.Tick();
        if (shootTimeout.Check() && Mathf.Abs(this.transform.position.x) < C.StageVisibleX) {
            GameController.soundPlayer.Play("kira1");
            Instantiate(bulletGeneratorPrefab).GetComponent<BulletLogic9>().SetParameters(
                this.transform.position, robert.Float() * Mathf.PI * 2
            );
        }
        if (shootTimeout.Check()) shootTimeout.Reset(SHOOT_INTERVAL);
        if (Mathf.Abs(this.transform.position.x) > C.StageVisibleX + 30 * C.p2u) Destroy(gameObject);
    }

    // protected override void OnTriggerEnter2D(Collider2D other) {}
    // protected override void OnTriggerStay2D(Collider2D other) {}
    // protected override void OnHealthChanged(int delta) {}
    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        GameController.Instance.InstantiateEffectBreak0(this.transform.position, (uint)C.ColorBreakEffect0.Blue);
        G.Instance.InstantiateItem(this.transform.position, ItemType.BigPower);
        base.OnHealthDrain(); // Destroy
    }

}