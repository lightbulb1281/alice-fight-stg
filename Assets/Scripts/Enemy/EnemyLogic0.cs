using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic0 : EnemyAbstractLogic
{
    private static readonly int HEALTH = 150;

    private enum State {
        DESCENDING, ROTATING, MOVING_OUT
    }

    public enum ColorSet {
        BLUE, RED
    }

    private Vector2 startPosition;
    private Vector2 shootTargetPosition;
    private bool moveRightwards;
    private uint shootDelay;
    private float descendDistance;
    private float rotateRadius;
    private float moveSpeed;
    private uint timer;
    private uint shootTimer;
    private ColorSet color;
    private bool shot;
    private ItemType carriedItem;

    private State state;
    private bool spriteSwitched;
    
    public GameObject bulletPrefab; 

    public void SetParameters(Vector2 startPosition, Vector2 shootTargetPosition, bool moveRightwards, uint shootDelay, 
        float descendDistance, float rotateRadius, float moveSpeed, ColorSet color, ItemType carriedItem) 
    {
        this.startPosition = startPosition; this.shootTargetPosition = shootTargetPosition;
        this.moveRightwards = moveRightwards; this.shootDelay = shootDelay;
        this.descendDistance = descendDistance; this.rotateRadius = rotateRadius;
        this.moveSpeed = moveSpeed; this.color = color; this.carriedItem = carriedItem;
    }

    void Start() {
        base.Initialize(HEALTH);
        transform.position = new Vector3(startPosition.x, startPosition.y, Constants.DepthEnemy);
        state = State.DESCENDING;
        shootTimer = 0;
        shot = false; 
    }

    void FixedUpdate() {
        // move
        switch (state) {
            case State.DESCENDING: {
                this.transform.Translate(moveSpeed * Vector3.down);
                if (this.transform.position.y <= startPosition.y - descendDistance) {
                    state = State.ROTATING;
                    this.transform.position = new Vector3(startPosition.x, startPosition.y - descendDistance, Constants.DepthEnemy);
                    timer = 0; spriteSwitched = false;
                }
                break;
            }
            case State.ROTATING: {
                Vector2 rotateCenter = new Vector2(startPosition.x + (moveRightwards ? 1 : (-1)) * rotateRadius, startPosition.y - descendDistance);
                float totalFrames = ((Mathf.PI * descendDistance / 2) / moveSpeed);
                float process = (float)(timer) / totalFrames;
                float angle = process * Mathf.PI / 2;
                if (process >= 1) {
                    this.transform.position = new Vector3(rotateCenter.x + moveSpeed * (timer-totalFrames) * (moveRightwards?1:-1), rotateCenter.y - rotateRadius, Constants.DepthEnemy);
                    state = State.MOVING_OUT;
                } else {
                    Vector2 p = rotateCenter + rotateRadius * new Vector2((moveRightwards ? (-1) : 1)*Mathf.Cos(angle), -Mathf.Sin(angle));
                    this.transform.position = new Vector3(p.x, p.y, Constants.DepthEnemy);
                }
                if (process > 0.25 & !spriteSwitched) {
                    spriteSwitched = true;
                    Utils.SetMoveAnimation(GetComponentInChildren<Animator>(), moveRightwards);
                }
                timer++;
                break;
            }
            case State.MOVING_OUT: {
                this.transform.Translate(moveSpeed * (moveRightwards?Vector3.right:Vector3.left));
                if (Mathf.Abs(this.transform.position.x) > Constants.StageMaxX) Destroy(gameObject);
                break;
            }
        }
        // shoot bullets
        shootTimer++;
        if (shootTimer > shootDelay && !shot) {
            shot = true;
            GameController.soundPlayer.Play("bullet0");
            for (int i=0; i<6; i++) {
                var bullet = Instantiate(bulletPrefab);
                var logic = bullet.GetComponent<BulletSimpleLogic0>();
                var direction = (shootTargetPosition - Utils.VectorXY(transform.position)).normalized;
                uint generationColor = (color==ColorSet.RED) ? 1u : 2u;
                uint bulletColor = (color==ColorSet.RED) ? 2u : 6u;
                logic.SetParameters(
                    Utils.VectorXY(transform.position), // start position
                    direction * (0.07f + i * 0.015f), // velocity
                    generationColor, bulletColor, 
                    false, 12, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg, i==0, 0
                );
            }
        }
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        GameController.Instance.InstantiateEffectBreak0(Utils.VectorXY(this.transform.position), 1);
        GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), carriedItem);
        Destroy(gameObject);
    }
}
