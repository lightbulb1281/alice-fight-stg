using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic5 : EnemyAbstractLogic
{

    private const float PATH_R = 250 * Constants.p2u;
    private const float SPEED = 0.04f;
    private const int HEALTH = 500;

    public GameObject bulletGeneratorPrefab;

    private float startY;
    private bool moveRightwards;

    private float movedLength; 
    private Timer timer;
    private ItemType carriedItem;

    public void SetParameters(float startY, bool moveRightwards, ItemType carriedItem) {
        this.startY = startY; this.moveRightwards = moveRightwards; 
        this.carriedItem = carriedItem;
    }

    private Vector2 placeByLength(float length) {
        float lengthCircle = PATH_R * Mathf.PI * 2 * 60f / 360f;
        if (length < lengthCircle) {
            float angle = length / PATH_R; // radians
            Vector2 displacement = Utils.UnitByTheta(-120*Mathf.Deg2Rad + angle) * PATH_R;
            Vector2 origin = new Vector2(-Constants.StageMaxX + PATH_R / 2, startY + PATH_R * Mathf.Sin(60 * Mathf.Deg2Rad));
            Vector2 d = displacement + origin;
            if (moveRightwards) return d; else return new Vector2(-d.x, d.y);
        } else {
            length -= lengthCircle;
            Vector2 origin = new Vector2(-Constants.StageMaxX  + PATH_R / 2, startY + PATH_R * Mathf.Sin(60 * Mathf.Deg2Rad));
            origin += Utils.UnitByTheta(-60 * Mathf.Deg2Rad) * PATH_R;
            Vector2 d = origin + length * Utils.UnitByTheta((-30 + 60) * Mathf.Deg2Rad);
            if (moveRightwards) return d; else return new Vector2(-d.x, d.y);
        }
    }

    void Start() {
        base.Initialize(HEALTH);
        movedLength = 0;
        this.transform.position = Utils.VectorZ(placeByLength(0), Constants.DepthEnemy);
        timer = new Timer();
        Utils.SetMoveAnimation(GetComponentInChildren<Animator>(), moveRightwards);
    }

    void FixedUpdate() {
        movedLength += SPEED;
        this.transform.position = Utils.VectorZ(placeByLength(movedLength), Constants.DepthEnemy);
        if (this.transform.position.y > Constants.StageMaxY + 20 * Constants.p2u) Destroy(gameObject);
        timer.Tick();
        if (timer.Check(40) || timer.Check(160)) {
            var logic = Instantiate(bulletGeneratorPrefab).GetComponent<BulletLogic7>();
            GameController.soundPlayer.Play("kira1");
            logic.SetParameters(this.transform.position, moveRightwards);
        }
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        uint breakColor = 3u;
        GameController.Instance.InstantiateEffectBreak0(Utils.VectorXY(this.transform.position), breakColor);
        GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), carriedItem);
        base.OnHealthDrain();
    }
}
