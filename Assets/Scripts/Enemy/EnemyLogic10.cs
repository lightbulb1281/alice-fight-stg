using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic10 : EnemyAbstractLogic
{

    private float x;
    int id;

    private bool spriteSwitched;
    
    public GameObject bulletPrefab; 
    
    private Task task;
    private Timer timer1, timer2;
    float rot; float distance; int sCount; 
    Vector2 shootPos; int dt;

    public void SetParameters(float x, int id) {
        this.x = x; this.id = id;
    }

    void shoot(Vector2 dir, float vel) {
        Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
            this.transform.position, vel * dir, 
            (uint)(C.ColorE16.Blue), (uint)(C.ColorE16.Blue), 
            true, 12, U.Atan2(dir) * Mathf.Rad2Deg
        );
    }

    
    private static readonly float[] rots = {20, 10, 5};

    void Start() {

        base.Initialize(100);
        SetAcceptDamage(false);

        Task moveTask = new SequencedTask()
            .Do(() => {
                this.transform.position = new Vector3(x, C.StageVisibleY + 16 * C.p2u, C.DepthEnemy);
                timer1 = new Timer();
            })
            .Wait((uint)(id) * 12)
            .Loop(120, () => {
                timer1.Tick(); float progress = timer1.Progress(120);
                float dy = Mathf.Lerp(16, -90, U.CurveSin(progress)) * C.p2u; 
                this.transform.position = new Vector3(x, C.StageVisibleY + dy, C.DepthEnemy);
            })
            .Wait(840)
            .Do(() => {
                timer1.Reset();
            })
            .Loop(120, () => {
                timer1.Tick(); float progress = timer1.Progress(120);
                float dy = Mathf.Lerp(-90, 32, U.CurveSin(progress)) * C.p2u; 
                this.transform.position = new Vector3(x, C.StageVisibleY + dy, C.DepthEnemy);
            })
            .Do(() => {
                Destroy(gameObject);
            });

        Task shootTask = new SequencedTask()
            .Do(() => {
                timer2 = new Timer(); sCount = 0;
            })
            .Wait(120)
            .Repeat(3, new SequencedTask()
                .Do(() => {
                    timer2.Reset(); dt = (sCount % 2 == 0) ? (5 + id * 15) : (130 + id * -15);
                }).Loop(300, () => {
                    timer2.Tick();
                    for (int i=0; i<20; i++) {
                        if (timer2.Check(dt + i * 2)) {
                            G.soundPlayer.Play("bullet0");
                            if (i==0) {shootPos = G.playerPosition;}
                            Vector2 dir = shootPos - this.transform.position.xy(); dir.Normalize();
                            float vel = (0.04f + i * 0.004f);
                            shoot(dir, vel);
                            shoot(U.Rotate(dir, -rots[sCount] * Mathf.Deg2Rad), vel);
                            shoot(U.Rotate(dir, -rots[sCount] * Mathf.Deg2Rad * 2), vel);
                            shoot(U.Rotate(dir, rots[sCount] * Mathf.Deg2Rad), vel);
                            shoot(U.Rotate(dir, rots[sCount] * Mathf.Deg2Rad * 2), vel);
                        }
                    }
                })
                .Do(() => {sCount++;})
            );

        task = moveTask | shootTask; task.OnStart();
    }

    void FixedUpdate() {
        task.Update();
    }

    protected override void OnHealthDrain() {
        Debug.LogWarning("Enemy 10 should never drain its health.");
        Destroy(gameObject);
    }
}
