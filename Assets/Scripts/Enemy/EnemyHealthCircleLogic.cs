using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthCircleLogic : MonoBehaviour
{
    public GameObject thresholdDotPrefab;
    
    private bool playerNear, shown;
    private List<(float, Transform)> dots;

    private float getTargetAlpha() {
        if (!shown) return 0;
        if (playerNear) return 0.5f;
        return 1;
    }
    
    void Start() {
        var renderer = this.GetComponent<SpriteRenderer>();
        Sprite sprite = renderer.sprite;
        var spriteRect = sprite.rect;
        Vector4 rect = new Vector4(
            (float)(spriteRect.min.x) / sprite.texture.width, 
            (float)(spriteRect.min.y) / sprite.texture.height, 
            (float)(spriteRect.max.x) / sprite.texture.width, 
            (float)(spriteRect.max.y) / sprite.texture.height
        );
        renderer.material.SetVector("_SpriteRect", rect);
        dots = new List<(float, Transform)>();
        shown = playerNear = false;
    }
    public void SetAngle(float angleInRadians) {
        angleInRadians = Mathf.Repeat(angleInRadians, Mathf.PI * 2);
        this.GetComponent<SpriteRenderer>().material.SetFloat("_Angle", angleInRadians);
        for (int i=dots.Count-1; i>=0; i--) {
            if (dots[i].Item1 <= angleInRadians) {Destroy(dots[i].Item2.gameObject); dots.RemoveAt(i);}
        }
    }
    public void SetThresholds(params float[] tsRadians) {
        foreach (var dot in dots) Destroy(dot.Item2.gameObject);
        dots.Clear();
        foreach (var t in tsRadians) {
            Vector3 place = Utils.VectorZ(Utils.UnitByTheta(90*Mathf.Deg2Rad - t), 0) * 1.26f;
            var tf = Instantiate(thresholdDotPrefab, this.transform).transform;
            tf.localPosition = place;
            tf.Rotate(new Vector3(0, 0, 90 - t*Mathf.Rad2Deg), Space.Self);
            dots.Add((t, tf));
        }
        float alpha = this.GetComponent<SpriteVisibilityController>().GetActual();
        SetGlobalAlpha(alpha);
    }
    public void SetThresholdRatios(params float[] tsRatios) {
        int n = tsRatios.Length;
        for (int i=0; i<n; i++) tsRatios[i] = (1-tsRatios[i]) * Mathf.PI * 2;
        SetThresholds(tsRatios);
    }
    public void Show() {
        shown = true; SetGlobalAlpha(getTargetAlpha());
    }
    public void Hide() {
        shown = false; SetGlobalAlpha(getTargetAlpha());
    }
    public void SetPlayerNear(bool f) {
        playerNear = f; SetGlobalAlpha(getTargetAlpha());
    }
    public void SetRatio(float percent) {
        SetAngle((1-percent) * Mathf.PI * 2);
    }
    public void SetGlobalAlpha(float alpha) {
        this.GetComponent<SpriteVisibilityController>().SetTarget(alpha);
        foreach (var dot in dots) dot.Item2.GetComponent<SpriteVisibilityController>().SetTarget(alpha);
    }
    void FixedUpdate() {
        float alpha = this.GetComponent<SpriteVisibilityController>().GetDisplayed();
        var renderer = this.GetComponent<SpriteRenderer>();
        renderer.material.SetFloat("_GlobalAlpha", alpha);
    }
}
