using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic9 : EnemyAbstractLogic
{

    private Vector2 centerPosition;

    private bool spriteSwitched;
    
    public GameObject bulletPrefab; 
    
    private SpriteRenderer spriteRenderer;
    private Task task;
    private Timer timer1;
    float rot; float distance; int sCount;

    public void SetParameters(Vector2 origin, float rot) {
        this.centerPosition = origin; this.rot = rot;
    }

    void Start() {

        base.Initialize(100);
        SetAcceptDamage(false);
        spriteRenderer = this.GetComponentInChildren<SpriteRenderer>();

        Task fadeInTask = new SequencedTask()
            .Do(() => {
                timer1 = new Timer();
                spriteRenderer.color = U.ColorAlpha(spriteRenderer.color, 0);
            })
            .Loop(24, () => {
                timer1.Tick(); float alpha = timer1.Progress(24); 
                spriteRenderer.color = U.ColorAlpha(spriteRenderer.color, alpha);
            });

        Task moveTask = new SequencedTask()
            .Do(() => {
                distance = 0;
                this.transform.position = (centerPosition + U.UnitByAngle(rot) * distance).zed(C.DepthEnemy);
            }).LoopInfinite(() => {
                rot += 0.8f * Mathf.Deg2Rad;
                distance = Mathf.Clamp(distance + 0.02f, 0, 1);
                this.transform.position = (centerPosition + U.UnitByAngle(rot) * distance).zed(C.DepthEnemy);
            });

        // shoot task
        Task shootTask = new SequencedTask()
            .Do(() => {
                sCount = 0;
            })
            .Wait(60)
            .Repeat(3, new SequencedTask()
                .Do(() => {
                    for (int i=0; i<=sCount; i++) {
                        G.soundPlayer.Play("bullet0");
                        Instantiate(bulletPrefab).GetComponent<BulletLogic33>().SetParameters(
                            this.transform.position, 
                            U.UnitByAngle(rot + (90 - i * 10) * Mathf.Deg2Rad) * 0.030f, 
                            3, bulletPrefab
                        );
                    }
                    sCount++;
                })
                .Wait(300)
            )
            .Do(() => {timer1.Reset();})
            .Loop(24, () => {
                timer1.Tick(); float alpha = 1 - timer1.Progress(24);
                spriteRenderer.color = U.ColorAlpha(spriteRenderer.color, alpha);
            }).Do(() => {
                Destroy(gameObject);
            });

        task = fadeInTask | moveTask | shootTask; task.OnStart();
    }

    void FixedUpdate() {
        task.Update();
    }

    protected override void OnHealthDrain() {
        Debug.LogWarning("Enemy 9 should never drain its health.");
        Destroy(gameObject);
    }
}
