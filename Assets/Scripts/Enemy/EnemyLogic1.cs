using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic1 : EnemyAbstractLogic
{
    private const int HEALTH = 450;
    public GameObject bulletPrefab;

    private float startX;
    private float descendSpeed;
    private uint shootDelay;
    private uint shootInterval;

    private uint timer;
    private uint shotCount;
    private ItemType carriedItem;

    public void SetParameters(float startX, float descendSpeed, uint shootDelay, uint shootInterval, ItemType carriedItem) {
        this.startX = startX; this.descendSpeed = descendSpeed;
        this.shootDelay = shootDelay; this.shootInterval = shootInterval;
        this.carriedItem = carriedItem;
    }

    void Start() {
        base.Initialize(HEALTH);
        this.transform.position = new Vector3(startX, 4.9f, Constants.DepthEnemy);
        timer = 0;
        shotCount = 0;
    }
    
    // expansionAngles in degrees.
    void shoot(uint ways, float expansionAngles, uint layers, float maxSpeed, float minSpeed) {
        Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        Vector3 displacement = playerPosition - this.transform.position;
        float angle = Mathf.Atan2(displacement.y, displacement.x);
        expansionAngles = expansionAngles * Mathf.Deg2Rad;
        GameController.soundPlayer.Play("bullet1");
        for (uint i=0; i<ways; i++) {
            float dtheta = expansionAngles * (-0.5f + (float)(i)/(ways-1));
            float theta = angle + dtheta;
            float speedFactor = 1/Mathf.Cos(dtheta);
            for (uint j=0; j<layers; j++) {
                var bullet = Instantiate(bulletPrefab);
                var logic = bullet.GetComponent<BulletSimpleLogic0>();
                var direction = new Vector2(Mathf.Cos(theta), Mathf.Sin(theta));
                var velocity = direction * speedFactor * (minSpeed + (maxSpeed-minSpeed)/(layers-1)*j);
                logic.SetParameters(
                    Utils.VectorXY(transform.position), velocity, 
                    1, 2, false, 12, 0, i+j==0);
            }
        }
    } 

    void FixedUpdate() {
        timer++;
        if (shotCount == 0) {
            if (timer == shootDelay) {
                shoot(3, 16, 3, 0.06f, 0.05f); shotCount++;
                timer = 0;
            }
        } else if (shotCount < 3) {
            if (timer == shootInterval) {
                shoot(3, 16, 3, 0.06f, 0.05f);
                timer = 0;
                shotCount++;
            }
        }
        this.transform.Translate(descendSpeed * Vector3.down);
        if (this.transform.position.y < -(Constants.StageMaxY+0.1)) Destroy(gameObject);
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        GameController.Instance.InstantiateEffectBreak0(
            Utils.VectorXY(this.transform.position), 0);
        GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), carriedItem);
        Destroy(gameObject);
    }
}
