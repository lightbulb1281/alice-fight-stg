using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic4 : EnemyAbstractLogic
{
    private const int HEALTH = 100000;
    private const float START_Y = Constants.StageVisibleY + 36f*Constants.PixelToUnit;
    private const float SHOOT_Y = Constants.StageItemCollectY; 
    private const uint MOVE_TIME = 100;
    private const uint SHOOT_TIME = 600;

    public GameObject bulletPrefab; 

    private enum State {
        Enter, Shoot, Leave
    }
    private float startX;
    private State state;
    private uint timer;
    private uint timerShootInterval;
    private Transform ringEffect; 

    private float sDistance;
    private float sAngle; // rad
    private uint sTimer;

    void initShootVars() {
        sDistance = 0; sTimer = 0;
        sAngle = -90 * Mathf.Deg2Rad;
    }
    void updateShootVars() {
        sTimer += 1; 
        sDistance = Utils.RepeatedSin((float)(sTimer%90)/90) * 0.6f + 0.1f;
        sAngle += -0.3f * Mathf.Deg2Rad;
    }
    void shoot() {
        int n = 16;
        Vector2 o = Utils.VectorXY(this.transform.position);
        for (int i=0; i<n; i++) {
            Vector2 du = Utils.UnitByTheta(sAngle + Mathf.PI * 2 / n * i);
            Vector2 start = o + sDistance * du;
            Vector2 direction = Utils.Rotate(du, 50*Mathf.Deg2Rad);
            var logic = Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>();
            logic.SetParameters(start, direction * 0.04f, 4, 13, true, 12, Utils.Atan2(direction) * Mathf.Rad2Deg, true, 0);
        }
    }

    public void SetParameters(float startX = 0) {
        this.startX = startX;
    }
    
    void Start() {
        base.Initialize(HEALTH);
        base.SetAcceptDamage(false);
        state = State.Enter;
        this.transform.position = new Vector3(startX, START_Y, Constants.DepthEnemy);
        timer = 0;
        ringEffect = transform.Find("RingEffect");
    }

    void FixedUpdate() {
        if (ringEffect) {
            ringEffect.transform.Rotate(new Vector3(0, 0, 1f), Space.Self);
        }
        switch (state) {
            case State.Enter: {
                if (timer == 10) GameController.soundPlayer.Play("prepareshoot");
                timer++; float progress = (float)(timer) / MOVE_TIME;
                this.transform.position = new Vector3(startX, Mathf.Lerp(START_Y, SHOOT_Y, Utils.CurveSin(progress)));
                if (timer == MOVE_TIME) {timer = timerShootInterval = 0; state = State.Shoot; base.SetAcceptDamage(true); initShootVars();}
                break;
            }
            case State.Shoot: {
                if (timer % 15 == 0) GameController.soundPlayer.Play("kira2");
                timer++; float progress = (float)(timer) / SHOOT_TIME; float interval = Mathf.Lerp(10, 3, Utils.CurveSin(progress));
                if (timer == SHOOT_TIME) {timer = 0; state = State.Leave;}
                timerShootInterval++;
                if (timerShootInterval >= interval) {shoot(); timerShootInterval=0;}
                if (timer <= 20) {
                    var renderer = ringEffect.GetComponent<SpriteRenderer>();
                    renderer.color = Utils.ColorAlpha(renderer.color, 1-(float)(timer)/20);
                }
                updateShootVars();
                break;
            }
            case State.Leave: {
                timer++; float progress = (float)(timer) / MOVE_TIME;
                this.transform.position = new Vector3(startX, Mathf.Lerp(SHOOT_Y, START_Y, Utils.CurveNSin(progress)));
                if (timer == MOVE_TIME) {Destroy(gameObject);}
                break;
            }
        }
    }

    protected override void OnHealthChanged(int delta) {
        if (delta < 0) {
            if (health < 30000) GameController.soundPlayer.Play("damage1");
            else GameController.soundPlayer.Play("damage0");
        }
        base.OnHealthChanged(delta);
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        var game = GameController.Instance;
        game.InstantiateEffectBreak0(this.transform.position, 0);
        game.InstantiateItem(this.transform.position, ItemType.Bomb);
        for (int i=0; i<10; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * 96 * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Power);
        }
        for (int i=0; i<10; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * 96 * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Point);
        }
        base.OnHealthDrain();
    }
}
