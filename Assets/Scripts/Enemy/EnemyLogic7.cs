using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic7 : EnemyAbstractLogic {

    private const int HEALTH = 240000;
    private const float START_Y = Constants.StageVisibleY + 36f*Constants.PixelToUnit;
    private const float SHOOT_Y = Constants.StageVisibleY - 150f*Constants.p2u; 
    private const uint MOVE_TIME = 100;
    private const uint SHOOT_TIME = 600;
    private const uint SHOOT_INTERVAL = 30;

    public GameObject bulletPrefab; 

    private enum State {
        Enter, Shoot, Leave
    }
    private float startX;
    private State state;
    private uint timer;
    private uint timerShootInterval;
    private Transform ringEffect; 
    private bool bombUsed;

    private float sAngle; // rad

    public void SetParameters(float startX = 0) {
        this.startX = startX;
    }
    
    void Start() {
        base.Initialize(HEALTH);
        base.SetAcceptDamage(false);
        state = State.Enter;
        this.transform.position = new Vector3(startX, START_Y, Constants.DepthEnemy);
        timer = 0;
        ringEffect = transform.Find("RingEffect"); 
        sAngle = Utils.Atan2(GameController.playerPosition - Utils.VectorXY(this.transform.position));
    }

    void NotifyBombUsed() => bombUsed = true;

    void FixedUpdate() {
        if (ringEffect) {
            ringEffect.transform.Rotate(new Vector3(0, 0, 1f), Space.Self);
        }
        switch (state) {
            case State.Enter: {
                timer++; float progress = (float)(timer) / MOVE_TIME;
                this.transform.position = new Vector3(startX, Mathf.Lerp(START_Y, SHOOT_Y, Utils.CurveSin(progress)));
                if (timer == MOVE_TIME) {
                    timer = timerShootInterval = 0; state = State.Shoot; base.SetAcceptDamage(true);
                    bombUsed = false; 
                    GameController.Instance.delegateOnBombUsed += this.NotifyBombUsed;
                }
                break;
            }
            case State.Shoot: {
                timer++; float progress = (float)(timer) / SHOOT_TIME; 
                if (timer == SHOOT_TIME) {timer = 0; state = State.Leave;}
                timerShootInterval++;
                if (timerShootInterval >= SHOOT_INTERVAL) {
                    timerShootInterval=0; 
                    sAngle += 5 * Mathf.Deg2Rad;
                    int n = 20;
                    G.soundPlayer.Play("bullet0");
                    for (int i=0; i<n; i++) {
                        Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>()
                            .SetParameters(
                                this.transform.position, 
                                velocity: Utils.UnitByTheta(sAngle + Mathf.PI * 2 / n * i) * 0.035f,
                                1, 2, true, 12, 0, true, 0
                            );
                    }
                }
                // if (timer <= 20) {
                //     var renderer = ringEffect.GetComponent<SpriteRenderer>();
                //     renderer.color = Utils.ColorAlpha(renderer.color, 1-(float)(timer)/20);
                // }
                break;
            }
            case State.Leave: {
                timer++; float progress = (float)(timer) / MOVE_TIME;
                this.transform.position = new Vector3(startX, Mathf.Lerp(SHOOT_Y, START_Y, Utils.CurveNSin(progress)));
                if (timer == MOVE_TIME) {Destroy(gameObject);}
                break;
            }
        }
        // print(health);
    }

    protected override void OnHealthChanged(int delta) {
        if (delta < 0) {
            if (health < 50000) G.soundPlayer.Play("damage1");
            else G.soundPlayer.Play("damage0");
        }
        base.OnHealthChanged(delta);
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        var game = GameController.Instance;
        game.InstantiateEffectBreak0(this.transform.position, 0);
        if (this.bombUsed) {
            game.InstantiateItem(this.transform.position, ItemType.Bomb); 
        } else {
            game.InstantiateItem(this.transform.position, ItemType.Life); 
        }
        for (int i=0; i<10; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * 96 * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Power);
        }
        for (int i=0; i<10; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * 96 * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Point);
        }
        base.OnHealthDrain();
    }

    void OnDestroy() {
        GameController.Instance.delegateOnBombUsed -= this.NotifyBombUsed;
    }

}