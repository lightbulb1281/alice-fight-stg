using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic3 : EnemyAbstractLogic
{
    private const int HEALTH = 200;
    private const float START_X = Constants.StageVisibleX + 12f * Constants.PixelToUnit;
    private const float DELTA_Y = 75f * Constants.PixelToUnit;
    private const int FLY_LENGTH = 90;
    private const int SHOOT_INTERVAL = 25;
    private const int BULLET_COUNT_PER_SHOOT = 2;

    public GameObject bulletPrefab;

    private float startY;
    private bool moveRightwards;
    public enum ColorSet {
        A, //green
        B, //yellow
    }
    ColorSet color;
    private uint timer;
    private ItemType carriedItem;
    private SRandom robert;
    private int shootOffset;
    
    public void SetParameters(int seed, bool moveRightwards, float startY, ColorSet color, ItemType carriedItem) {
        this.moveRightwards = moveRightwards; this.startY = startY;
        this.color = color; this.carriedItem = carriedItem;
        robert = new SRandom(seed);
        shootOffset = robert.Int(SHOOT_INTERVAL);
    }

    void Start() {
        base.Initialize(HEALTH);
        this.transform.position = new Vector3(START_X * (moveRightwards ? -1: 1), startY, Constants.DepthEnemy);
        Utils.SetMoveAnimation(this.GetComponentInChildren<Animator>(), moveRightwards);
        timer = 0;
    }

    void FixedUpdate() {
        timer++;
        float progress = (float)(timer) / FLY_LENGTH;
        float x = Mathf.Lerp(-START_X, START_X, Utils.CurveExp(progress, 0, 0.693f));
        if (!moveRightwards) x = -x;
        float y = startY - Utils.CurveSin(progress * 1.6f) * DELTA_Y;
        this.transform.position = new Vector3(x, y, Constants.DepthEnemy);
        if (timer % SHOOT_INTERVAL == shootOffset) {
            for (int i=0; i<BULLET_COUNT_PER_SHOOT; i++) {
                var logic = Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>();
                Vector2 velocity = robert.Rotate(Vector2.down, 15) * robert.Float(0.02f, 0.04f);
                logic.SetParameters(
                    this.transform.position, velocity, 
                    color == ColorSet.A ? 11u : 13u, color == ColorSet.A ? 11u : 13u,
                    true, 12, robert.Float() * 360, true, robert.Float() * 2 + 3 
                );
            } 
        }
        if (timer == FLY_LENGTH) Destroy(gameObject);
    }

  protected override void OnHealthDrain() {
    GameController.soundPlayer.Play("enemybreak");
    uint breakColor = color == ColorSet.A ? 3u : 2u;
    GameController.Instance.InstantiateEffectBreak0(Utils.VectorXY(this.transform.position), breakColor);
    GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), carriedItem);
    base.OnHealthDrain();
  }
}
