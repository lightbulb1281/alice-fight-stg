using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class EnemyLogic8 : EnemyAbstractLogic {

    private const int HEALTH = 60000; 
    private const float START_Y = C.StageVisibleY + 40f * C.p2u;
    private const uint SHOOT_INTERVAL = 3;
    private const float ROTATION_INTERVAL = 4f * Mathf.Deg2Rad;

    public GameObject bulletPrefab;

    private Transform ringEffect;
    private float startX;
    private float shootRotation;
    private bool clockwise;
    private Timeout shootTimeout;
    private uint color;
    private Timer timer;

    public void SetParameters(float startX, bool clockwise, uint color) {
        this.startX = startX; this.clockwise = clockwise; this.color = color;
    }

    void Start() {
        base.Initialize(HEALTH);
        ringEffect = this.transform.Find("RingEffect");
        this.transform.position = new Vector3(startX, START_Y, C.DepthEnemy);
        shootRotation = 90 * Mathf.Deg2Rad + G.random.Float(-ROTATION_INTERVAL, ROTATION_INTERVAL) * 0.5f;
        shootTimeout = new Timeout(); shootTimeout.Reset(SHOOT_INTERVAL);
        this.timer = new Timer();
    }

    void FixedUpdate() {
        timer.Tick(); float p = timer.Progress(240); p = Mathf.Clamp(p, 0, 1); float speed = (40 + p * 40) * C.PixelsPerSecond;
        this.transform.Translate(speed * Vector3.down); 
        ringEffect.transform.Rotate(new Vector3(0, 0, 3), Space.Self);
        if (shootTimeout.TickReset(SHOOT_INTERVAL)) {
            G.soundPlayer.Play("bullet2");
            shootRotation += ROTATION_INTERVAL * (clockwise ? -1 : 1);
            float sr = shootRotation;
            Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                this.transform.position, Utils.UnitByTheta(sr) * 0.06f, 
                color, color, true, 12, sr * Mathf.Rad2Deg
            );
            sr = shootRotation + Mathf.PI;
            Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                this.transform.position, Utils.UnitByTheta(sr) * 0.06f, 
                color, color, true, 12, sr * Mathf.Rad2Deg
            );
        }
        if (this.transform.position.y < -C.StageVisibleY - 40 * C.PixelToUnit) {
            Destroy(gameObject);
        }
    }

    protected override void OnHealthChanged(int delta) {
        base.OnHealthChanged(delta);
        var r = ringEffect.GetComponent<SpriteRenderer>();
        r.color = U.ColorAlpha(r.color, (float)(health) / HEALTH);
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        G.I.InstantiateEffectBreak0(this.transform.position, (uint)(C.ColorBreakEffect0.Red));
        var game = GameController.Instance; 
        for (int i=0; i<6; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * 96 * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Power);
        }
        for (int i=0; i<5; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * 96 * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Point);
        }
        base.OnHealthDrain(); // Destroy
    }

}