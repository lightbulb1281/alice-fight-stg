using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic2 : EnemyAbstractLogic
{
    private const int HEALTH = 1000;
    private const float START_Y = Constants.StageVisibleY + 24f*Constants.PixelToUnit;
    private const float SHOOT_Y = Constants.StageVisibleY - 96f*Constants.PixelToUnit;
    private const float LEAVE_Y = -START_Y;
    private const uint ENTER_TIME = 20;
    private const uint SHOOT_TIME = 120;
    private const uint SHOOT_INTERVAL = 6;
    private const float LEAVE_MAX_SPEED = 0.07f;

    public GameObject bulletPrefab;

    private float startX;
    private float leaveX; 
    private SRandom robert;
    private enum State {
        Enter, Shoot, Leave
    }
    private State state;
    private uint timer;
    private ItemType carriedItem;
    public enum ColorSet {
        Blue, Cyan
    }
    private ColorSet colorSet;

    public void SetParameters(int robertSeed, float startX, ColorSet colorSet, ItemType carriedItem) {
        robert = new SRandom(robertSeed);
        this.startX = startX; this.carriedItem = carriedItem; this.colorSet = colorSet;
    }

    void Start() {
        base.Initialize(HEALTH);
        state = State.Enter;
        this.transform.position = new Vector3(startX, START_Y, Constants.DepthEnemy);
        leaveX = robert.Float(0, Constants.StageMaxX * 0.7f);
        if (startX > 0) leaveX = -leaveX;
        timer = 0;
    }

    void FixedUpdate() {
        switch (state) {
            case State.Enter: {
                timer++; float progress = (float)(timer) / ENTER_TIME;
                this.transform.position = new Vector3(startX, Mathf.Lerp(START_Y, SHOOT_Y, Utils.CurveSin(progress)), Constants.DepthEnemy); 
                if (timer == ENTER_TIME) {timer = 0; state = State.Shoot;}
                break;
            }
            case State.Shoot: {
                timer++;
                if (timer % SHOOT_INTERVAL == 0) {
                    GameController.soundPlayer.Play("bullet2");
                    var logic = Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>();
                    var direction = Utils.VectorXY(GameController.player.transform.position - transform.position);
                    direction.Normalize();
                    if (colorSet == ColorSet.Blue) direction = robert.Rotate(direction, 10);
                    var speed = robert.Float(0.05f, 0.08f);
                    logic.SetParameters(
                        Utils.VectorXY(transform.position),
                        direction * speed, 2, colorSet == ColorSet.Blue ? 6u : 8u, true, 12, 0, true, 0
                    );
                }
                if (timer == SHOOT_TIME) {timer = 0; state = State.Leave;}
                break;
            }
            case State.Leave: {
                timer++;
                float speed = ((timer<60) ? Mathf.Lerp(0, 1, (float)(timer)/60) : 1) * LEAVE_MAX_SPEED;
                if (timer == 30) Utils.SetMoveAnimation(GetComponentInChildren<Animator>(), leaveX > startX);
                Vector3 direction = new Vector2(leaveX, LEAVE_Y) - Utils.VectorXY(this.transform.position);
                this.transform.Translate(direction.normalized * speed, Space.World);
                if (Utils.OutsideStage(this.transform.position)) Destroy(gameObject);
                break;
            }
        }
    }

    protected override void OnHealthDrain() {
        GameController.soundPlayer.Play("enemybreak");
        GameController.Instance.InstantiateEffectBreak0(Utils.VectorXY(this.transform.position), 1);
        GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), carriedItem);
        base.OnHealthDrain(); // Destroy
    }
}
