using System.Collections;
using System.Collections.Generic;
using DC = UIDialogueSystemController.DialogueSystemCommand;
using DCS = System.Collections.Generic.List<UIDialogueSystemController.DialogueSystemCommand>;
using R = StaticStorage;

// TODO: Strings should be fetched from Localization

public static class Dialogue {

    private static readonly DC Clear = new DC(DC.Type.ClearTexts);
    private static readonly DC EnterLeft = new DC(DC.Type.EnterLeft);
    private static readonly DC EnterRight = new DC(DC.Type.EnterRight);
    private static readonly DC ExitLeft = new DC(DC.Type.ExitLeft);
    private static readonly DC ExitRight = new DC(DC.Type.ExitRight);
    private static readonly DC SetMaterial0 = new DC(DC.Type.SetTextMaterial, 0);
    private static readonly DC SetMaterial1 = new DC(DC.Type.SetTextMaterial, 1);
    private static DC SetLine1(string s) {return new DC(DC.Type.SetLine1, s);}
    private static DC SetLine2(string s) {return new DC(DC.Type.SetLine2, s);}
    private static DC ResetLine1(string s) {return new DC(DC.Type.ResetLine1, s);}
    private static DC ResetLine2(string s) {return new DC(DC.Type.ResetLine2, s);}
    private static DC Notify(int s) {return new DC(DC.Type.NotifyGameController, s);}
    private static DC Wait(int s) {return new DC(DC.Type.Wait, s);}
    private static DC SetSpriteLeft(int s) {return new DC(DC.Type.SetSpriteLeft, s);}
    private static DC SetSpriteRight(int s) {return new DC(DC.Type.SetSpriteRight, s);}

    public static List<DCS> Dialogue0, Dialogue1, Dialogue2, Dialogue3;
    
    public static void LoadDialogues() {

        Dialogue0 = new List<DCS>() {
            SetSpriteRight(2) + 
            SetMaterial0 + Clear +             EnterLeft  +  SetSpriteLeft( 2) +   SetLine1(R.GetLocaleString("Dialogue0.0")),
            Notify(0) + Wait(60),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 2) +   SetLine1(R.GetLocaleString("Dialogue0.1")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue0.2")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 3) +   SetLine1(R.GetLocaleString("Dialogue0.3")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 3) +   SetLine1(R.GetLocaleString("Dialogue0.4")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue0.5")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 3) +   SetLine1(R.GetLocaleString("Dialogue0.6")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 4) +   SetLine1(R.GetLocaleString("Dialogue0.7")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue0.8")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 4) +   SetLine1(R.GetLocaleString("Dialogue0.9")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue0.10")),
        };

        Dialogue1 = new List<DCS>() {
            SetSpriteLeft(5) + SetSpriteRight(4) + Notify(1) + Wait(60),
            SetMaterial1 + Clear +             EnterRight + SetSpriteRight( 4) +   SetLine1(R.GetLocaleString("Dialogue1.0")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 5) +   SetLine1(R.GetLocaleString("Dialogue1.1")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue1.2")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 5) +   SetLine1(R.GetLocaleString("Dialogue1.3")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue1.4")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 6) +   SetLine1(R.GetLocaleString("Dialogue1.5")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 6) +   SetLine1(R.GetLocaleString("Dialogue1.6")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 2) +   SetLine1(R.GetLocaleString("Dialogue1.7")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue1.8")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 7) +   SetLine1(R.GetLocaleString("Dialogue1.9")),
                                                            SetSpriteRight( 4) + ResetLine1(R.GetLocaleString("Dialogue1.10")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue1.11")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 7) +   SetLine1(R.GetLocaleString("Dialogue1.12")),
                                                             SetSpriteLeft( 5) + ResetLine1(R.GetLocaleString("Dialogue1.13")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue1.14")),
        };

        Dialogue2 = new List<DCS>() {
            SetSpriteLeft(5) + 
            SetMaterial1 + Clear +             EnterRight + SetSpriteRight( 8) +   SetLine1(R.GetLocaleString("Dialogue2.0")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 5) +   SetLine1(R.GetLocaleString("Dialogue2.1")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 9) +   SetLine1(R.GetLocaleString("Dialogue2.2")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue2.3")),
        };
        
        Dialogue3 = new List<DCS>() {
            SetSpriteLeft(8) +
            SetMaterial1 + Clear +             EnterRight + SetSpriteRight(10) +   SetLine1(R.GetLocaleString("Dialogue3.0")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 8) +   SetLine1(R.GetLocaleString("Dialogue3.1")),
                                                             SetSpriteLeft( 9) +   SetLine2(R.GetLocaleString("Dialogue3.2")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(11) +   SetLine1(R.GetLocaleString("Dialogue3.3")),
                                                            SetSpriteRight(12) +   SetLine2(R.GetLocaleString("Dialogue3.4")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(10) +   SetLine1(R.GetLocaleString("Dialogue3.5")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.6")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(13) +   SetLine1(R.GetLocaleString("Dialogue3.7")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.8")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 3) +   SetLine1(R.GetLocaleString("Dialogue3.9")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 0) +   SetLine1(R.GetLocaleString("Dialogue3.10")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(11) +   SetLine1(R.GetLocaleString("Dialogue3.11")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.12")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(14) +   SetLine1(R.GetLocaleString("Dialogue3.13")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(12) +   SetLine1(R.GetLocaleString("Dialogue3.14"))
                                                          +                        SetLine2(R.GetLocaleString("Dialogue3.15")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(15) +   SetLine1(R.GetLocaleString("Dialogue3.16")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(2)  +   SetLine1(R.GetLocaleString("Dialogue3.17"))
                                                                               +   SetLine2(R.GetLocaleString("Dialogue3.18")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(16) +   SetLine1(R.GetLocaleString("Dialogue3.19")),
                                                                                 ResetLine1(R.GetLocaleString("Dialogue3.20")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.21")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft( 4) +   SetLine1(R.GetLocaleString("Dialogue3.22")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.23")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(17) +   SetLine1(R.GetLocaleString("Dialogue3.24")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(13) +   SetLine1(R.GetLocaleString("Dialogue3.25")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.26")),
                           Clear +                           SetSpriteLeft( 3) + ResetLine1(R.GetLocaleString("Dialogue3.27")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.28")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 2) +   SetLine1(R.GetLocaleString("Dialogue3.29")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.30")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(10) +   SetLine1(R.GetLocaleString("Dialogue3.31")),
                                                                                   SetLine2(R.GetLocaleString("Dialogue3.32")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight( 3) +   SetLine1(R.GetLocaleString("Dialogue3.33")),
                                                            SetSpriteRight( 5) +   SetLine2(R.GetLocaleString("Dialogue3.34")),
            SetMaterial0 + Clear + ExitRight +  EnterLeft +  SetSpriteLeft(14) +   SetLine1(R.GetLocaleString("Dialogue3.35")),
            SetMaterial1 + Clear +  ExitLeft + EnterRight + SetSpriteRight(18) +   SetLine1(R.GetLocaleString("Dialogue3.36")),
            
        };
    }
    

}