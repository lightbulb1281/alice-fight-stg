using UnityEngine;

public abstract class StageAbstractLogic : MonoBehaviour {

    public abstract void StartDialogue(int i);
    public abstract void NotifyDialogueDone();
    public abstract void NotifyDialogueSignal(int signal);
    public abstract void NotifyBossLeft();

}