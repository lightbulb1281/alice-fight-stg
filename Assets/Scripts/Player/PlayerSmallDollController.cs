using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSmallDollController : MonoBehaviour
{
    private Vector3 targetPosition;
    private float targetAlpha;
    public void setTargetPosition(Vector3 targetLocalPosition) {targetPosition = targetLocalPosition;}
    public void setTargetAlpha(float a){targetAlpha = a;}
    void FixedUpdate() {
        Vector3 pos = this.transform.localPosition;
        float len = (targetPosition - pos).magnitude; 
        float mlen = 0.03f + len/20; if (mlen>len) mlen = len;
        this.transform.Translate((targetPosition - pos).normalized * mlen);
        var renderer = this.GetComponentInChildren<SpriteRenderer>();
        var currentAlpha = renderer.color.a;
        if (targetAlpha != currentAlpha) {
            float a = 0;
            if (targetAlpha > currentAlpha) {a = currentAlpha + 0.1f; if (a>targetAlpha) a = targetAlpha;}
            else                            {a = currentAlpha - 0.1f; if (a<targetAlpha) a = targetAlpha;}
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, a);
        }
    }
}
