using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private const float POSITION_X_MAXIMUM = Constants.StageVisibleX - 32/64f/2;
    private const float POSITION_Y_MAXIMUM = Constants.StageVisibleY - 48/64f/2;

    private const uint SLOW_SHOOT_INTERVAL = 3; // frames
    private const uint FAST_SHOOT_INTERVAL = 3;

    private const float SMALL_DOLL_DISTANCE = 0.5f;
    private const uint SMALL_DOLL_ROTATION_INTERVAL = 60; // frames

    private const uint MISS_KURAI_TIME = 8;
    private const uint MISS_DISSOLVE_TIME = 20; // frames
    private const uint MISS_REBIRTH_TIME = 60;
    private const uint MISS_INVINCIBLE_TIME = 300;

    private enum MissState {
        Normal, Dissolving, Rebirth
        // Normal: The player is playing safely. 
        // Dissolving: The player has just taken a bullet and the sprite is shrinking.
        // Rebirth: The life is take and a new player sprite is moving up from the bottom of the stage.
    };

    public Animator spriteAnimator;

    [Header("Game-related variables")]
    public float MoveSpeedFast = 0.10f; // pixels per frame
    public float MoveSpeedSlow = 0.03f;

    [Header("Prefabs")]
    public GameObject slowShotPrefab;
    public GameObject fastShotPrefab;
    public GameObject smallDollPrefab;
    public GameObject fastBombPrefab;
    public GameObject slowBombPrefab;

    [Header("GameObject References")]
    public TMPro.TMP_Text powerText;

    private Transform displayObject;
    private uint slowShotTimer = 0;
    private uint fastShotTimer = 0;
    private uint smallDollsRotationTimer = 0;
    private uint power; // 0~800
    private List<GameObject> smallDolls;
    private MissState missState;
    private bool invincible;
    private uint missTimer;
    private uint invincibleTimer; 
    private uint bombDisabledTimer;
    private GameController game;
       
    void Start() {
        game = GameController.Instance;
        transform.position = new Vector3(0, -2, Constants.DepthPlayer);
        displayObject = this.transform.GetChild(0);
        slowShotTimer = 0;
        smallDolls = new List<GameObject>();
        initializePlayerState();
    }

    void initializePlayerState() {
        setPower(0);
        missState = MissState.Normal;
        invincible = false; bombDisabledTimer = 0;
        GameController.Instance.bombs = 3;
    }

    void setPower(uint power) {
        power = (uint)Mathf.Clamp((int)power, (int)0, (int)Constants.MaxPower);
        this.power = power;
        powerText.text = string.Format("{0}.{1}{2}", power/100, (power%100)/10, power%10);
        uint c = power / 100 + 1; // new count of small dolls;
        if (c == smallDolls.Count) return; // no need to update
        if (c > smallDolls.Count) {
            for (int i=smallDolls.Count; i<c; i++) 
                smallDolls.Add(Instantiate(smallDollPrefab, this.transform.position, Quaternion.identity, displayObject.transform));
        } else {
            for (int i=smallDolls.Count; i>c; i--) 
                Destroy(smallDolls[smallDolls.Count-1]);
                smallDolls.RemoveAt(smallDolls.Count-1);
        }
    }

    public uint GetPower() {
        return power;
    }

    void setSmallDollsPosition(bool center) {
        int c = smallDolls.Count;
        if (!center) {
            float totalDegrees = 22 * (c-1); 
            float startDegrees = 270 - totalDegrees / 2;
            for (int i=0; i<c; i++) {
                float r = (startDegrees + i * 22) * Mathf.PI / 180; 
                Vector3 dpos = new Vector3(Mathf.Cos(r), Mathf.Sin(r), 0) * SMALL_DOLL_DISTANCE;
                var controller = smallDolls[i].GetComponent<PlayerSmallDollController>();
                controller.setTargetPosition(dpos);
                controller.setTargetAlpha(1);
            }
        } else {
            foreach (var item in smallDolls) {
                var controller = item.GetComponent<PlayerSmallDollController>();
                controller.setTargetPosition(Vector3.zero);
                controller.setTargetAlpha(0);
            }
        }
    }

    void move() {
        if (!game.playerControlEnabled) return;
        bool slow = Input.GetKey(KeyCode.LeftShift);
        Vector3 moveDirection = Vector3.zero;
        if (Input.GetKey(KeyCode.LeftArrow)) {
            spriteAnimator.SetBool("LeftKeyPressed", true);
            moveDirection += Vector3.left;
        } else {
            spriteAnimator.SetBool("LeftKeyPressed", false);
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            spriteAnimator.SetBool("RightKeyPressed", true);
            moveDirection += Vector3.right;
        } else {
            spriteAnimator.SetBool("RightKeyPressed", false);
        }
        if (Input.GetKey(KeyCode.UpArrow)) moveDirection += Vector3.up;
        if (Input.GetKey(KeyCode.DownArrow)) moveDirection += Vector3.down;
        if (moveDirection.magnitude > 0.1f) {
            if (slow) 
                moveDirection = moveDirection.normalized * MoveSpeedSlow; 
            else 
                moveDirection = moveDirection.normalized * MoveSpeedFast; 
            transform.Translate(moveDirection);
            // check inside stage
            var position = transform.position;
            transform.position = new Vector3(
                Mathf.Clamp(position.x, -POSITION_X_MAXIMUM, POSITION_X_MAXIMUM), 
                Mathf.Clamp(position.y, -POSITION_Y_MAXIMUM, POSITION_Y_MAXIMUM), position.z);
        }
    }

    void shoot(bool normalShootEnabled=true, bool bombEnabled=true) {
        if (game.IsInDialogue()) return;
        if (!game.playerControlEnabled) return;
        bool slow = Input.GetKey(KeyCode.LeftShift);
        if (normalShootEnabled) {
            if (Input.GetKey(KeyCode.Z)) {
                if (slow) {
                    if (slowShotTimer == 0)
                        Instantiate(slowShotPrefab, this.transform.position, Quaternion.identity);
                    slowShotTimer++;
                    if (slowShotTimer == SLOW_SHOOT_INTERVAL) slowShotTimer = 0;
                    fastShotTimer = 0;
                } else {
                    if (fastShotTimer == 0) {
                        int c = smallDolls.Count;
                        float a = 3 + 2*c;
                        for (int i=0; i<c; i++) {
                            float r = (c==1) ? 90 : (90 + a/2 - (a/(c-1))*i);
                            var shot = Instantiate(fastShotPrefab, smallDolls[i].transform.position, Quaternion.Euler(0, 0, r));
                            GameController.soundPlayer.Play("playershoot");
                            shot.GetComponent<PlayerFastShotLogic>().setDirection(r);
                        }
                    }
                    fastShotTimer++;
                    if (fastShotTimer == FAST_SHOOT_INTERVAL) fastShotTimer = 0;
                    slowShotTimer = 0;
                }
            } else {
                slowShotTimer = 0; 
            }
        }
        if (bombEnabled) {
            if (Input.GetKey(KeyCode.X) && bombDisabledTimer == 0) {
                uint bombsLeft = GameController.Instance.bombs;
                if (bombsLeft > 0) {
                    if (!slow) {
                        bombDisabledTimer = 240; 
                        Vector2 shootPos = Utils.VectorXY(this.transform.position);
                        Vector2 targetPos = shootPos + Mathf.Min(4.5f, (Constants.StageMaxY - shootPos.y) / 2) * Vector2.up;
                        Instantiate(fastBombPrefab).GetComponent<PlayerFastBombLogic>().SetParameters(shootPos, targetPos);
                    } else {
                        bombDisabledTimer = 180;
                        Vector2 shootPos = Utils.VectorXY(this.transform.position);
                        Instantiate(slowBombPrefab).GetComponent<PlayerSlowBombLogic>().SetParameters(shootPos);
                        GameController.soundPlayer.Play("slowbomb");
                    }
                    if (missState == MissState.Dissolving) {
                        Debug.Log("Kurai bomb used.");
                        missState = MissState.Normal;
                    }
                    invincible = true; invincibleTimer = bombDisabledTimer + 60;
                    GameController.Instance.Notify(GameController.Notification.PlayerUsedBomb);
                    GameController.Instance.PlayPlayerSpellcardTachie(slow);
                }
            }
        }
        if (bombDisabledTimer > 0) bombDisabledTimer--;
    }

    void updateSmallDollsTransfrom() {
        smallDollsRotationTimer++;
        float smallDollRotation = Mathf.Sin((float)smallDollsRotationTimer / SMALL_DOLL_ROTATION_INTERVAL * 2 * Mathf.PI);
        foreach (var k in smallDolls) {
            k.transform.localRotation = Quaternion.Euler(0, 0, smallDollRotation * 15);
        }
        bool slow = Input.GetKey(KeyCode.LeftShift);
        setSmallDollsPosition(slow);
    }

    void FixedUpdate() {
        if (invincible && missState != MissState.Dissolving) {
            invincibleTimer--; if (invincibleTimer==0) invincible=false;
            bool flickering = (invincibleTimer / 12)%2==1; 
            var renderers = displayObject.GetComponentsInChildren<SpriteRenderer>();
            foreach (var renderer in renderers) renderer.enabled = flickering || !invincible;
        }
        if (missState == MissState.Normal) {
            move();
            if (transform.position.y > Constants.StageItemCollectY) {
                var itemObjects = GameObject.FindGameObjectsWithTag("Item");
                foreach (var itemObject in itemObjects) {
                    var logic = itemObject.GetComponent<ItemLogic>(); 
                    if (logic) logic.NotifyAutoCollect();
                    else Debug.LogWarning("Item GameObject does not have a ItemLogic.");
                }
            }
            updateSmallDollsTransfrom();
            shoot();
        } else if (missState == MissState.Dissolving) {
            if (missTimer < MISS_KURAI_TIME) {
                updateSmallDollsTransfrom();
                shoot(false, true);
            }
            if (missState == MissState.Dissolving) {
                missTimer++;
                if (missTimer == MISS_KURAI_TIME) {
                    spriteAnimator.SetBool("LeftKeyPressed", false);
                    spriteAnimator.SetBool("RightKeyPressed", false);
                    GameController.Instance.Notify(GameController.Notification.PlayerMissed);
                }
                float progress = (float)(missTimer) / MISS_DISSOLVE_TIME;
                displayObject.localScale = new Vector3(1-progress, 1+progress, 1);
                if (missTimer == MISS_DISSOLVE_TIME) {
                    missState = MissState.Rebirth; missTimer = 0; invincibleTimer = MISS_INVINCIBLE_TIME;
                    displayObject.localScale = Vector3.one; 
                }
            } else if (missState == MissState.Normal) {
                displayObject.localScale = new Vector3(1, 1, 1);
            }
        } else if (missState == MissState.Rebirth) {
            missTimer++;
            float progress = (float)(missTimer) / MISS_REBIRTH_TIME;
            float y = Mathf.Lerp(-POSITION_Y_MAXIMUM - 48f/64, -3.5f, Mathf.Sin(progress * Mathf.PI/2));
            this.transform.position = new Vector3(0, y, Constants.DepthPlayer);
            if (missTimer == MISS_REBIRTH_TIME) {
                missState = MissState.Normal;
            }
        }

    }

    public void NotifyHit() {
        if (invincible) return;
        invincible = true; missState = MissState.Dissolving; missTimer = 0;
        GameController.soundPlayer.Play("miss");
    }

    public void NotifyCollectedPower(uint amount) {
        setPower(power + amount);
    }

    public void DebugInvincible() {
        invincible = true; invincibleTimer = uint.MaxValue;
    }
}
