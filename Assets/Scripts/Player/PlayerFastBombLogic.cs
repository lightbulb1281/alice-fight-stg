using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFastBombLogic : MonoBehaviour
{
    private static readonly uint FLY_TIME = 40; // frames
    private static readonly float RING_RADIUS = 3.2f;
    private enum State {
        Flying, Cast
    }

    public GameObject ringEffect0Prefab;
    public GameObject ringEffect1Prefab;

    private Transform ring0;
    private List<Transform> ring1s;
    private List<int> ring1LiveTimes;
    private Transform dollDisplay, lightDisplay;
    private Transform eraser;
    private State state;
    private uint timer; 
    private Vector2 targetPosition;
    private Vector2 startPosition;

    public void SetParameters(Vector2 startPosition, Vector2 targetPosition) {
        this.targetPosition = targetPosition;
        this.startPosition = startPosition;
    }

    void Start() {
        state = State.Flying; timer = 0;
        this.transform.position = Utils.VectorZ(startPosition, Constants.DepthBomb);
        Debug.Assert(transform.childCount >= 3);
        dollDisplay = transform.GetChild(0);
        lightDisplay = transform.GetChild(1);
        eraser = transform.GetChild(2);
        ring1s = new List<Transform>(); ring0 = null; ring1LiveTimes = new List<int>();
    }
    
    void setEffects(uint timer) {
        if (ring0) {
            float progress = (float)(timer) / 240;
            float sp = Mathf.Clamp(progress, 0, 1); sp = 1-Mathf.Pow(1-sp, 4);
            float scale = Mathf.Sin(sp * Mathf.PI / 2) * 6.5f; 
            ring0.localScale = new Vector3(scale, scale, 1);
            ring0.localRotation = Quaternion.Euler(0, 0, timer * 6);
            float alpha = 1 - progress * 1.2f; if (alpha<0) alpha = 0;
            var renderer = ring0.GetComponent<SpriteRenderer>(); renderer.color = Utils.ColorAlpha(renderer.color, alpha);
            this.GetComponent<CircleCollider2D>().radius = scale * 0.55f;
            eraser.GetComponent<CircleCollider2D>().radius = scale * 0.60f;
            if (alpha <= 0) Destroy(gameObject);
        }
        int n = ring1s.Count;
        for (int i=0; i<n; i++) {
            float progress = (float)(ring1LiveTimes[i]) / 90;
            float scale = Mathf.Sin(Mathf.Clamp(progress, 0, 1) * Mathf.PI / 2) * 2f;
            ring1s[i].localScale = new Vector3(scale, scale, 1);
            ring1s[i].localRotation = Quaternion.Euler(0, 0, progress * 360 * 2);
            float alpha = 1 - progress; if (alpha<0) alpha = 0; 
            var renderer = ring1s[i].GetComponent<SpriteRenderer>(); renderer.color = Utils.ColorAlpha(renderer.color, alpha);
        }
        {
            float tp = Mathf.Clamp((float)(timer) / 40, 0, 1);
            float scale = 1 + Mathf.Sin(tp * Mathf.PI / 2) * 4.5f;
            float alpha = 1-tp;
            dollDisplay.localScale = new Vector3(scale, scale, 1);
            lightDisplay.localScale = new Vector3(scale * 1.6f, scale * 1.6f, 1);
            var renderer = dollDisplay.GetComponent<SpriteRenderer>(); renderer.color = Utils.ColorAlpha(renderer.color, alpha);
            renderer = lightDisplay.GetComponent<SpriteRenderer>(); renderer.color = Utils.ColorAlpha(renderer.color, alpha);
        }
    }

    void FixedUpdate() {
        switch (state) {
            case State.Flying: {
                timer++; float progress = (float)(timer) / FLY_TIME;
                this.transform.position = Utils.VectorZ(Vector2.Lerp(startPosition, targetPosition, progress), Constants.DepthBomb);
                this.transform.localRotation = Quaternion.Euler(0, 0, Mathf.PI*2*progress*Mathf.Rad2Deg);
                if (timer == FLY_TIME) {
                    state = State.Cast; timer = 0;
                    ring0 = Instantiate(ringEffect0Prefab, transform.position, Quaternion.identity, transform).transform;
                    this.GetComponent<CircleCollider2D>().enabled = true;
                    eraser.GetComponent<Collider2D>().enabled = true;
                    setEffects(0);
                    GameController.soundPlayer.Play("fastbomb");
                }
                break;
            } 
            case State.Cast: {
                timer++;
                for (uint i=0; i<9; i++) {
                    if (timer == i*5+50) {
                        float theta = 10+20*i;
                        float x = Mathf.Sin(theta*Mathf.Deg2Rad), y = -Mathf.Cos(theta*Mathf.Deg2Rad);
                        x *= RING_RADIUS * 0.8f; y *= RING_RADIUS * 0.8f;
                        ring1s.Add(Instantiate(ringEffect1Prefab, transform.position + new Vector3(x, y, 0), Quaternion.identity, transform).transform);
                        ring1s.Add(Instantiate(ringEffect1Prefab, transform.position + new Vector3(-x, y, 0), Quaternion.identity, transform).transform);
                        ring1LiveTimes.Add(0); ring1LiveTimes.Add(0);
                    }
                }
                int n = ring1s.Count;
                for (int i=0; i<n; i++) {ring1LiveTimes[i]++;}
                setEffects(timer);
                break;
            }
        }
    }
}
