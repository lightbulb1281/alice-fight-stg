using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitpointController : MonoBehaviour
{
    float alpha; 
    PlayerController player;

    void Start() {
        alpha = 0; 
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift)) {
            alpha = Mathf.Clamp01(alpha + 0.25f);
        } else {
            alpha = Mathf.Clamp01(alpha - 0.25f);
        }
        this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, alpha);
        this.transform.Rotate(0, 0, 2, Space.Self);
    }

    void OnTriggerStay2D(Collider2D collider) {
        if (collider.tag == "Bullets" || collider.tag == "EnemyBody") {
            // print(collider.gameObject);
            GameController.player.GetComponent<PlayerController>().NotifyHit();
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Bullets" || collider.tag == "EnemyBody") {
            // print(collider.gameObject);
            GameController.player.GetComponent<PlayerController>().NotifyHit();
        }
    }
}
