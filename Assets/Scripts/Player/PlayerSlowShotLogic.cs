using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlowShotLogic : MonoBehaviour
{
    public const float MOVE_SPEED = 0.06f;
    public const float FADE_FRAMES = 10;

    private float alpha;
    private float displacement; 
    private int timer = 0;

    void Start() {
        alpha = 0.7f;
        displacement = 0;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, Constants.DepthShot);
        setAlpha();
        timer = 0;
    }

    void setAlpha() {
        SpriteRenderer renderer = this.GetComponentInChildren<SpriteRenderer>();
        renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);
    }

    void FixedUpdate() {
        displacement += MOVE_SPEED;
        this.transform.GetChild(0).Translate(new Vector3(0, 0.1f, 0), Space.World);
        if (displacement > 4) {
            displacement = 0;
            this.transform.GetChild(0).Translate(new Vector3(0, -4f, 0), Space.World);
        }
        alpha -= 0.7f/FADE_FRAMES;
        timer ++;
        if (timer == FADE_FRAMES) Destroy(gameObject);
        else setAlpha();
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.tag == "Enemy") {
            GameController.Instance.score += 2;
        }
    }
}
