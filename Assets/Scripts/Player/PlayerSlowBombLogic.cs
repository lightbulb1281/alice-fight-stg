using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlowBombLogic : MonoBehaviour
{
    private Vector3 targetPosition, startPosition;
    private Transform laserDisplay;
    private Transform dollDisplay;
    private Transform eraser;
    private State state;
    private uint timer;
    private float laserDisplacement;

    private enum State {
        Flying, Cast, Shrinking
    }

    public void SetParameters(Vector2 startPosition) {
        this.startPosition = Utils.VectorZ(startPosition, Constants.DepthBomb);
        this.targetPosition = this.startPosition + Vector3.up * 0.5f;
    }

    void setFlyingTransforms() {
        float progress = (float)(timer) / 20;
        float scale = progress; float rotation = 180 + progress * 180;
        this.transform.position = Vector3.Lerp(startPosition, targetPosition, progress);
        this.transform.localScale = new Vector3(scale, scale, 1);
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation);
    }

    void Start() {
        Debug.Assert(transform.childCount >= 4);
        laserDisplay = transform.GetChild(0);
        dollDisplay = transform.GetChild(1);
        eraser = transform.GetChild(2);
        eraser.GetComponent<Collider2D>().enabled = false;
        state = State.Flying; timer = 0; setFlyingTransforms();
    }

    void FixedUpdate() {
        switch (state) {
            case State.Flying: {
                timer++; setFlyingTransforms();
                if (timer == 20) {
                    state = State.Cast; timer = 0;
                    laserDisplay.GetComponent<SpriteRenderer>().enabled = true;
                    laserDisplacement = 0;
                    this.GetComponent<BoxCollider2D>().enabled = true;
                    eraser.GetComponent<Collider2D>().enabled = true;
                }
                break;
            }
            case State.Cast: {
                timer++; 
                laserDisplacement = Mathf.Repeat(laserDisplacement + 0.06f, 4f);
                laserDisplay.localPosition = new Vector3(0, laserDisplacement, 0);
                dollDisplay.position = targetPosition + Utils.RandomIdentityVector2D() * 0.02f * Random.value;
                if (timer == 150) {
                    state = State.Shrinking; timer = 0;
                    dollDisplay.position = targetPosition;
                }
                break;
            }
            case State.Shrinking: {
                timer++;
                float progress = (float)(timer) / 20;
                float scale = 1-progress; float rotation = + progress * 180;
                dollDisplay.localScale = new Vector3(scale, scale, 1);
                dollDisplay.localRotation = Quaternion.Euler(0, 0, rotation);
                laserDisplacement = Mathf.Repeat(laserDisplacement + 0.06f, 4f);
                laserDisplay.localPosition = new Vector3(0, laserDisplacement, 0);
                laserDisplay.localScale = new Vector3(1, scale, 1);
                if (timer == 20) Destroy(gameObject);
                break;
            }
        }
    }

}
