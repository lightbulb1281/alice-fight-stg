using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFastShotLogic : MonoBehaviour
{
    private static readonly float VELOCITY = 0.3f;

    public GameObject effectHitPrefab;

    private Vector3 direction;
    private uint timer;
    void Start() {
        timer = 0;
    }
    public void setDirection(float directionDegrees) {
        float r = directionDegrees / 180 * Mathf.PI;
        direction = new Vector3(Mathf.Cos(r), Mathf.Sin(r), 0);
    }
    void FixedUpdate() {
        timer++;
        this.transform.Translate(direction * VELOCITY, Space.World);
        var renderer = GetComponentInChildren<SpriteRenderer>();
        renderer.color = Color.HSVToRGB(Mathf.Repeat((float)(timer)/40, 1), 1f, 1);
        if (Mathf.Abs(transform.position.x) > Constants.StageMaxX || Mathf.Abs(transform.position.y) > Constants.StageMaxY) Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Enemy") {
            Instantiate(effectHitPrefab, this.transform.position + Utils.RandomIdentityVector2D() * 0.05f, Quaternion.identity);
            GameController.Instance.score += 15;
            Destroy(gameObject);
        }
    }
}
