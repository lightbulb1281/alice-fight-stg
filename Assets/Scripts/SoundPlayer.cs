using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{

    [System.Serializable]
    public class SFXSetting {
        public string name;
        public AudioClip clip;
        public uint minInterval;
        public float volume = 1;
        Timeout timer;
        public void initializeTimer() {
            timer = new Timeout(0);
        }
        public void play(AudioSource source) {
            if (timer.Check()) {
                source.PlayOneShot(clip, volume);
                timer.Reset(minInterval);
            }
        }
        public void tick() {
            timer.Tick();
        }
        public override string ToString() {
            return name;
        }
    }

    public List<SFXSetting> settings;
    
    private Dictionary<string, SFXSetting> clips;
    private AudioSource source;
    bool soundEnabled;
    
    private LinkedList<(uint, string)> scheduled;

    void Awake() {
        bool se = PlayerPrefs.GetInt("SE", 1) != 0;
        soundEnabled = se;
        clips = new Dictionary<string, SFXSetting>();
        for (int i=0; i<settings.Count; i++) {
            clips.Add(settings[i].name, settings[i]);
        }
        foreach (var p in clips) {
            p.Value.initializeTimer();
        }
        this.source = GetComponent<AudioSource>();
        scheduled = new LinkedList<(uint, string)>();
    }

    public void Play(string key) {
        SFXSetting s;
        bool p = clips.TryGetValue(key, out s);
        if (!p) {
            Debug.LogWarningFormat("Key {0} not found as a SFX.", key);
        } else {
            if (soundEnabled) s.play(source);
        }
    }

    public void SchedulePlay(string key, uint delay) {
        scheduled.AddLast((delay, key));
    }

    void FixedUpdate() {
        foreach (var x in clips) {
            x.Value.tick();
        }
        for (var iterator = scheduled.First; iterator != null;) {
            uint k = iterator.Value.Item1;
            if (k>1) {
                iterator.Value = (k-1, iterator.Value.Item2);
                iterator = iterator.Next;
            } else {
                Play(iterator.Value.Item2);
                var next = iterator.Next;
                scheduled.Remove(iterator);
                iterator = next;
            }
        }
    }

}
