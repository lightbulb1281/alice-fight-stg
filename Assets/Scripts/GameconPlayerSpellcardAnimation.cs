using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameconPlayerSpellcardAnimation : MonoBehaviour
{

    private const uint TACHIE_LENGTH = 90;
    private const uint TEXT_LENGTH = 120;
    private uint blackSheenLength;
    private bool playing;
    private uint timer;

    public Transform playerTachie;
    public SpriteRenderer blackSheen;
    public Transform text;

    void Start() {
        playing = false;
        playerTachie.GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,0);
        playerTachie.gameObject.SetActive(false);
        text.GetComponent<RectTransform>().localPosition = (-30+522) * Vector3.left;
    }

    public void Play(bool slow) {
        this.blackSheenLength = slow ? 180u : 240u;
        playing = true; timer = 0;
        playerTachie.gameObject.SetActive(true);
        text.GetComponentInChildren<LocalizationAdapter>()
            .Reset(slow ? "BombSlow" : "BombFast");
        // text.GetComponentInChildren<TMPro.TMP_Text>().text = slow ? Localization.BombSlow : Localization.BombFast;
    }

    void FixedUpdate() {
        if (!playing) return;
        timer++;
        // tachie
        if (timer < TACHIE_LENGTH) {
            float progress = (float)(timer) / TACHIE_LENGTH;
            float alpha = Mathf.Lerp(0.5f, 0f, 2*Mathf.Abs(progress - 0.5f));
            playerTachie.GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,alpha);
            var t = playerTachie.GetComponent<RectTransform>();
            t.localPosition = new Vector3(-392, Mathf.Lerp(-306, 0, progress), 0);
        }
        // black sheen
        if (timer < blackSheenLength) {
            float alpha = 1f;
            if (timer < 5) alpha = (float)(timer)/5;
            else if (timer > blackSheenLength - 60) alpha = (float)(blackSheenLength-timer) / 60;
            blackSheen.color = Utils.ColorAlpha(Color.black, alpha * 0.7f);
        }
        // text
        if (timer <= TEXT_LENGTH) {
            if (timer <= 30) {
                float progress = (float)(timer) / 30;
                float scale = 3 - progress * 2, alpha = progress;
                var stripe = text.GetComponentInChildren<UnityEngine.UI.Image>();
                stripe.color = new Color(1,1,1,alpha);
                var prompt = text.GetComponentInChildren<TMPro.TMP_Text>();
                prompt.color = new Color(1,1,1,alpha);
                var rect = text.GetComponent<RectTransform>();
                rect.localPosition = 30*Vector3.right;
                rect.localScale = new Vector3(scale, scale, 1);
            } else if (timer > TEXT_LENGTH - 20) {
                float progress = (float)(timer - TEXT_LENGTH + 20) / 20;
                var rect = text.GetComponent<RectTransform>();
                rect.localPosition = 30*Vector3.right + 522*progress*Vector3.left;
            }
        }
        if (timer == TACHIE_LENGTH) {
            playerTachie.gameObject.SetActive(false);
            playerTachie.GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,0);
        }
        if (timer == blackSheenLength) {
            blackSheen.color = Utils.ColorAlpha(Color.black, 0);
        }
        if (timer == Mathf.Max(TACHIE_LENGTH, blackSheenLength, TEXT_LENGTH)) playing = false;
    }
}
