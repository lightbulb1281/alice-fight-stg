using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteVisibilityController : MonoBehaviour
{
    public uint animationLength = 20;
    [Range(0f, 1f)] public float globalAlpha = 1;

    private SpriteRenderer spriteRenderer;
    private float displayed;
    private float actual;

    void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer) spriteRenderer.color = Utils.ColorAlpha(spriteRenderer.color, 0); 
        displayed = actual = 0;
    }

    public void Show(){actual = 1;}
    public void Hide(){actual = 0;}
    public void SetTarget(float c) {actual = c;}
    public float GetDisplayed() {return displayed;}
    public float GetActual() {return actual;}

    void FixedUpdate() {
        if (displayed < actual) {displayed += 1.0f/animationLength; if (displayed > actual) displayed = actual; refresh();}
        else if (displayed > actual) {displayed -= 1.0f/animationLength; if (displayed < actual) displayed = actual; refresh();}
    }

    void refresh() {
        if (spriteRenderer) spriteRenderer.color = Utils.ColorAlpha(spriteRenderer.color, displayed * globalAlpha); 
    }
}
