using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class StageLogic0 : StageAbstractLogic {

    public static List<string> GetPracticeNames() {
        return new List<string>() {
            "First Half Part 0",
            "First Half Part 1",
            "First Half Part 2",
            "First Half Part 3",
            "First Half Part 4",
            "First Half Part 5",
            "Midboss Nonspell 1",
            "Midboss Spellcard 1",
            "Midboss Spellcard 2",
            "Second Half Part 6",
            "Second Half Part 7",
            "Second Half Part 8",
            "Boss Nonspell 1",
            "Boss Spellcard 1",
            "Boss Nonspell 2",
            "Boss Spellcard 2",
            "Boss Nonspell 3",
            "Boss Spellcard 3",
            "Boss Nonspell 4",
            "Boss Spellcard 4",
            "Boss Nonspell 5",
            "Boss Spellcard 5",
            "Boss Nonspell 6",
            "Boss Spellcard 6",
            "Boss Spellcard 7",
            "Boss Spellcard 8",
            "Boss Spellcard 9",
            "Boss Spellcard 10",
            "Boss Spellcard 11"
        };
    }
    
    public enum State {
        Debug, 
        Wait0, Part0, Part1, Part2, Part3, Part4, Part5,
        Dialogue0, Midboss,
        Wait1, Part6, Part7, Part8,
        Dialogue1, Boss, Dialogue2, Dialogue3,
        PracticeWait,
    }

    [Header("Prefabs")]
    
    public GameObject enemyMarisaPrefab;

    public GameObject enemyPart0Prefab;
    public GameObject enemyPart1Prefab;
    public GameObject enemyPart2Prefab;
    public GameObject enemyPart3PrefabA;
    public GameObject enemyPart3PrefabB;
    public GameObject enemyPart4Prefab;
    public GameObject enemyPart5Prefab;
    
    public GameObject enemyPart6Prefab; 
    public GameObject enemyPart7PrefabA;
    public GameObject enemyPart7PrefabB;
    public GameObject enemyPart8Prefab;

    private State state;
    private Timer timer;
    private SRandom robert, random;
    private EnemyMarisaController marisa;

    void Start() {
        timer = new Timer();
        random = new SRandom(G.random.Int());
        timer.Reset();
        string practice = StaticStorage.Instance.practice;
        Debug.LogFormat("Practice = {0}", practice);
        if (practice != "None") switchState(State.PracticeWait);
        else {
            switchState((Constants.StartDebug) ? State.Debug : State.Wait0);
        }
        G.I.musicPlayer.volume = 0.7f;
    }

    public override void StartDialogue(int i) {
        if (i==2) {
            switchState(State.Dialogue2);
        } else if (i==3) {
            switchState(State.Dialogue3);
        } else {
            throw new System.Exception(string.Format("Dialogue indexed {0} not found", i));
        }
    }

    public override void NotifyDialogueDone() {
        switch (state) {
            case State.Dialogue0:
                switchState(State.Midboss); 
                marisa.Notify(EnemyMarisaController.Notification.GameDialogue0Done);
                break;
            case State.Dialogue1:
                switchState(State.Boss);
                marisa.Notify(EnemyMarisaController.Notification.GameDialogue1Done);
                break;
            case State.Dialogue2: 
                marisa.Notify(EnemyMarisaController.Notification.GameDialogue2Done);
                this.state = State.Boss;
                break;
            case State.Dialogue3:
                if (StaticStorage.Instance.practice == "None") {
                    StaticStorage.Instance.hasCurrentScore = true;
                    StaticStorage.Instance.currentScore = G.I.score;
                    StaticStorage.Instance.showScoreBoard = true;
                } else {
                    StaticStorage.Instance.showScoreBoard = false;
                }
                G.I.ClearedReturnToTitle();
                break;
            default:
                Debug.LogWarning("Dialogue done received but I am not in Dialogue state.");
                break;
        }
    }

    public override void NotifyDialogueSignal(int signal) {
        switch (state) {
            case State.Dialogue0: {
                if (signal == 0) { // Spawn Marisa in Dialogue0
                    marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                } else Debug.LogWarning("processDialogueSignal received unexpected signal for Dialogue0");
                break;
            }
            case State.Dialogue1: {
                if (signal == 1) { // Spawn Marisa in Dialogue1
                    marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                } else Debug.LogWarning("processDialogueSignal received unexpected signal for Dialogue1");
                break;
            }
        }
    }

    public override void NotifyBossLeft() {
        switch (state) {
            case State.Midboss:
                switchState(State.Wait1); break;
            default:
                Debug.Log("Not in Boss state but BossLeft notified.");
                break;
        }
    }

    private void switchState(State state) {
        switch (state) {
            case State.Debug: break;
            case State.Wait0: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part0: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part1: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part2: {G.I.PlayMusic(0); timer.Reset(); robert=new SRandom("Part2"); break;}
            case State.Part3: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part4: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part5: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Dialogue0: {G.I.PlayMusic(0); timer.Reset(); G.I.StartDialogue(Dialogue.Dialogue0); break;}
            case State.Midboss: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Wait1: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part6: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part7: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Part8: {G.I.PlayMusic(0); timer.Reset(); break;}
            case State.Dialogue1: {G.I.PlayMusic(0); timer.Reset(); G.I.StartDialogue(Dialogue.Dialogue1); break;}
            case State.Boss: {timer.Reset(); break;}
            case State.Dialogue2: {timer.Reset(); G.I.StartDialogue(Dialogue.Dialogue2); break;}
            case State.Dialogue3: {timer.Reset(); G.I.StartDialogue(Dialogue.Dialogue3); break;}
            case State.PracticeWait: {
                timer.Reset(); break;
            }
            default: Debug.LogWarning("GameController switchState default block."); break;
        }
        this.state = state;
    }

    void FixedUpdate() {
        switch (state) {
            case State.Debug: {
                timer.Tick();
                if (timer.Check(120)) {
                    // G.I.StartDialogue(Dialogue.Dialogue2);
                    switchState(State.Dialogue3);
                    // marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                    // G.player.NotifyCollectedPower(800);
                    // G.player.DebugInvincible();
                } 
                else if (timer.Check(270)) {
                    marisa.Notify(EnemyMarisaController.Notification.GameDialogue1Done);
                    switchState(State.Midboss) ;
                }
                break;
            }
            case State.Wait0: {
                timer.Tick();
                if (timer.Check(180)) switchState(State.Part0);
                break;
            }
            case State.Part0: {
                timer.Tick();
                for (int i=1; i<=10; i++) 
                    if (timer.Check(i*10)) Instantiate(enemyPart0Prefab).GetComponent<EnemyLogic0>().SetParameters(
                        new Vector2(-3, 4.9f), new Vector2(3.8f - i*0.76f, -4.8f), true, 
                        (uint)(40-i*3), 1.50f - i*0.08f, 1f, 0.08f, EnemyLogic0.ColorSet.BLUE, 
                        i%2==0 ? ItemType.Point : ItemType.Power);
                for (int i=1; i<=10; i++) 
                    if (timer.Check(i*10+80)) Instantiate(enemyPart0Prefab).GetComponent<EnemyLogic0>().SetParameters(
                        new Vector2(3, 4.9f), new Vector2(-3.8f + i*0.76f, -4.8f), false, 
                        (uint)(40-i*3), 1.50f - i*0.08f, 1f, 0.08f, EnemyLogic0.ColorSet.RED, 
                        i%2==0 ? ItemType.Point : ItemType.Power);
                for (int i=1; i<=10; i++) 
                    if (timer.Check(i*10+200)) {
                        Instantiate(enemyPart0Prefab).GetComponent<EnemyLogic0>().SetParameters(
                        new Vector2(-3, 4.9f), new Vector2(3.8f - i*0.76f, -4.8f), true, 
                        (uint)(40-i*3), 1.50f - i*0.08f, 1f, 0.08f, EnemyLogic0.ColorSet.BLUE,
                        i%2==0 ? ItemType.Point : ItemType.Power);
                        Instantiate(enemyPart0Prefab).GetComponent<EnemyLogic0>().SetParameters(
                        new Vector2(3, 4.9f), new Vector2(-3.8f + i*0.76f, -4.8f), false, 
                        (uint)(40-i*3), 1.50f - i*0.08f, 1f, 0.08f, EnemyLogic0.ColorSet.RED,
                        i%2==0 ? ItemType.Point : ItemType.Power);
                    }
                if (timer.Check(460)) switchState(State.Part1);
                break;
            }
            case State.Part1: {
                timer.Tick();
                for (int i=0; i<5; i++) if (timer.Check(i*60+10)) 
                    Instantiate(enemyPart1Prefab).GetComponent<EnemyLogic1>().SetParameters(2-0.4f*i, 0.05f, 30, 30, i==4 ? ItemType.BigPower : ItemType.Point);
                for (int i=0; i<5; i++) if (timer.Check(i*60+40)) 
                    Instantiate(enemyPart1Prefab).GetComponent<EnemyLogic1>().SetParameters(-2+0.4f*i, 0.05f, 30, 30, i==4 ? ItemType.BigPower : ItemType.Point);
                if (timer.Check(280)) switchState(State.Part2);
                break;
            }
            case State.Part2: {
                timer.Tick();
                for (int i=0; i<16; i++) {
                    if (timer.Check(i*20 + 150)) {
                        Instantiate(enemyPart2Prefab).GetComponent<EnemyLogic2>().SetParameters(
                            random.Int(), 
                            ((i%2==0) ? 1 : -1) * robert.Float() * Constants.StageVisibleX * 0.8f,
                            (i/2%2==0) ? EnemyLogic2.ColorSet.Blue: EnemyLogic2.ColorSet.Cyan,
                            ItemType.Power
                        );
                        if (i==15) switchState(State.Part3);
                    }
                }
                break;
            }
            case State.Part3: {
                timer.Tick();
                for (int i=0; i<40; i++) {
                    if (timer.Check(i*8 + 90)) {
                        var obj = Instantiate(i%2==0 ? enemyPart3PrefabA : enemyPart3PrefabB);
                        var logic = obj.GetComponent<EnemyLogic3>();
                        var color = (i%2==0) ? EnemyLogic3.ColorSet.A : EnemyLogic3.ColorSet.B;
                        ItemType item =  (i%3==0) ? ItemType.Power : ItemType.Point;
                        logic.SetParameters(random.Int(), i%2==0, Constants.StageVisibleY * 0.8f, color, item);
                        if (i==39) switchState(State.Part4);
                    }
                }
                break;
            }
            case State.Part4: {
                timer.Tick();
                for (int i=1; i<=10; i++) 
                    if (timer.Check(i*10+1)) {
                        Instantiate(enemyPart4Prefab).GetComponent<EnemyLogic0>().SetParameters(
                            new Vector2(-3, 4.9f), new Vector2(3.8f - i*0.76f, -4.8f), true, 
                            (uint)(40-i*3), 1.50f - i*0.08f, 1f, 0.08f, EnemyLogic0.ColorSet.BLUE,
                            ItemType.Point);
                        Instantiate(enemyPart4Prefab).GetComponent<EnemyLogic0>().SetParameters(
                            new Vector2(3, 4.9f), new Vector2(-3.8f + i*0.76f, -4.8f), false, 
                            (uint)(40-i*3), 1.50f - i*0.08f, 1f, 0.08f, EnemyLogic0.ColorSet.RED,
                            ItemType.Point);
                    }
                if (timer.Check(160)) switchState(State.Part5);
                break;
            }
            case State.Part5: {
                timer.Tick();
                if (timer.Check(10)) {
                    var logic = Instantiate(enemyPart5Prefab).GetComponent<EnemyLogic4>();
                    logic?.SetParameters();
                }
                if (timer.Check(1000)) switchState(State.Dialogue0);
                break;
            }
            case State.Dialogue0: {
                break;
            }
            case State.Midboss: {
                timer.Tick();
                break;
            }
            case State.Wait1: {timer.Tick(); if (timer.Check(60)) switchState(State.Part6); break;}
            case State.Part6: {
                timer.Tick();
                for (uint i=0; i<16; i++) {
                    if (timer.Check(10 + i*45)) {
                        Instantiate(enemyPart6Prefab).GetComponent<EnemyLogic5>()
                            .SetParameters((100f) * Constants.p2u, i%2==0, (i<14) ? ItemType.Point : ItemType.BigPower);
                    }
                }
                if (timer.Check(10 + 15*45 + 360)) switchState(State.Part7);
                break;
            }
            case State.Part7: {
                timer.Tick();
                for (uint i=0; i<8; i++) {
                    if (timer.Check(10 + i*60)) {
                        Instantiate(enemyPart7PrefabA).GetComponent<EnemyLogic6>()
                            .SetParameters(i, C.StageVisibleY - 60 * C.p2u, false, (15 + i*24) % EnemyLogic6.SHOOT_INTERVAL);
                    }
                }
                if (timer.Check(1)) {
                    Instantiate(enemyPart7PrefabB).GetComponent<EnemyLogic7>()
                        .SetParameters(0);
                }
                if (timer.Check(840)) switchState(State.Part8);
                break;
            }
            case State.Part8: {
                timer.Tick();
                if (timer.Check(60)) {
                    Instantiate(enemyPart8Prefab).GetComponent<EnemyLogic8>().SetParameters(-100 * C.PixelToUnit, true, (uint)C.ColorE16.Green);
                    Instantiate(enemyPart8Prefab).GetComponent<EnemyLogic8>().SetParameters(+100 * C.PixelToUnit, false, (uint)C.ColorE16.Green);
                }
                if (timer.Check(360)) {
                    Instantiate(enemyPart8Prefab).GetComponent<EnemyLogic8>().SetParameters(-270 * C.PixelToUnit, true, (uint)C.ColorE16.Cyan);
                    Instantiate(enemyPart8Prefab).GetComponent<EnemyLogic8>().SetParameters(+270 * C.PixelToUnit, false, (uint)C.ColorE16.Cyan);
                }
                if (timer.Check(1200)) {
                    switchState(State.Dialogue1);
                }
                break;
            }
            case State.Dialogue1: {
                break;
            }
            case State.Boss: {
                timer.Tick(); break;
            }
            case State.Dialogue2: {
                break;
            }
            case State.Dialogue3: {
                break;
            }
            case State.PracticeWait: {
                timer.Tick();
                string practice = StaticStorage.Instance.practice;
                uint p = 180;
                if (practice.Contains("boss") || practice.Contains("Boss")) p = 10;
                if (timer.Check(p)) switchPracticeState(); break;
            }
            default: Debug.LogWarning("Stage FixedUpdate default block"); break;
        }
    }

    void switchPracticeState() {
        string practice = StaticStorage.Instance.practice;
        switch (practice) {
            case "First Half Part 0": switchState(State.Part0); G.player.NotifyCollectedPower(0); break;
            case "First Half Part 1": switchState(State.Part1); G.player.NotifyCollectedPower(100); break;
            case "First Half Part 2": switchState(State.Part2); G.player.NotifyCollectedPower(200); break;
            case "First Half Part 3": switchState(State.Part3); G.player.NotifyCollectedPower(300); break;
            case "First Half Part 4": switchState(State.Part4); G.player.NotifyCollectedPower(350); break;
            case "First Half Part 5": switchState(State.Part5); G.player.NotifyCollectedPower(400); break;
            case "Midboss Nonspell 1": {
                marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                G.player.NotifyCollectedPower(400); switchState(State.Midboss); break;
            }
            case "Midboss Spellcard 1": {
                marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                G.player.NotifyCollectedPower(450); switchState(State.Midboss); break;
            }
            case "Midboss Spellcard 2": {
                marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                G.player.NotifyCollectedPower(500); switchState(State.Midboss); break;
            }
            case "Second Half Part 6": switchState(State.Part6); G.player.NotifyCollectedPower(550); break;
            case "Second Half Part 7": switchState(State.Part7); G.player.NotifyCollectedPower(600); break;
            case "Second Half Part 8": switchState(State.Part8); G.player.NotifyCollectedPower(700); break;
            case "Boss Nonspell 1": {
                marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                G.player.NotifyCollectedPower(750); switchState(State.Boss); break;
            }
            case "Boss Spellcard 1": 
            case "Boss Nonspell 2":
            case "Boss Spellcard 2":
            case "Boss Nonspell 3":
            case "Boss Spellcard 3":
            case "Boss Nonspell 4":
            case "Boss Spellcard 4":
            case "Boss Nonspell 5":
            case "Boss Spellcard 5":
            case "Boss Nonspell 6":
            case "Boss Spellcard 6":
            case "Boss Spellcard 7":
            case "Boss Spellcard 8":
            case "Boss Spellcard 9":
            case "Boss Spellcard 10":
            case "Boss Spellcard 11": {
                marisa = Instantiate(enemyMarisaPrefab).GetComponent<EnemyMarisaController>();
                G.player.NotifyCollectedPower(800); switchState(State.Boss); break;
            }
        }
    }


}