using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBombLogic : MonoBehaviour
{
    private static readonly int INTERVAL = 28;
    public GameObject uiBombPrefab;
    public void SetBombs(uint count) {
        uint current = (uint)(this.transform.childCount);
        if (current < count) {
            for (uint i=current; i<count; i++) {
                var child = Instantiate(uiBombPrefab, this.transform);
                var rectTranform = child.GetComponent<RectTransform>();
                rectTranform.anchoredPosition = new Vector2(i*INTERVAL, 0);
            }
        } else {
            for (int i=(int)current-1; i>=(int)count; i--) Destroy(this.transform.GetChild(i).gameObject);
        }
    }
}
