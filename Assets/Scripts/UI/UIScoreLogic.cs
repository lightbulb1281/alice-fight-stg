using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScoreLogic : MonoBehaviour
{
    private TMPro.TMP_Text text;

    private long actual;
    private long displayed;

    void Start() {
        text = GetComponent<TMPro.TMP_Text>();
        displayed = actual = 0;
    }

    public void SetScore(long score) {
        if (score < actual) {
            actual = displayed = score; updateDisplay();
        }
        actual = score;
    }

    void FixedUpdate() {
        if (actual == displayed) return;
        if (displayed > actual) {
            displayed = actual;
            Debug.LogWarning("UIScore: Displayed > Actual.");
        } else {
            long increment = System.Math.Min(System.Math.Max((actual-displayed) / 20, 20), actual-displayed);
            displayed += increment;
        }
        updateDisplay();
    }

    void updateDisplay() {
        long billions = displayed / 1_000_000_000;
        long millions = (displayed % 1_000_000_000) / 1000000; 
        long thousands = (displayed % 1000000) / 1000;
        long ones = displayed % 1000;
        text.text = string.Format("{0:D3},{1:D3},{2:D3},{3:D3}", billions, millions, thousands, ones);
    }
}
