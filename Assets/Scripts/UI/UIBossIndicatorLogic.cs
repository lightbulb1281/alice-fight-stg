using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBossIndicatorLogic : MonoBehaviour
{
    private Transform target;

    public void BindTarget(Transform target) {
        this.target = target; 
        this.GetComponent<UnityEngine.UI.Image>().enabled = true;
        updatePosition();
    }

    public void UnbindTarget() {
        target = null;
        if (this != null) this.GetComponent<UnityEngine.UI.Image>().enabled = false;
    }

    void Start() {
        target = null;
        this.GetComponent<UnityEngine.UI.Image>().enabled = false;
    }

    void updatePosition() {
        Vector2 p = Utils.WorldToScreen(target.transform.position);
        var rect = this.GetComponent<RectTransform>();
        rect.localPosition = new Vector3(p.x - 640, rect.localPosition.y, 0);
    }

    void FixedUpdate() {
        if (target) updatePosition();
    }
}
