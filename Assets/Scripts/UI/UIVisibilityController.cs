using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIVisibilityController : MonoBehaviour
{
    public uint animationLength = 20;
    public bool hideWhenPlayerIsNear_ = false;

    [Range(0f, 1f)] public float globalAlpha = 1;

    private UnityEngine.UI.Image imageRef;
    private TMPro.TMP_Text textRef;
    private float displayed;
    private float actual;

    private bool hideWhenPlayerIsNear;
    private float hideMinX, hideMinY, hideMaxX, hideMaxY;

    void Start() {
        imageRef = GetComponent<UnityEngine.UI.Image>();
        textRef = GetComponent<TMPro.TMP_Text>();
        displayed = actual = 0;
        if (imageRef) imageRef.color = Utils.ColorAlpha(imageRef.color, 0); 
        if (textRef) textRef.color = Utils.ColorAlpha(textRef.color, 0); 
        SetHideWhenPlayerIsNear(hideWhenPlayerIsNear_);
    }

    public void Show(){actual = 1;}
    public void Hide(){actual = 0;}
    public void HideImmediate() {displayed = actual = 0; refresh();}
    public void SetTarget(float c) {actual = c;}
    public void SetHideWhenPlayerIsNear(bool flag) {
        if (flag) {
            var rectTransform = this.GetComponent<RectTransform>();
            if (!rectTransform) {
                Debug.LogError("The GameObject does not contain a RectTransform"); return;
            }
            Vector3[] w = new Vector3[4]; rectTransform.GetWorldCorners(w); 
            hideMinX = w[0].x; hideMinY = w[0].y;
            hideMaxX = w[2].x; hideMaxY = w[2].y;
        }
        hideWhenPlayerIsNear = flag;
    }

    void FixedUpdate() {
        float a = actual;
        if (hideWhenPlayerIsNear) {
            Vector2 p = GameController.playerPosition;
            p = Utils.WorldToScreen(p);
            bool flag = p.x >= hideMinX && p.x <= hideMaxX && p.y >= hideMinY && p.y <= hideMaxY;
            if (flag) a -= 0.5f; if (a<0) a=0;
        }
        if (displayed < a) {displayed += 1.0f/animationLength; if (displayed > a) displayed = a; refresh();}
        else if (displayed > a) {displayed -= 1.0f/animationLength; if (displayed < a) displayed = a; refresh();}
    }

    void refresh() {
        if (imageRef) imageRef.color = Utils.ColorAlpha(imageRef.color, displayed * globalAlpha); 
        if (textRef) textRef.color = Utils.ColorAlpha(textRef.color, displayed * globalAlpha); 
    }
}
