using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoadingLogic : MonoBehaviour {

    float progress;
    public bool shown;
    float offsetX, offsetY;
    UnityEngine.UI.RawImage image;
    TMPro.TMP_Text loadingText;
    Timeout disable;

    void Awake() {
        progress=shown ? 0 : 1; 
        offsetX = offsetY = 0;
    }

    void Start() {
        // print("Start");
        image = this.GetComponentInChildren<UnityEngine.UI.RawImage>();
        image.material = new Material(image.material); // create a copy of the material
        image.material.SetVector("_TexOffset", new Vector4(offsetX, offsetY, 0, 0));
        image.material.SetFloat("_HideProgress", progress);
        loadingText = GetComponentInChildren<TMPro.TMP_Text>();
        loadingText.color = Utils.ColorAlpha(loadingText.color, 1-progress);
        disable = null;
    }

    public void Restart(bool shown) {
        this.gameObject.SetActive(true);
        this.enabled = true;
        this.shown = shown;
        progress = shown ? 1 : 0;
        loadingText.color = Utils.ColorAlpha(loadingText.color, 1-progress);
        image.material = new Material(image.material); // create a copy of the material
        image.material.SetVector("_TexOffset", new Vector4(offsetX, offsetY, 0, 0));
        image.material.SetFloat("_HideProgress", progress);
        disable = null;
    }

    public void DisableAfter(uint frames) {
        disable = new Timeout();
        disable.Reset(frames);
    }

    public void Show() {
        shown=true;
    }

    public void Hide() {
        shown=false;
    }

    void FixedUpdate() {
        if (disable != null) {
            if (disable.Tick()) this.gameObject.SetActive(false);
        }
        // float old = progress;
        if (shown) progress -= 0.02f;
        else progress += 0.02f;
        progress = Mathf.Clamp(progress, 0, 1);
        // if (progress == old) return;
        offsetX += 0.001f; offsetY += 0.0012f;
        image.material.SetVector("_TexOffset", new Vector4(offsetX, offsetY, 0, 0));
        image.material.SetFloat("_HideProgress", progress);
        loadingText.color = Utils.ColorAlpha(loadingText.color, 1-progress);
    }

    void OnDestroy() {
        StaticStorage.SaveIsLoading(true);
        StaticStorage.SaveLoadingOffset(offsetX, offsetY);
    }
    
    public void LoadOffsets() {
        // print("Load offsets");
        var k = StaticStorage.LoadLoadingOffset();
        offsetX = k.x; offsetY = k.y;
        image.material.SetVector("_TexOffset", new Vector4(offsetX, offsetY, 0, 0));
    }

}