using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDialogueTachieController : MonoBehaviour
{
    public Vector2 positionA;
    public Vector2 positionB;
    public uint animationLength;
    public List<Sprite> sprites;

    private bool state; 
    private uint interpolation;
    private RectTransform rectTransform;
    private UnityEngine.UI.Image image;

    void Start() {
        rectTransform = GetComponent<RectTransform>();
        image = this.GetComponent<UnityEngine.UI.Image>();
        interpolation = 0;
        state = false;
        rectTransform.localPosition = positionA;
        image.color = Utils.ColorAlpha(image.color, 0);
    }

    public void SetState(bool s) {this.state = s;}
    public void SetSprite(uint index) {
        if (index >= sprites.Count) {
            Debug.LogWarning("SetSprite called with invalid index.");
        } else image.sprite = sprites[(int)index];
    }
    public void SetSprite(Sprite sprite) {image.sprite = sprite;}

    void FixedUpdate() {
        // position, shade
        if (state && interpolation < animationLength) {
            interpolation++; 
        } else if (!state && interpolation > 0) {
            interpolation--; 
        } 
        float progress = (float)(interpolation) / animationLength;
        Vector2 position = Vector2.Lerp(positionA, positionB, state ? Utils.CurveSin(progress) : Utils.CurveNSin(progress));
        rectTransform.localPosition = position;
        float lightness = Mathf.Lerp(0.5f, 1, progress);
        image.color = new Color(lightness, lightness, lightness, image.color.a);
        // alpha
    }
}
