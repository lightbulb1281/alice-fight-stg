using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerSpellcardTachieAnimation : MonoBehaviour
{
    private const uint LENGTH = 90;
    private bool playing;
    private uint timer;

    void Start() {
        playing = false;
        GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,0);
        this.gameObject.SetActive(false);
    }

    public void Play() {
        playing = true; timer = 0;
        this.gameObject.SetActive(true);
    }

    void FixedUpdate() {
        if (!playing) return;
        timer++;
        float progress = (float)(timer) / LENGTH;
        float alpha = Mathf.Lerp(0.5f, 0f, 2*Mathf.Abs(progress - 0.5f));
        GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,alpha);
        var t = GetComponent<RectTransform>();
        t.localPosition = new Vector3(-392, Mathf.Lerp(-306, 0, progress), 0);
        if (timer == LENGTH) {
            this.gameObject.SetActive(false);
            playing = false;
            GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,0);
        }
    }
}
