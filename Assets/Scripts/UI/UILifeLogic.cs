using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILifeLogic : MonoBehaviour
{
    private static readonly int INTERVAL = 28;
    public GameObject uiLifePrefab;
    public void SetLives(uint count) {
        uint current = (uint)(this.transform.childCount);
        if (current < count) {
            for (uint i=current; i<count; i++) {
                var child = Instantiate(uiLifePrefab, this.transform);
                var rectTranform = child.GetComponent<RectTransform>();
                rectTranform.anchoredPosition = new Vector2(i*INTERVAL, 0);
            }
        } else {
            for (int i=(int)current-1; i>=(int)count; i--) Destroy(this.transform.GetChild(i).gameObject);
        }
    }
}
