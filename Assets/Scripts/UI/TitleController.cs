using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour
{

    private static readonly Color COLOR_PLAIN = new Color(0.40f, 0.84f, 1.00f);
    private static readonly Color COLOR_SELECTED = new Color(1.00f, 0.84f, 0.40f);
    private static readonly TMPro.VertexGradient GRADIENT_PLAIN = new TMPro.VertexGradient
        (COLOR_PLAIN, COLOR_PLAIN, Color.white, Color.white);
    private static readonly TMPro.VertexGradient GRADIENT_SELECTED = new TMPro.VertexGradient
        (COLOR_SELECTED);
    
    public RectTransform[] uiTitleMenu;
    public RectTransform uiLoading;
    public RectTransform uiScoreboard;
    public GameObject uiScoreboardEntryPrefab;
    public RectTransform uiPracticeOptionsParent;
    public GameObject uiPracticeOptionPrefab;
    private List<RectTransform> uiPracticeOptions;
    private int practiceSelected = 0;
    private int optionsSelected = 0;
    public MusicPlayer musicPlayer;
    
    [Header("Options panel")]
    public RectTransform uiOptions;
    public RectTransform uiOptionsLanguage;
    public RectTransform uiOptionsLanguage_zhCN;
    public RectTransform uiOptionsLanguage_jp;
    public RectTransform uiOptionsSE;
    public RectTransform uiOptionsSE_on;
    public RectTransform uiOptionsSE_off;
    public RectTransform uiOptionsBGM;
    public RectTransform uiOptionsBGM_on;
    public RectTransform uiOptionsBGM_off;

    private enum State {
        Scoreboard, Title, Practice, Options
    }

    private enum TitleMenu {
        Start, Practice, Scoreboard, Options, Quit
    }
    TitleMenu titleMenuSelected;
    private bool loading;
    State state;

    void updateTitleMenuDisplay() {
        int k = (int)titleMenuSelected;
        for (int i=0; i<uiTitleMenu.Length; i++) {
            var text = uiTitleMenu[i].GetComponent<TMPro.TMP_Text>();
            if (i!=k) text.colorGradient = GRADIENT_PLAIN;
            else text.colorGradient = GRADIENT_SELECTED;
        }
    }

    void updatePracticeOptionsDisplay() {
        int k = practiceSelected;
        for (int i=0; i<uiPracticeOptions.Count; i++) {
            var text = uiPracticeOptions[i].GetComponent<TMPro.TMP_Text>();
            if (i!=k) text.colorGradient = GRADIENT_PLAIN;
            else text.colorGradient = GRADIENT_SELECTED;
        }
    }

    // void updateLocaleDisplay() {
    //     string loc = StaticStorage.GetLocaleName();
    //     if (loc == "zh-CN") {
    //         uiLocales[0].GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
    //         uiLocales[1].GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
    //     } else {
    //         uiLocales[0].GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
    //         uiLocales[1].GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
    //     }
    // }
    
    void updateOptionsMenuDisplay() {
        string loc = StaticStorage.GetLocaleName();
        if (loc == "zh-CN") {
            uiOptionsLanguage_zhCN.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
            uiOptionsLanguage_jp.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        } else {
            uiOptionsLanguage_zhCN.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
            uiOptionsLanguage_jp.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
        }
        bool bgm = PlayerPrefs.GetInt("BGM", 1) != 0;
        if (bgm) {
            uiOptionsBGM_on .GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
            uiOptionsBGM_off.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        } else {
            uiOptionsBGM_off.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
            uiOptionsBGM_on .GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        }
        bool se = PlayerPrefs.GetInt("SE", 1) != 0;
        if (se) {
            uiOptionsSE_on .GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
            uiOptionsSE_off.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        } else {
            uiOptionsSE_off.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
            uiOptionsSE_on .GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        }
        uiOptionsLanguage.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        uiOptionsBGM.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        uiOptionsSE.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_PLAIN;
        if (optionsSelected == 0) {
            uiOptionsLanguage.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
        } else if (optionsSelected == 1) {
            uiOptionsSE.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
        } else {
            uiOptionsBGM.GetComponent<TMPro.TMP_Text>().colorGradient = GRADIENT_SELECTED;
        }
    }

    string getDateString() {
        System.DateTime dt = System.DateTime.Now;
        return dt.ToString("yyyy.MM.dd");
    }

    void generateScoreboard() {
        PlayerdataScoreboard scoreboard = PlayerdataScoreboard.Load();
        int k = -1;
        if (StaticStorage.Instance.hasCurrentScore) 
            k = scoreboard.TryInsertEntry(getDateString(), StaticStorage.Instance.currentScore);
        float y = 220; // -64;
        var highlightGradient = new TMPro.VertexGradient(
            Color.yellow
        );
        for (int i=0; i<10; i++) {
            var r = Instantiate(uiScoreboardEntryPrefab).GetComponent<RectTransform>();
            r.SetParent(uiScoreboard);
            r.localPosition = new Vector3(0, y, r.localPosition.z);
            var p0 = r.transform.GetChild(0).GetComponent<TMPro.TMP_Text>();
            var p1 = r.transform.GetChild(1).GetComponent<TMPro.TMP_Text>();
            p0.text = scoreboard.scores[i].Item1;
            p1.text = string.Format("{0}", scoreboard.scores[i].Item2);
            if (i==k) {
                p0.colorGradient = highlightGradient;
                p1.colorGradient = highlightGradient;
            }
            y -= 56;
        }
        if (k==-1 && StaticStorage.Instance.hasCurrentScore) {
            var r = Instantiate(uiScoreboardEntryPrefab).GetComponent<RectTransform>();
            r.SetParent(uiScoreboard);
            r.localPosition = new Vector3(0, y, r.localPosition.z);
            var p0 = r.transform.GetChild(0).GetComponent<TMPro.TMP_Text>();
            var p1 = r.transform.GetChild(1).GetComponent<TMPro.TMP_Text>();
            p0.text = getDateString();
            p1.text = string.Format("{0}", StaticStorage.Instance.currentScore);
            p0.colorGradient = highlightGradient;
            p1.colorGradient = highlightGradient;
        }
    }

    void switchState(State s) {
        this.state = s;
        switch (this.state) {
            case State.Scoreboard: {
                musicPlayer.Play(1);
                uiScoreboard.gameObject.SetActive(true);
                uiOptions.gameObject.SetActive(false);
                uiPracticeOptionsParent.gameObject.SetActive(false);
                foreach (var p in uiTitleMenu) p.gameObject.SetActive(false);
                generateScoreboard();
                break;
            }
            case State.Practice: {
                foreach (var p in uiTitleMenu) p.gameObject.SetActive(false);
                uiPracticeOptionsParent.gameObject.SetActive(true);
                break;
            }
            case State.Title: {
                musicPlayer.Play(0);
                uiPracticeOptionsParent.gameObject.SetActive(false);
                uiScoreboard.gameObject.SetActive(false);
                uiOptions.gameObject.SetActive(false);
                foreach (var p in uiTitleMenu) p.gameObject.SetActive(true);
                updateTitleMenuDisplay();
                break;
            }
            case State.Options: {
                foreach (var p in uiTitleMenu) p.gameObject.SetActive(false);
                uiOptions.gameObject.SetActive(true);
                optionsSelected = 0;
                updateOptionsMenuDisplay();
                break;
            }
        }
    }

    void reloadAllLocales() {
        var r = GameObject.FindObjectsOfType<LocalizationAdapter>();
        foreach (var p in r) {
            print(p.gameObject);
            p.Reload();
        }
    }

    void generatePracticeOptions() {
        for (int k=uiPracticeOptionsParent.childCount-1; k>0; k--) {
            Destroy(uiPracticeOptionsParent.GetChild(k).gameObject);
        }
        var p = StageLogic0.GetPracticeNames();
        float x = 130, y = 376; int i = 0;
        uiPracticeOptions = new List<RectTransform>();
        for (int k=0; k<p.Count; k++) {
            var obj = Instantiate(uiPracticeOptionPrefab);
            obj.transform.SetParent(uiPracticeOptionsParent);
            var rect = obj.GetComponent<RectTransform>();
            rect.localPosition = new Vector3(x, y, 0);
            string name = string.Format("PracticeNames.{0}", k);
            obj.GetComponent<LocalizationAdapter>().Reset(name);
            uiPracticeOptions.Add(rect);
            y -= 35;
            i++;
            if (i%20==0) {x += 250; y = 376;}
        }
        practiceSelected = 0;
        updatePracticeOptionsDisplay();
        uiPracticeOptionsParent.gameObject.SetActive(false);
    }

    void Awake() {
        // CrossSceneStorage.TryInitialize();
        titleMenuSelected = TitleMenu.Start; 
        generatePracticeOptions();
    }

    void Start() {
        loading = false;
        if (StaticStorage.LoadIsLoading()) {
            print("IsLoading");
            uiLoading.gameObject.SetActive(true);
            var p = uiLoading.GetComponent<UILoadingLogic>();
            p.LoadOffsets();
            p.Restart(false);
        }
        if (StaticStorage.Instance.showScoreBoard) {
            switchState(State.Scoreboard);
        } else {
            switchState(State.Title);
        }
    }

    void startGame() {
        musicPlayer.Stop();
        uiLoading.gameObject.SetActive(true); loading = true;
        uiLoading.GetComponent<UILoadingLogic>().Show();
        StartCoroutine(loadStageboardScene());
    }

    void Update() {
        if (loading) return;
        if (state == State.Title) {
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                if (titleMenuSelected != TitleMenu.Quit) {titleMenuSelected++; updateTitleMenuDisplay();}
            } 
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                if (titleMenuSelected != TitleMenu.Start) {titleMenuSelected--; updateTitleMenuDisplay();}
            }
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Return)) {
                if (titleMenuSelected == TitleMenu.Start) {
                    StaticStorage.Instance.practice = "None";
                    startGame();
                } else if (titleMenuSelected == TitleMenu.Scoreboard) {
                    StaticStorage.Instance.hasCurrentScore = false;
                    switchState(State.Scoreboard);
                } else if (titleMenuSelected == TitleMenu.Practice) {
                    switchState(State.Practice);
                } else if (titleMenuSelected == TitleMenu.Quit) {
                    Debug.Log("Application quits.");
                    Application.Quit();
                } else if (titleMenuSelected == TitleMenu.Options) {
                    switchState(State.Options);
                }
            }
            if (Input.GetKeyDown(KeyCode.R)) {
                string current = StaticStorage.GetLocaleName();
                string next;
                if (current == "zh-CN") next = "ja";
                else next = "zh-CN";
                StaticStorage.LoadLocale(next);
                PlayerPrefs.SetString("locale", next);
                reloadAllLocales();

            }
        } else if (state == State.Practice) {
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                practiceSelected = (practiceSelected + 1) % uiPracticeOptions.Count;
                updatePracticeOptionsDisplay();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                practiceSelected = (practiceSelected - 1 + uiPracticeOptions.Count) % uiPracticeOptions.Count;
                updatePracticeOptionsDisplay();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                if (practiceSelected >= 20) practiceSelected -= 20;
                updatePracticeOptionsDisplay();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                if (practiceSelected + 20 < uiPracticeOptions.Count) practiceSelected += 20;
                updatePracticeOptionsDisplay();
            }
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Return)) {
                StaticStorage.Instance.practice = StageLogic0.GetPracticeNames()[practiceSelected];
                startGame();
            }
            if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Escape)) {
                switchState(State.Title);
            }
        } else if (state == State.Scoreboard) {
            if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Escape)) {
                int p = uiScoreboard.childCount;
                for (int i=1; i<p; i++) Destroy(uiScoreboard.GetChild(i).gameObject);
                switchState(State.Title);
            }
        } else if (state == State.Options) {
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                optionsSelected = (optionsSelected + 2) % 3;
                updateOptionsMenuDisplay();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                optionsSelected = (optionsSelected + 1) % 3;
                updateOptionsMenuDisplay();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)) {
                if (optionsSelected == 0) {
                    string current = StaticStorage.GetLocaleName();
                    string next;
                    if (current == "zh-CN") next = "ja";
                    else next = "zh-CN";
                    StaticStorage.LoadLocale(next);
                    PlayerPrefs.SetString("locale", next);
                    reloadAllLocales();
                    updateOptionsMenuDisplay();
                } else if (optionsSelected == 1) {
                    bool se = PlayerPrefs.GetInt("SE", 1) != 0;
                    se = !se;
                    PlayerPrefs.SetInt("SE", se ? 1 : 0);
                    updateOptionsMenuDisplay();
                } else if (optionsSelected == 2) {
                    bool bgm = PlayerPrefs.GetInt("BGM", 1) != 0;
                    bgm = !bgm;
                    PlayerPrefs.SetInt("BGM", bgm ? 1 : 0);
                    musicPlayer.SetEnabled(bgm);
                    updateOptionsMenuDisplay();
                }
            }
            if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Escape)) {
                switchState(State.Title);
            }
        }
    }

    IEnumerator loadStageboardScene() {
        yield return new WaitForSeconds(2);
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
        AsyncOperation asyncLoad = UnityEngine.SceneManagement
            .SceneManager.LoadSceneAsync("StageboardScene");
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }
}
