using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAliceFaces : MonoBehaviour
{
    public Sprite[] sprites;

    public void SetCount(uint n) {
        if (n>=3) this.GetComponent<UnityEngine.UI.Image>().sprite = sprites[3];
        else this.GetComponent<UnityEngine.UI.Image>().sprite = sprites[n];
    }
}