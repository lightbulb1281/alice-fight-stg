using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class UIPausePanelController: MonoBehaviour {
    
    public TMPro.TMP_Text title;
    public TMPro.TMP_Text[] menus;

    enum Mode {
        Pause,
        Gameover
    }

    private uint selection;
    Mode mode;

    void refreshMenuDisplay() {
        TMPro.VertexGradient plain = new TMPro.VertexGradient(new Color(0.8f, 0.8f, 0.8f));
        TMPro.VertexGradient sel = new TMPro.VertexGradient(new Color(1, 0.7f, 0.7f), new Color(1, 0.7f, 0.7f), Color.white, Color.white);
        for (int i=0; i<menus.Length; i++) {
            if (i == selection) menus[i].colorGradient = sel;
            else menus[i].colorGradient = plain;
        }
    }

    void Awake() {
        selection = 0;
    }

    void Start() {
        refreshMenuDisplay();
        // setPauseTexts();
    }

    void Update() {
        uint previousSelection = selection;
        if (Input.GetKeyDown(KeyCode.DownArrow)) selection = (uint)((selection + 1) % menus.Length);
        if (Input.GetKeyDown(KeyCode.UpArrow)) selection = (uint)((selection - 1 + menus.Length) % menus.Length);
        if (selection != previousSelection) refreshMenuDisplay();
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Z)) {
            if (selection == 0) {
                G.paused = false;
                if (mode == Mode.Gameover) {
                    G.I.lives = 3;
                    G.I.score = 0;
                }
            } else if (selection == 1) {
                G.I.Restart();
            } else if (selection == 2) {
                if (mode == Mode.Pause) {
                    StaticStorage.Instance.showScoreBoard = false;
                }
                G.I.StopMusic();
                G.I.uiLoading.Restart(true);
                G.I.escapeEnabled = false;
                StartCoroutine(loadTitleScene());
            }
        }
    }

    public void setPauseTexts() {
        // TODO: localization
        // print("Set pause texts");
        title.text = StaticStorage.GetLocaleString("Pause.Title1");
        menus[0].text = StaticStorage.GetLocaleString("Pause.Continue1");
        menus[1].text = StaticStorage.GetLocaleString("Pause.Restart1");
        menus[2].text = StaticStorage.GetLocaleString("Pause.ReturnToTitle1");
        mode = Mode.Pause;
    }

    public void setGameoverTexts() {
        // print("Set gameover texts");
        title.text = StaticStorage.GetLocaleString("Pause.Title2");
        menus[0].text = StaticStorage.GetLocaleString("Pause.Continue2");
        menus[1].text = StaticStorage.GetLocaleString("Pause.Restart2");
        menus[2].text = StaticStorage.GetLocaleString("Pause.ReturnToTitle2");
        mode = Mode.Gameover;
    }

    public IEnumerator loadTitleScene() {
        yield return new WaitForSeconds(2);
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
        AsyncOperation asyncLoad = UnityEngine.SceneManagement
            .SceneManager.LoadSceneAsync("TitleScene");
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }
}