using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDialogueSystemController : MonoBehaviour {

    // TODO: add wait command, force the dialogue system to wait for some time for next command
    public class DialogueSystemCommand {
        public enum Type {
            EnterLeft, EnterRight, ExitLeft, ExitRight, ClearTexts, SetTextMaterial,
            SetLine1, SetLine2, ResetLine1, ResetLine2,
            SetSpriteLeft, SetSpriteRight, NotifyGameController, Wait,
        }
        public Type t; public int i; public string s;
        public DialogueSystemCommand(Type t) {this.t=t;}
        public DialogueSystemCommand(Type t, int i) {this.t=t; this.i=i;}
        public DialogueSystemCommand(Type t, string s) {this.t=t; this.s=s;}
        public static List<DialogueSystemCommand> operator +(DialogueSystemCommand c1, DialogueSystemCommand c2) {
            return new List<DialogueSystemCommand>() {c1, c2};   
        }
        public static List<DialogueSystemCommand> operator +(List<DialogueSystemCommand> l, DialogueSystemCommand c) {
            var p = l; p.Add(c); return p;
        }
        public static implicit operator List<DialogueSystemCommand>(DialogueSystemCommand c) {
            return new List<DialogueSystemCommand>() {c};
        }
    };

    public Transform tachieLeft;
    public Transform tachieRight;
    public UnityEngine.UI.Image box;
    public TMPro.TMP_Text line1;
    public TMPro.TMP_Text line2;

    public List<TMPro.VertexGradient> textGradients;

    private enum State {
        Hidden, Preparing, Loaded
    }
    private State state;
    private int autoTimer;
    private Timeout waitTimeout;
    private List<List<DialogueSystemCommand>> program;
    private uint current;
    private bool nextHeld; private int skipTimer;

    void Start() {
        line1.text = ""; line2.text = ""; 
        state = State.Hidden;
        autoTimer = 0; waitTimeout = new Timeout();
    }

    public void Show(List<List<DialogueSystemCommand>> program) {
        if (program.Count >= 1) {
            tachieLeft.GetComponent<UIVisibilityController>().Show();
            tachieRight.GetComponent<UIVisibilityController>().Show();
            box.GetComponent<UIVisibilityController>().Show();
            // line1.GetComponent<UIVisibilityController>().Show();
            // line2.GetComponent<UIVisibilityController>().Show();
            this.program = program; this.state = State.Preparing; autoTimer = 0;
        } else {
            Debug.LogError("Program is not valid: length < 1.");
        }
    }

    public void Hide() {
        tachieLeft.GetComponent<UIVisibilityController>().Hide();
        tachieRight.GetComponent<UIVisibilityController>().Hide();
        tachieLeft.GetComponent<UIDialogueTachieController>().SetState(false);
        tachieRight.GetComponent<UIDialogueTachieController>().SetState(false);
        box.GetComponent<UIVisibilityController>().Hide();
        line1.GetComponent<UIVisibilityController>().Hide();
        line2.GetComponent<UIVisibilityController>().Hide();
        line1.text = ""; line2.text = "";
    }

    public void Execute(DialogueSystemCommand c) {
        switch (c.t) {
            case DialogueSystemCommand.Type.ClearTexts: {
                line1.text = ""; line1.GetComponent<UIVisibilityController>().HideImmediate();
                line2.text = ""; line2.GetComponent<UIVisibilityController>().HideImmediate();
                break;
            }
            case DialogueSystemCommand.Type.EnterLeft: {
                tachieLeft.GetComponent<UIDialogueTachieController>().SetState(true);
                break;
            }
            case DialogueSystemCommand.Type.EnterRight: {
                tachieRight.GetComponent<UIDialogueTachieController>().SetState(true);
                break;
            }
            case DialogueSystemCommand.Type.ExitLeft: {
                tachieLeft.GetComponent<UIDialogueTachieController>().SetState(false);
                break;
            }
            case DialogueSystemCommand.Type.ExitRight: {
                tachieRight.GetComponent<UIDialogueTachieController>().SetState(false);
                break;
            }
            case DialogueSystemCommand.Type.SetLine1: {
                line1.GetComponent<UIVisibilityController>().Show();
                line1.text = c.s; 
                break;
            }
            case DialogueSystemCommand.Type.SetLine2: {
                line2.GetComponent<UIVisibilityController>().Show();
                line2.text = c.s; 
                break;
            }
            case DialogueSystemCommand.Type.ResetLine1: {
                line1.GetComponent<UIVisibilityController>().HideImmediate();
                line1.GetComponent<UIVisibilityController>().Show();
                line1.text = c.s; 
                break;
            }
            case DialogueSystemCommand.Type.ResetLine2: {
                line2.GetComponent<UIVisibilityController>().HideImmediate();
                line2.GetComponent<UIVisibilityController>().Show();
                line2.text = c.s; 
                break;
            }
            case DialogueSystemCommand.Type.SetTextMaterial: {
                line1.colorGradient = textGradients[c.i];
                line2.colorGradient = textGradients[c.i];
                break;
            }
            case DialogueSystemCommand.Type.SetSpriteLeft: {
                tachieLeft.GetComponent<UIDialogueTachieController>().SetSprite((uint)c.i);
                break;
            }
            case DialogueSystemCommand.Type.SetSpriteRight: {
                tachieRight.GetComponent<UIDialogueTachieController>().SetSprite((uint)c.i);
                break;
            }
            case DialogueSystemCommand.Type.NotifyGameController: {
                GameController.Instance.Notify(GameController.Notification.DialogueSignalled, c.i);
                break;
            }
            case DialogueSystemCommand.Type.Wait: {
                waitTimeout.Reset((uint)(c.i)); break;
            }
        }
    }
    public void Execute(List<DialogueSystemCommand> commands) {
        foreach (var c in commands) Execute(c);
    }
    public bool ExecuteNext() {
        // print("ExecuteNext");
        if (program == null) {
            Debug.LogError("Program not loaded."); return false;
        } else if (current >= program.Count) {
            Debug.LogError("ID trying to execute > program length."); return false;
        } else {
            Execute(program[(int)current]);
            current++;
            autoTimer = 0; nextHeld = true; skipTimer = 5;
            return true;
        }
    }

    public void ProcessInput(bool next, bool skip) {
        if (state != State.Loaded) return;
        if (!waitTimeout.Check()) return;
        if ((!nextHeld && next) || (skipTimer == 0 && skip)) {
            if (current >= program.Count) {
                Hide(); this.state = State.Hidden;
                GameController.Instance.Notify(GameController.Notification.DialogueDone);
            } else {
                ExecuteNext(); 
            }
        }
        nextHeld = next;
    }

    void FixedUpdate() {
        if (!waitTimeout.Tick()) return;
        switch (state) {
            case State.Hidden: break;
            case State.Preparing: {
                autoTimer++;
                if (autoTimer == 30) {
                    current = 0; state = State.Loaded; 
                    ExecuteNext(); 
                }
                break;
            }
            case State.Loaded: {
                autoTimer++; if (skipTimer > 0) skipTimer--;
                if (autoTimer == 600) {
                    if (current >= program.Count) {
                        Hide(); this.state = State.Hidden;
                        GameController.Instance.Notify(GameController.Notification.DialogueDone);
                    } else {
                        ExecuteNext(); 
                    }
                }
                break;
            }
        }
    }
}
