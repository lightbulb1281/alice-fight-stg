using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utils {

  public static Vector2 VectorXY(Vector3 v){return new Vector2(v.x, v.y);}
  public static Vector3 VectorZ(Vector2 v, float z){return new Vector3(v.x, v.y, z);}

  private static int[] slowShotDamage = {30, 40, 50, 59, 67, 75, 82, 88, 95};
  public static int GetStayDamageToEnemy(string colliderTag, uint power) {
    if (colliderTag == "SlowShot") {
      return slowShotDamage[power/100];
    }
    else if (colliderTag == "FastBomb") return 400;
    else if (colliderTag == "SlowBomb") return 500;
    return 0;
  } 
  public static int GetEnterDamageToEnemy(string colliderTag, uint power) {
    if (colliderTag == "FastShot") return 300;
    return 0;
  } 
  public static int GetEnterDamageToEnemy(string colliderTag) {
    return GetEnterDamageToEnemy(colliderTag, GameObject.FindWithTag("Player").GetComponent<PlayerController>().GetPower());
  }
  public static int GetStayDamageToEnemy(string colliderTag) {
    return GetStayDamageToEnemy(colliderTag, GameObject.FindWithTag("Player").GetComponent<PlayerController>().GetPower());
  }

  public static Quaternion RandomRotation2D() {
    return Quaternion.Euler(0, 0, Random.value * 360f);
  }
  public static Vector3 RandomIdentityVector2D() {
    float r = Random.value * Mathf.PI * 2;
    return new Vector3(Mathf.Sin(r), Mathf.Cos(r), 0);
  }
  public static Color ColorAlpha(Color original, float alpha) {
    return new Color(original.r, original.g, original.b, alpha);
  }
  public static GameObject Player() {return GameObject.FindWithTag("Player");}

  public static int Sign(bool p) {
    return p ? -1 : 1;
  }

  // Curve: A function from [0,1] to [0,1]
  public static float CurveSin(float progress) {return Mathf.Sin(progress * Mathf.PI / 2);}
  public static float CurveOSin(float progress) {return Mathf.Sin(progress * Mathf.PI - Mathf.PI/2) / 2 + 0.5f;}
  public static float CurveSquare(float progress) {return progress*progress;}
  public static float CurveNSin(float progress) {return 1-CurveSin(1-progress);}
  public static float CurveNSquare(float progress) {return 1-CurveSquare(1-progress);}
  public static float CurveExp(float progress, float emin=0, float emax=1) {
    return (Mathf.Exp(emin+progress*(emax-emin)) - Mathf.Exp(emin)) / (Mathf.Exp(emax) - Mathf.Exp(emin));
  }
  public static float CurveNExp(float progress, float emin=0, float emax=1) {
    return 1-CurveExp(1-progress, emin, emax);
  }
  public static float CurvePlateau(float p, float threshold) {
    if (p>threshold) return 1;
    return (p/threshold);
  }

  // Repeated: A repeating function, T=1, namely f(x)=f(x+kT), and f(0)=0, max(f)=1
  public static float RepeatedSin(float x) {return Mathf.Sin(x*Mathf.PI*2 - Mathf.PI/2) / 2 + 0.5f;}
  public static float RepeatedLinear(float x) {
    x = x - Mathf.Floor(x);
    if (x<0.5) return x*2; else return (1-x)*2;
  }
  
  public static void SetMoveAnimation(Animator animator, bool reverseX = false) {
    animator.SetBool("Move", true);
    animator.gameObject.transform.localScale = new Vector3(reverseX ? -1 : 1, 1, 1);
  }
  public static bool OutsideStage(Vector2 pos) {
    return Mathf.Abs(pos.x) >= Constants.StageMaxX ||
      Mathf.Abs(pos.y) >= Constants.StageMaxY;
  }
  public static Vector2 UnitByTheta(float radians) {
    return UnitByAngle(radians);
  }
  public static Vector2 UnitByAngle(float radians) {
    return new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
  }
  public static Vector2 Rotate(Vector2 original, float radians) {
    float t = Mathf.Atan2(original.y, original.x);
    t += radians;
    return new Vector2(Mathf.Cos(t), Mathf.Sin(t)) * original.magnitude;
  }
  public static float Atan2(Vector2 o) {return Mathf.Atan2(o.y, o.x);}

  public static Vector2 WorldToScreen(Vector2 v) {
    return v * Constants.u2p + new Vector2(454, 480);
  }
  public static Vector2 ScreenToWorld(Vector2 v) {
    return (v - new Vector2(454, 480)) * Constants.p2u;
  }

  // Extensions
  public static Vector2 xy(this Vector3 a) {
    return new Vector2(a.x, a.y);
  }
  public static Vector3 zed(this Vector2 a, float z) {
    return new Vector3(a.x, a.y, z);
  }
  public static Vector2 flipx(this Vector2 a, bool flip = true) {
    if (flip) return new Vector2(-a.x, a.y); return a;
  }
}

public class SRandom {
  private uint s;
  private void Initialize(uint s) {
    s = (s*0x5cf58a1a); s = (s^0x624e9146);
    s = (0x1fd45f46*s*s)^(0x689e1e03*s)^(0x0e2cdbd5);
    s = (s^0x3bc70186);
    this.s = s;
  }
  public SRandom(int s) {
    Initialize((uint)s);
  }
  public SRandom(string s) {
    Initialize((uint)(s.GetHashCode()));
  }
  public uint UInt() {
    s = ((0x4771d36f*s*s) ^ 0x62c3ec82) ^ ((0x47898975*s) ^ 0x54299440) ^ 0x37656b97;
    return s;
  }
  public uint UInt(uint max) {return UInt() % max;}
  public int Int() {return (int)(UInt());}
  public int Int(int max) {
    return (Int()%max + max)%max;
  }
  public int Int(int min, int max) {
    return Int(max-min) + min;
  }
  public float Float() {return (float)(UInt()) / uint.MaxValue;}
  public float Float(float min, float max) {return Float() * (max-min) + min;} 
  public float FloatPM() {return Float(-1, 1);}
  public float PM() {return Bool() ? 1 : -1;}
  public float TwoPi() {return Float() * Mathf.PI * 2;}
  public bool Bernoulli(float p) {return Float() < p;}
  public bool Bool() {return Bernoulli(0.5f);}
  public Vector2 Unit2() {
    float angle = Float() * Mathf.PI * 2;
    return new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
  }
  public Vector2 InsideUnit2() {
    float r = Mathf.Sqrt(Float()); 
    return Unit2() * r;
  }
  public Vector2 Rotate(Vector2 original, float maxRotationInDegrees) {
    float t = Mathf.Atan2(original.y, original.x);
    t += Float(-1, 1) * maxRotationInDegrees * Mathf.Deg2Rad;
    return new Vector2(Mathf.Cos(t), Mathf.Sin(t)) * original.magnitude;
  }
  public Vector2 InsideRect(Vector2 a, Vector2 b) {
    return new Vector2(Float(a.x, b.x), Float(a.y, b.y));
  }
  public Vector2 InsideRect(float xmin, float xmax, float ymin, float ymax) {
    return new Vector2(Float(xmax, xmin), Float(ymax, ymin));
  }
  public Vector2 Noised(Vector2 v, float maxRatio) {
    return v + InsideUnit2() * v.magnitude * maxRatio;
  }
}

public class Timer {
  uint c;
  public Timer() {c=0;}
  public void Tick() {c++;}
  public uint Get() {return c;}
  public void Set(uint k) {c=k;}
  public void Reset() {c=0;}
  public float Progress(uint k) {return (float)(c)/k;}
  public float SubstractProgress(uint offset, uint k) {return (float)(c-offset)/k;}
  public bool Check(uint k) {return c==k;}
  public bool Check(int k) {return (int)(c)==k;}
  public bool CheckReset(uint k) {if (c==k) {c=0; return true;} else return false;}
  public bool TickCheck(uint k) {c++; return c==k;}
  public bool TickCheckReset(uint k) {c++; if (c==k) {c=0; return true;} else return false;}
  public int GetTimeout(uint total) {return (int)(total-c);}
  public static bool operator >(Timer t, uint k) => t.c>k;
  public static bool operator <(Timer t, uint k) => t.c<k;
  public static bool operator >=(Timer t, uint k) => t.c>=k;
  public static bool operator <=(Timer t, uint k) => t.c<=k;
}

public class Timeout {
  uint c; uint t;
  public Timeout(uint total=1) {c=total; t=total;}
  public bool Done() {return c==0;}
  public bool Tick() {if (c>=1) c--; return Done();}
  public bool TickReset(uint t) {bool r = Tick(); if (r) {this.t=t; c=t;} return r;}
  public void SetTotal(uint total) {t=total;}
  public void Reset(uint t) {this.t = c = t;}
  public float Progress() {return (float)(t-c)/t;}
  public bool Check() {return c==0;}
  public void Set(uint s) {c=s;}
  public uint Get() {return c;}
  public uint GetElapsed() {return t-c;}
}