using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic40 : BulletPhonyLogic {

    private Vector2 startPos, velocity;
    private uint count, sentCount;
    private Timer timer;
    public GameObject bulletPrefab;
    private float moveLength;
    private uint shootInterval;
    System.Action<float> callback;
    uint color;

    public void SetParameters(Vector2 startPos, Vector2 velocity, uint count, float moveLength, uint color, uint shootInterval = 1, System.Action<float> callback = null) {
        this.startPos = startPos;
        this.velocity = velocity;
        this.count = count; this.moveLength = moveLength;
        this.color = color; this.shootInterval = shootInterval;
        this.callback = callback;
    }

    void Start() {
        sentCount = 0; timer = new Timer();
        G.soundPlayer.Play("bullet0");
    }

    void FixedUpdate() {
        if (timer.TickCheckReset(shootInterval)) {
            Instantiate(bulletPrefab).GetComponent<BulletLogic38>().SetParameters(
                startPos, velocity, moveLength, color, callback
            );
            sentCount++;
            if (sentCount == count) Destroy(gameObject);
        }
    }

}