using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic21 : BulletAbstractLogic1 {

    protected Vector2 velocity;
    Timeout timer;
    float rotation;
    protected uint bulletColor;

    public void SetParameters(Vector2 startPos, Vector2 velocity, uint color) {
        base.SetParameters(startPos, color, color, true, 12, true);
        this.velocity = velocity;
        this.bulletColor = color;
    }

    protected override void Start() {
        base.Start();
        this.timer = new Timeout(20);
        this.rotation = G.random.TwoPi();
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
    }

    protected override void Move() {
        timer.Tick(); float progress = timer.Progress();
        Vector2 vel = velocity * Mathf.Lerp(4, 1, progress);
        this.transform.Translate(vel, Space.World);
        rotation += 3 * Mathf.Deg2Rad;
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
    }

    // protected override void Start() { 
    //     base.Start(); 
    // }
    // protected override void OnBulletGenerated() { 
    //     base.OnBulletGenerated();
    // }
    // protected override void GenerationMove() {
    //     base.GenerationMove();
    // }
    // protected override bool ShouldDestroy() {
    //     return base.ShouldDestroy();
    // }
    // protected override Vector2 GetMoveDirection() {
    //     return base.GetMoveDirection();
    // }

}