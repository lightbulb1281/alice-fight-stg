using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic20 : BulletSimpleLogic0 {

    protected override bool ShouldDestroy() {
        if (this.transform.position.y > 0) return false;
        return base.ShouldDestroy();
    }

}