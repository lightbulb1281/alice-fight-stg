using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic33 : BulletSimpleLogic0
{
    uint spawnLeft;
    Timeout timeout;
    GameObject shardObject;

    private readonly static uint[] colors = {
        (uint)(C.ColorE16.Blue),
        (uint)(C.ColorE16.Orange),
        (uint)(C.ColorE16.Yellow),
        (uint)(C.ColorE16.White),
    };

    public void SetParameters(Vector2 startPosition, Vector2 velocity, uint spawnLeft, GameObject shardObject)  {
        uint color = colors[spawnLeft];
        base.SetParameters(startPosition, velocity, color, color, true, 12, U.Atan2(velocity), true, 3);
        this.spawnLeft = spawnLeft; timeout = new Timeout(70);
        this.shardObject = shardObject;
    }

    protected override void OnBulletGenerated() {}

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (timeout.Tick() && spawnLeft > 0) {
            G.soundPlayer.Play("kira0");
            for (int i=0; i<4; i++) {
                Instantiate(shardObject).GetComponent<BulletLogic33>().SetParameters(
                    this.transform.position, U.Rotate(velocity, (120f + 48f * i) * Mathf.Deg2Rad), spawnLeft - 1, shardObject
                );
            }
            Destroy(gameObject);
        }
    }
}
