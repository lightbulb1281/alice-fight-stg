using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic4 : BulletAbstractLogic
{
    private const uint GENERATION_TIME = 12;

    public GameObject bulletDisplayPrefab;
    public GameObject generationDisplayPrefab;
    public GameObject explodeBulletPrefab;

    private GameObject bulletDisplay;
    private GameObject generationDisplay;

    private Vector2 startPosition, explodePosition;
    private uint moveTime;
    private uint explodeCount;
    private Vector2 centerExplodePosition;
    private float explosionSpeed;

    private enum State {
        GENERATION, MOVING
    }

    private State state;
    private uint timer;

    public enum ColorSet {
        Red, Orange
    }

    private ColorSet colorSet;
    private uint getGenerationColor() {return colorSet == ColorSet.Red ? 2u : 14u;}
    private uint getBulletColor() {return colorSet == ColorSet.Red ? 2u : 14u;}

    // bulletSpin: degrees
    public void SetParameters(Vector2 startPosition, Vector2 explodePosition, Vector2 centerExplodePosition, 
        float explosionSpeed, ColorSet colorSet,
        uint moveTime, uint explodeCount) 
    {
        this.startPosition = startPosition; this.explodePosition = explodePosition;
        this.moveTime = moveTime;
        this.explodeCount = explodeCount; this.centerExplodePosition = centerExplodePosition;
        this.explosionSpeed = explosionSpeed; this.colorSet = colorSet;
    }

    void Start() {
        base.Initialize();
        this.transform.position = new Vector3(startPosition.x, startPosition.y, Constants.DepthBullet);
        state = State.GENERATION;
        generationDisplay = Instantiate(generationDisplayPrefab, this.transform);
        generationDisplay.GetComponent<SpriteColorPicker>().SetColor(getGenerationColor());
        generationDisplay.transform.localScale = new Vector3(4, 4, 1);
        generationDisplay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        timer = 0;
    }

    void FixedUpdate() {
        switch (state) {
            case State.GENERATION: {
                timer++;
                generationDisplay.GetComponent<SpriteRenderer>().color = 
                    new Color(1, 1, 1, Mathf.Lerp(0, 0.8f, (float)timer / 12));
                float scale = Mathf.Lerp(4, 1.5f, (float)timer / 12);
                generationDisplay.transform.localScale = new Vector3(scale, scale, 1);
                if (timer == 12) {
                    Destroy(generationDisplay);
                    bulletDisplay = Instantiate(bulletDisplayPrefab, this.transform);
                    bulletDisplay.GetComponent<SpriteRenderer>().sortingOrder = GameController.Instance.GetSpriteIndex();
                    bulletDisplay.GetComponent<SpriteColorPicker>().SetColor(getBulletColor());
                    this.transform.Rotate(0, 0, 3);
                    this.state = State.MOVING;
                    this.GetComponent<Collider2D>().enabled = true;
                    timer = 0;
                }
                break;
            }
            case State.MOVING: {
                timer++; float progress = (float)(timer) / moveTime;
                this.transform.position = Vector2.Lerp(startPosition, explodePosition, Utils.CurveSin(progress));
                this.transform.Rotate(0, 0, 3);
                if (timer == moveTime) explode();
                break;
            }
        }
    }

    private void explode() {
        var game = GameController.Instance;
        Vector2 od = GameController.random.Unit2();
        Vector2 bd = (Utils.VectorXY(GameController.player.transform.position) - centerExplodePosition).normalized;
        float rot = GameController.random.Float(0, 360);
        for (int i=0; i<explodeCount; i++) {
            Vector2 d = bd * 0.04f + Utils.Rotate(od, Mathf.PI * 2 / explodeCount * i) * explosionSpeed;
            var logic = Instantiate(explodeBulletPrefab).GetComponent<BulletSimpleLogic0>();
            logic.SetParameters(this.transform.position, d, 
                getGenerationColor(), getBulletColor(),
                true, 12, rot, true, 1
            );
        }
        // TODO: create some effects here
        Destroy(gameObject);
    }

    public override void Erase(bool spawnItem) {
        GameController.Instance.InstantiateEffectErase(Utils.VectorXY(this.transform.position), getGenerationColor());
        base.Erase(spawnItem);
    }

}
