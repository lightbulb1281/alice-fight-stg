using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic7 : BulletPhonyLogic {

    public GameObject bulletPrefab;
    private Vector2 pos;
    private Timer timer;
    private bool moveRightwards;

    public void SetParameters(Vector2 pos, bool moveRightwards) {
        this.pos = pos; this.moveRightwards = moveRightwards;
    }

    void Start() {
        base.Initialize();
        this.transform.position = Utils.VectorZ(pos, Constants.DepthBullet);
        timer = new Timer();
    }

    void FixedUpdate() {
        timer.Tick();
        int n = 10, m = 3;
        for (int i=0; i<n; i++) {
            if (timer.Check(1+i)) {
                for (int j=0; j<m; j++) {
                    float angle = 360f/n*i + 360f/n/m*j;
                    // uint wait = (uint)(90 - i);
                    uint wait = (uint)(90 - (i*m+j) * 2);
                    angle = moveRightwards ? (90-angle) : (90+angle);
                    var logic = Instantiate(bulletPrefab).GetComponent<BulletLogic8>();
                    var spos = Utils.UnitByTheta(angle * Mathf.Deg2Rad) * 90f * Constants.p2u;
                    spos += Utils.VectorXY(this.transform.position);
                    logic.SetParameters(this.transform.position, spos, BulletLogic8.Preset.A, wait, 0);
                }
            }
            if (timer.Check(1+i+2*n)) {
                for (int j=0; j<m; j++) {
                    float angle = 360f/n*i + 360f/n/m*j;
                    // uint wait = (uint)(90 - i);
                    uint wait = (uint)(90 - (i*m+j) * 2);
                    angle = !moveRightwards ? (90-angle) : (90+angle);
                    var logic = Instantiate(bulletPrefab).GetComponent<BulletLogic8>();
                    var spos = Utils.UnitByTheta(angle * Mathf.Deg2Rad) * 70f * Constants.p2u;
                    spos += Utils.VectorXY(this.transform.position);
                    logic.SetParameters(this.transform.position, spos, BulletLogic8.Preset.B, wait, 0);
                }
            }
            if (timer.Check(1+i+4*n)) {
                for (int j=0; j<m; j++) {
                    float angle = 360f/n*i + 360f/n/m*j;
                    uint wait = (uint)(90 - i);
                    angle = moveRightwards ? (90-angle) : (90+angle);
                    for (int k=0; k<2; k++) {
                        var logic = Instantiate(bulletPrefab).GetComponent<BulletLogic8>();
                        var spos = Utils.UnitByTheta(angle * Mathf.Deg2Rad) * 50f * Constants.p2u;
                        spos += Utils.VectorXY(this.transform.position);
                        logic.SetParameters(this.transform.position, spos, BulletLogic8.Preset.C, wait, 0);
                    }
                }
            }
        }
        if (timer.Check(90)) Destroy(gameObject);
    }

}