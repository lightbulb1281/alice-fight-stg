using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic26 : BulletAbstractLogic1 {

    private Vector2 centerPosition;
    float velocity; float distance; float angle;
    EnemyMarisaController marisa;
    public uint color;
    public void SetParameters(EnemyMarisaController marisa, Vector2 centerPos, Vector2 startPos, float velocity) {
        base.SetParameters(startPos, color, color, true, 12);
        this.centerPosition = centerPos;
        this.velocity = velocity;
        distance = (startPos - centerPos).magnitude;
        angle = U.Atan2(startPos - centerPos);
        this.marisa = marisa;
    }

    protected override void Start() { 
        base.Start(); 
    }

    protected override void Move() {
        angle += velocity / distance;
        this.transform.position = centerPosition + distance * U.UnitByAngle(angle);
    }

    public override void Erase(bool spawnItem) {
        base.Erase(spawnItem);
        marisa.sc5PresentDustErased(distance);
    }

    protected override bool ShouldDestroy() {
        return false;
    }

    // protected override void Start() { 
    //     base.Start(); 
    // }
    // protected override void OnBulletGenerated() { 
    //     base.OnBulletGenerated();
    // }
    // protected override void GenerationMove() {
    //     base.GenerationMove();
    // }
    // protected override bool ShouldDestroy() {
    //     return base.ShouldDestroy();
    // }
    // protected override Vector2 GetMoveDirection() {
    //     return base.GetMoveDirection();
    // }

}