using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

/// <summary>
/// For lasers with a root and extending like a ray towards infinite far.
/// </summary>
public class BulletAbstractLogic2 : BulletAbstractLogic {

    private BulletDisplayLaserController display_;
    protected BulletDisplayLaserController display {get {return display_;}}
    private Transform eraseBody;
    
    private Timeout grazeTimeout;

    private float rotation_;
    protected float rotation {
        get {return rotation_;}
        set {
            rotation_ = value;
            this.transform.localRotation = Quaternion.Euler(0, 0, value * Mathf.Rad2Deg);
        }
    }

    void Awake() {
        display_ = this.transform.GetComponentInChildren<BulletDisplayLaserController>();
        eraseBody = this.transform.Find("EraseBody");
        rotation_ = 0; grazeTimeout = new Timeout(0);
    }

    protected virtual void Start() {
        base.Initialize();
        eraseBody.GetComponent<Collider2D>().enabled = true; // 从一开始就可以消除。
    }

    protected virtual void FixedUpdate() {}

    void TryGraze() {
        if (grazeTimeout.Tick()) {
            grazeTimeout.Reset(5);
            G.I.Notify(G.Notification.BulletGrazed);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "PlayerGrazebox") {
            TryGraze();
        }
    }

    protected virtual void OnTriggerStay2D(Collider2D other) {
        if (other.tag == "PlayerGrazebox") {
            TryGraze();
        }
    }

    protected void SetColliders(bool f) {
        var collider = GetComponent<Collider2D>();
        if (collider) collider.enabled = f;
    }

    public override void Erase(bool spawnItem) {
        var collider = GetComponent<Collider2D>();
        bool en = false;
        if (collider && collider.enabled) en = true;
        if (en) {
            Vector2 pos = this.transform.position; Vector2 dir = U.UnitByTheta(rotation_);
            while (!U.OutsideStage(pos)) {
                if (spawnItem) G.I.InstantiateItem(pos, ItemType.SmallPoint).GetComponent<ItemLogic>().NotifyAutoCollect();
                pos += dir * 60 * C.p2u;
            }
        }
        G.I.InstantiateItem(this.transform.position, ItemType.SmallPoint).GetComponent<ItemLogic>().NotifyAutoCollect();
        Destroy(gameObject);
    }
}

