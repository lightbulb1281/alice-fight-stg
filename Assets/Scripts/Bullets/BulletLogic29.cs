using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic29 : BulletAbstractLogic1 {

    enum MoveState {
        Spawn, Rotate, Line
    }

    float angle; bool slow; bool left; float velocity; bool spin; float rot;
    MoveState moveState; Timer moveTimer;
    public void SetParameters(Vector2 startPos, float angle, bool left, bool slow, uint color, bool spin) {
        base.SetParameters(startPos, color, color, true, 12, true);
        this.angle = angle; this.slow = slow; this.left = left; this.spin = spin;
    }

    protected override void Start() {
        base.Start();
        moveState = MoveState.Spawn;
        moveTimer = new Timer();
        velocity = slow ? 0.06f : 0.08f; rot = 0;
    }

    protected override void Move() {
        moveTimer.Tick();
        if (spin) {
            rot += 3 * Mathf.Deg2Rad;
            this.transform.localRotation = Quaternion.Euler(0, 0, rot * Mathf.Rad2Deg);
        }
        switch (moveState) {
            case MoveState.Spawn: {
                angle += 2 * Mathf.Deg2Rad * (left ? 1 : -1);
                velocity -= 0.003f;
                if (slow) {
                    if (moveTimer.CheckReset(12)) moveState = MoveState.Rotate;
                } else {
                    if (moveTimer.CheckReset(12+2)) moveState = MoveState.Rotate;
                }
                break;
            }
            case MoveState.Rotate: {
                angle += 2 * Mathf.Deg2Rad * (left ? 1 : -1);
                if (slow) {
                    if (moveTimer.CheckReset(30)) moveState = MoveState.Line;
                } else {
                    if (moveTimer.CheckReset(60)) moveState = MoveState.Line;
                }
                break;
            }
            case MoveState.Line: {
                break;
            }
        }
        this.transform.Translate(U.UnitByAngle(angle).zed(0) * velocity, Space.World);
    }

    protected override bool ShouldDestroy() {
        if (moveState == MoveState.Line) return base.ShouldDestroy();
        return false;
    }

}