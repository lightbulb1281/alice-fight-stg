using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic38 : BulletSimpleLogic0
{
    uint color;
    Vector2 startPos;
    float moveLength; 
    public GameObject shardObject;
    public System.Action<float> callback;

    public void SetParameters(Vector2 startPosition, Vector2 velocity, float moveLength, uint color, System.Action<float> callback) {
        base.SetParameters(startPosition, velocity, color, color, false, 12, U.Atan2(velocity) * Mathf.Rad2Deg, true, 0);
        this.color = color; this.startPos = startPosition; this.moveLength = moveLength;
        this.callback = callback;
    }

    protected override void OnBulletGenerated() {}

    protected override void Move() {
        base.Move();
        float dist = (this.transform.position.xy() - startPos).magnitude;
        if (dist > moveLength) {
            SRandom random = G.random;
            Vector2 breakPos = startPos + velocity.normalized * moveLength;
            for (int i=0; i<1; i++) {
                Vector2 vel = velocity * random.Float(0.5f, 1f);
                vel = U.Rotate(vel, random.Float(-Mathf.PI*0.4f, Mathf.PI*0.4f));
                Instantiate(shardObject).GetComponent<BulletSimpleLogic0>().SetParameters(
                    breakPos, vel, color, color, true, 0, 
                    random.TwoPi() * Mathf.Rad2Deg, false, 3
                );
                if (callback != null) callback(moveLength);
            }
            Destroy(gameObject);
        }
    }
    
}
