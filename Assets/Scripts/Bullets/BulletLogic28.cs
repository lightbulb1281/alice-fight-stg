using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic28 : BulletAbstractLogic1 {

    Timeout timeout; Vector2 vel; float rot;
    public void SetParameters(Vector2 startPos, Vector2 velocity, uint color, uint wait, float rotation = 0) {
        base.SetParameters(startPos, color, color, true, 12, true);
        timeout = new Timeout(); timeout.Reset(wait); this.vel = velocity; this.rot = rotation;
    }

    protected override void Start() {
        base.Start();
        this.transform.localRotation = Quaternion.Euler(0, 0, rot * Mathf.Rad2Deg);
    }

    protected override void GenerationMove() {
        // do nothing
    }

    protected override Vector2 GetMoveDirection() {
        return vel.normalized;
    }

    protected override void Move() {
        if (timeout.Tick()) this.transform.position += vel.zed(0);
    }

}