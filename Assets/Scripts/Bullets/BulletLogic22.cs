using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic22 : BulletLogic21 {

    public GameObject shardPrefab;

    private int counter;
    private Timeout flickerTimer;

    // uint getShardColor() {
    //     if (bulletColor == 1) return 2;
    //     if (bulletColor == 3) return 6;
    //     if (bulletColor == 4) return 8;
    //     if (bulletColor == 5) return 10;
    //     return 13;
    // }

    protected override void Start() {
        base.Start();
        counter = 0;
        flickerTimer = new Timeout(24);
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (flickerTimer.Tick()) {
            counter++; uint nextInterval = (uint)(24 - counter * 3);
            if (counter == 8) {
                G.soundPlayer.Play("bullet0");
                for (int i=0; i<16; i++) {
                    Instantiate(shardPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                        this.transform.position, G.random.Noised(this.velocity, 0.4f) + this.velocity * 0.1f, 
                        15, 15, true, 4, 0, true, 0
                    );
                }
                Destroy(gameObject);
            } else {
                var colorpicker = GetComponentInChildren<SpriteColorPicker>();
                if (counter % 2 == 1) colorpicker.SetColor((uint)(C.ColorE8.White));
                else colorpicker.SetColor(bulletColor);
            }
            flickerTimer.Reset(nextInterval);
        }
    }

    // protected override void Start() { 
    //     base.Start(); 
    // }
    // protected override void OnBulletGenerated() { 
    //     base.OnBulletGenerated();
    // }
    // protected override void GenerationMove() {
    //     base.GenerationMove();
    // }
    // protected override bool ShouldDestroy() {
    //     return base.ShouldDestroy();
    // }
    // protected override Vector2 GetMoveDirection() {
    //     return base.GetMoveDirection();
    // }

}