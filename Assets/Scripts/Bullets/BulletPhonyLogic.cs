using UnityEngine;

/// <summary>
/// This may have a collider (in order to be erased), 
/// but cannot be collided with player hitpoint, or get grazed.
/// </summary>
public class BulletPhonyLogic : BulletAbstractLogic {
    protected override void Initialize() {
        grazed = false;
        if (GetComponent<Collider>()) {
            if (GetComponent<Collider>().enabled) {
                Debug.LogWarning("Bullet collider2d enabled at generation. Disabling...");
                GetComponent<Collider>().enabled = false;
            }
        }
    }
    public override void Erase(bool spawnItem) {
        Destroy(gameObject);
    }
    protected override void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Eraser") Erase(false);
    }
}

/* Template

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletNameHere : BulletPhonyLogic {

    public void SetParameters() {

    }

    void Start() {

    }

    void FixedUpdate() {

    }

}

*/