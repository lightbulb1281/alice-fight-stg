using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic25 : BulletLogic17Base {

    uint color;

    public void SetParameters(Vector2 startPos, Vector2 targetPos, uint moveDuration, uint warnDuration, uint persistDuration, uint color) {
        base.SetParameters(startPos, targetPos, moveDuration, warnDuration, persistDuration);
        this.color = color;
    }

    protected override float getShootDirection() {
        return U.Atan2(G.playerPosition - this.transform.position.xy());
    }

    protected override void Start() {
        base.Start();
        var p = GetComponentsInChildren<SpriteColorPicker>();
        foreach (var item in p) {
            item.SetColor(color);
        }
    }

}

