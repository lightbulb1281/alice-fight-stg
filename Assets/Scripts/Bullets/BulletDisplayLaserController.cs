using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletDisplayLaserController: MonoBehaviour {

    private Transform root, laser;
    private float expandRatio;

    public const float ExpandSpeed = 0.03f;
    public static readonly uint ExpandTime = (uint)(Mathf.CeilToInt(1/ExpandSpeed));
    
    private enum State {
        Root, Fold, Expand
    }
    State state;

    void Awake() {
        root = this.transform.GetChild(0);
        laser = this.transform.GetChild(1);
        state = State.Root; 
    }

    public void SetColor(uint c) {
        root.GetComponent<SpriteColorPicker>().SetColor(c);
        laser.GetComponent<SpriteColorPicker>().SetColor(c);
    }

    void Start() {
        updateDisplay();
    }

    public void Fold() {
        state = State.Fold; updateDisplay();
    }

    public void FoldImmediate() {
        state = State.Fold;
        expandRatio = 0;  updateDisplay();
    }

    public void Expand() {
        state = State.Expand; updateDisplay();
    }

    public void ExpandImmediate() {
        state = State.Expand;
        expandRatio = 1; updateDisplay();
    }

    private void updateDisplay() {
        if (state == State.Root) {
            if (laser.gameObject.activeSelf) laser.gameObject.SetActive(false);
        } else {
            if (!laser.gameObject.activeSelf) laser.gameObject.SetActive(true);
            var ori = laser.localScale;
            laser.localScale = new Vector3(Mathf.Lerp(1/14f, 1, expandRatio), ori.y, ori.z);
        }
    }

    void FixedUpdate() {
        if (state == State.Fold) expandRatio = Mathf.Clamp(expandRatio - ExpandSpeed, 0, 1);
        if (state == State.Expand) expandRatio = Mathf.Clamp(expandRatio + ExpandSpeed, 0, 1);
        updateDisplay();
    }

}