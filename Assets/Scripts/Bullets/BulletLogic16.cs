using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic16 : BulletPhonyLogic {

    public GameObject bulletPrefab;

    private bool shootRightwards;
    private float y;
    private uint color;
    private Timeout timeout;
    private Timeout liveTimeout;
    SRandom robert;

    public void SetParameters(int seed, float targetY, bool shootRightwards, uint color) {
        this.shootRightwards = shootRightwards; this.y = targetY; this.color = color;
        robert = new SRandom(seed);
    }

    void Start() {
        timeout = new Timeout(); liveTimeout = new Timeout();
        timeout.Reset(0); liveTimeout.Reset(150);
    }

    void FixedUpdate() {
        if (timeout.TickReset(2)) {
            // print(y + BulletLogic15.tan * C.StageMaxX * 2);
            float displacement = liveTimeout.Progress() * 30 * C.p2u; displacement = robert.FloatPM() * displacement;
            float dy = displacement / Mathf.Cos(BulletLogic15.angle);
            Instantiate(bulletPrefab).GetComponent<BulletLogic15>().SetParameters(
                shootRightwards, y + BulletLogic15.tan * C.StageMaxX * 2 + dy, color
            );
        }
        if (liveTimeout.Tick()) Destroy(this.gameObject);
    }
}
