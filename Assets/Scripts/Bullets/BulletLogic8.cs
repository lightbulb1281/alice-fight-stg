using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic8 : BulletAbstractLogic {

    private const uint EXPAND_TIME = 30;
    
    public enum Preset {
        A, B, C
    }

    private enum State {
        Generate, Expand, Wait, Shoot
    }

    public GameObject bulletDisplayPrefab;
    public GameObject generationDisplayPrefab;

    private GameObject display;

    private Vector2 startPosition, redirectPosition;
    private uint waitBeforeShoot;
    private Preset preset;
    private State state;
    private Timer timer;
    private Vector2 velocity;
    private float shootDirectionRotation;

    private static Constants.ColorE16 getGenerationColor(Preset preset) {
        switch (preset) {
            case Preset.A: return Constants.ColorE16.Red;
            case Preset.B: return Constants.ColorE16.Orange;
            case Preset.C: return Constants.ColorE16.Yellow;
            default: goto case Preset.A;
        }
    }
    
    private static Constants.ColorE16 getBulletColor(Preset preset) {return getGenerationColor(preset);}

    public void SetParameters(Vector2 startPos, Vector2 redirectPos, Preset preset, uint waitBeforeShoot, float shootDirectionRotation) {
        this.startPosition = startPos; this.redirectPosition = redirectPos; this.preset = preset;
        this.waitBeforeShoot = waitBeforeShoot; this.shootDirectionRotation = shootDirectionRotation;
    }

    private void setVelocity() {
        switch (preset) {
            case Preset.A: 
                velocity = (GameController.playerPosition - redirectPosition
                    // + GameController.random.InsideUnit2() * 20 * Constants.p2u
                    ).normalized * 0.10f; break; 
            case Preset.B:
                velocity = (GameController.playerPosition - redirectPosition
                    + GameController.random.InsideUnit2() * 40 * Constants.p2u
                    ).normalized * 0.07f; break;
            case Preset.C:
                velocity = (redirectPosition - startPosition).normalized * 0.04f
                    + GameController.random.InsideUnit2() * 0.004f; break;
            default: goto case Preset.A;
        }
        // switch (preset) {
        //     case Preset.A: 
        //         velocity = (redirectPosition - startPosition).normalized * 0.04f
        //             + GameController.random.InsideUnit2() * 0.000f; break;
        //     case Preset.B:
        //         velocity = (redirectPosition - startPosition).normalized * 0.04f
        //             + GameController.random.InsideUnit2() * 0.002f; break;
        //     case Preset.C:
        //         velocity = (redirectPosition - startPosition).normalized * 0.04f
        //             + GameController.random.InsideUnit2() * 0.004f; break;
        //     default: goto case Preset.A;
        // }
        velocity = Utils.Rotate(velocity, shootDirectionRotation);
    }

    void Start() {
        state = State.Generate; timer = new Timer(); 
        this.transform.position = Utils.VectorZ(startPosition, Constants.DepthBullet);
        display = Instantiate(generationDisplayPrefab, this.transform);
        display.GetComponent<SpriteColorPicker>().SetColor(getGenerationColor(preset));
        display.transform.localScale = new Vector3(4, 4, 1);
        this.transform.localRotation = Quaternion.Euler(0, 0, Utils.Atan2(redirectPosition - startPosition) * Mathf.Rad2Deg);
        display.GetComponent<SpriteRenderer>().color = new Color(1,1,1,0);
    }

    void FixedUpdate() {
        switch (state) {
            case State.Generate: {
                timer.Tick(); float progress = timer.Progress(12);
                float scale = Mathf.Lerp(4, 1.5f, progress);
                display.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, progress * 0.8f);
                display.transform.localScale = new Vector3(scale, scale, 1);
                this.transform.position = 
                    Utils.VectorZ(Vector2.Lerp(startPosition, redirectPosition, Utils.CurveSin(timer.Progress(EXPAND_TIME))), Constants.DepthBullet);
                if (timer.Check(12)) {
                    Destroy(display); display = Instantiate(bulletDisplayPrefab, this.transform);
                    display.GetComponent<SpriteRenderer>().sortingOrder = GameController.Instance.GetSpriteIndex();
                    display.GetComponent<SpriteColorPicker>().SetColor(getBulletColor(preset));
                    this.state = State.Expand;
                    var collider = this.GetComponent<Collider2D>();
                    if (collider) collider.enabled = true; 
                }
                break;
            }
            case State.Expand: {
                timer.Tick(); 
                this.transform.position = 
                    Utils.VectorZ(Vector2.Lerp(startPosition, redirectPosition, Utils.CurveSin(timer.Progress(EXPAND_TIME))), Constants.DepthBullet);
                if (timer.Check(EXPAND_TIME)) {this.state = State.Wait; timer.Reset();}
                break;
            }
            case State.Wait: {
                if (timer.TickCheck(waitBeforeShoot)) {
                    this.state = State.Shoot; setVelocity();
                    this.transform.localRotation = Quaternion.Euler(0, 0, Utils.Atan2(velocity) * Mathf.Rad2Deg);
                }
                break;
            }
            case State.Shoot: {
                // GameController.soundPlayer.Play("bullet1");
                this.transform.Translate(velocity, Space.World); 
                var x = Mathf.Abs(this.transform.position.x); var y = Mathf.Abs(this.transform.position.y);
                if (x > Constants.StageMaxX || y > Constants.StageMaxY) Destroy(gameObject);
                break;
            }
        }
    }

    public override void Erase(bool spawnItem) {
        Vector2 d = (this.state == State.Shoot) ? velocity.normalized : (redirectPosition - startPosition).normalized;
        GameController.Instance.InstantiateEffectEraseGeneral(
            this.transform.position, generationDisplayPrefab, (uint)getGenerationColor(preset), Utils.Atan2(d)
        );
        base.Erase(spawnItem);
    }

}