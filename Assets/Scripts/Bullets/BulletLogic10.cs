using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic10 : BulletAbstractLogic1 {

    private BulletLogic9 reference;
    private Vector2 referenceOrigin;
    private float distance, rotation;

    public void SetParameters(BulletLogic9 reference, float distance, float rotation) {
        this.distance = distance;  this.reference = reference;
        this.rotation = rotation;
        base.SetParameters(
            getPosition(), 
            (uint)(C.ColorE16.Blue), (uint)(C.ColorE16.Blue),
            true, 12);
    }

    public void SetReferenceOrigin(Vector2 r) {referenceOrigin = r;}

    private Vector2 getPosition() {
        Vector2 r = referenceOrigin;
        r += distance * U.UnitByTheta(rotation);
        return r;
    }

    protected override void Move() {
        rotation += BulletLogic9.ROTATE_SPEED;
        this.transform.position = U.VectorZ(getPosition(), C.DepthBullet);
    }

    protected override bool ShouldDestroy() {
        return this.transform.position.y < -Constants.StageMaxY;
    }

    protected override Vector2 GetMoveDirection() {
        throw new System.NotImplementedException();
    }

    public override void Erase(bool spawnItem) {        
        GameController.Instance.InstantiateEffectEraseGeneral(
            this.transform.position, generationDisplayPrefab, (uint)(C.ColorE16.Blue), rotation
        );
        if (spawnItem) {
            var t = GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), ItemType.SmallPoint);
            t.GetComponent<ItemLogic>().NotifyAutoCollect();
        }
        Destroy(gameObject);
    }

    private void OnDestroy() {
        reference.RemoveNotifyPositionDelegate(this.SetReferenceOrigin);
    }

}