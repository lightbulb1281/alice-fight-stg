using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic27 : BulletAbstractLogic1 {

    private const float initialSpeed = 0.12f;
    private const float finalSpeed = 0.06f;
    private Timer generationTimer;
    private Vector2 direction;
    private float rotation;

    public void SetParameters(Vector2 startPos, Vector2 direction, uint color) {
        base.SetParameters(startPos, color, color, true, 12, true);
        generationTimer = new Timer(); 
        this.direction = direction.normalized;
        this.rotation = U.Atan2(direction);
    }

    protected override void Start() { 
        base.Start(); 
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
    }

    protected override void GenerationMove() {
        generationTimer.Tick(); float progress = generationTimer.Progress(12);
        float speed = Mathf.Lerp(initialSpeed, finalSpeed, progress);
        this.transform.position += speed * direction.zed(0);
        rotation += 3 * Mathf.Deg2Rad;
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
    }

    protected override void Move() {
        this.transform.position += finalSpeed * direction.zed(0);
        rotation += 3 * Mathf.Deg2Rad;
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
    }


}