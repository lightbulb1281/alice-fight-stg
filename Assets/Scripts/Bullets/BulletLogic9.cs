using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic9 : BulletPhonyLogic {

    private const float R0 = 25 * C.p2u;
    private static readonly float H = R0 / Mathf.Sin(18 * Mathf.Deg2Rad) * Mathf.Sin(36 * Mathf.Deg2Rad);
    public static readonly float ROTATE_SPEED = 1.2f * Mathf.Deg2Rad;
    public const int LINE_N = 4;

    public GameObject bulletPrefab;
    
    public delegate void DelegateNotifyPosition(Vector2 pos);
    DelegateNotifyPosition notifyPositionDelegates;

    private Timer timer;
    private float rotation;
    private Vector2 startPos;

    public void SetParameters(Vector2 startPos, float startRotation) {
        this.rotation = startRotation; this.startPos = startPos;
    }

    public void RemoveNotifyPositionDelegate(DelegateNotifyPosition d) {
        notifyPositionDelegates -= d;
    }

    void Start() {
        timer = new Timer(); this.transform.position = U.VectorZ(startPos, C.DepthBullet);
        notifyPositionDelegates = null;
    }

    void FixedUpdate() {
        timer.Tick(); float accelerationProgress = Mathf.Clamp(timer.Progress(30), 0, 1); 
        rotation += ROTATE_SPEED;
        int n = LINE_N;
        for (int i=0; i<n; i++) {
            if (timer.Check(2+i*6)) {
                float h = H / n * i; float l = Mathf.Sqrt(R0*R0+h*h-2*R0*h*Mathf.Cos(126*Mathf.Deg2Rad));
                float theta = Mathf.Asin(h * Mathf.Sin(126 * Mathf.Deg2Rad) / l);
                for (int k=0; k<5; k++) {
                    var logic = Instantiate(bulletPrefab).GetComponent<BulletLogic10>();
                    logic.SetParameters(
                        this, l, theta + Mathf.PI * 2 / 5 * k + rotation
                    );
                    notifyPositionDelegates += logic.SetReferenceOrigin;
                }
                h = H / n * (n-i); l = Mathf.Sqrt(R0*R0+h*h-2*R0*h*Mathf.Cos(126*Mathf.Deg2Rad));
                theta = Mathf.Asin(h * Mathf.Sin(126 * Mathf.Deg2Rad) / l);
                for (int k=0; k<5; k++) {
                    var logic = Instantiate(bulletPrefab).GetComponent<BulletLogic10>();
                    logic.SetParameters(
                        this, l, -theta + Mathf.PI * 2 / 5 * k + 72 * Mathf.Deg2Rad + rotation
                    );
                    notifyPositionDelegates += logic.SetReferenceOrigin;
                }
            } 
        }
        float speed = U.CurveSin(accelerationProgress) * 0.045f;
        this.transform.Translate(speed * Vector3.down, Space.World);
        if (notifyPositionDelegates != null)
            notifyPositionDelegates.Invoke(this.transform.position);
        if (this.transform.position.y < -C.StageMaxY - 120 * C.p2u) Destroy(gameObject);
    }

}