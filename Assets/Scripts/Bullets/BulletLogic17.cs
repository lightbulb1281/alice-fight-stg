using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic17Base : BulletAbstractLogic2 {

    private uint moveDuration, warnDuration, persistDuration;
    private Vector2 startPosition, targetPosition;

    private enum State {
        Move, Warn, Persist, Fold
    }
    State state;

    private Timeout timeout;

    public void SetParameters(Vector2 startPosition, Vector2 targetPosition, uint moveDuration, uint warnDuration, uint persistDuration) {
        this.startPosition = startPosition; this.targetPosition = targetPosition;
        this.moveDuration = moveDuration; 
        this.warnDuration = warnDuration;
        this.persistDuration = persistDuration;
        if (moveDuration < 12) {
            Debug.LogWarning("Move duration less than 12.");
        }
    }

    protected override void Start() {
        base.Start(); this.transform.position = startPosition.zed(C.DepthBullet);
        this.transform.localScale = new Vector3(0, 0, 1);
        timeout = new Timeout(moveDuration);
        this.state = State.Move;
    }

    protected virtual float getShootDirection() {return -90 * Mathf.Deg2Rad;}

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (this.state == State.Move) {
            timeout.Tick(); float scaleProgress = (float)(timeout.GetElapsed()) / 12;
            float scale = Mathf.Lerp(0, 1, scaleProgress);
            this.transform.localScale = new Vector3(scale, scale, 1);
            float progress = timeout.Progress();
            this.transform.position = Vector2.Lerp(startPosition, targetPosition, U.CurveSin(progress)).zed(C.DepthBullet);
            if (timeout.Tick()) {
                display.Fold();
                timeout.Reset(warnDuration); state = State.Warn;
                rotation = getShootDirection();
            }
        } else if (this.state == State.Warn) {
            if (timeout.TickReset(persistDuration)) {
                state = State.Persist; display.Expand(); base.SetColliders(true);
                G.soundPlayer.Play("laser0");
            }
        } else if (this.state == State.Persist) {
            timeout.Tick();
            if (timeout.Check()) {
                state = State.Fold; display.Fold(); 
                timeout.Reset(BulletDisplayLaserController.ExpandTime); 
                SetColliders(false);
            }
        } else {
            if (timeout.Tick()) Destroy(this.gameObject);
        }
    }

}



public class BulletLogic17 : BulletLogic17Base {

    protected override float getShootDirection() {
        return G.random.Float(-100, -80) * Mathf.Deg2Rad;
    }

}

