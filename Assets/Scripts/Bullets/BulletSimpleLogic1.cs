using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Linear motion with a fixed acceleration.
/// In generation it will not accelerate.
/// </summary>
public class BulletSimpleLogic1 : BulletAbstractLogic1
{

    protected Vector2 velocity, acceleration;
    private float spin, startRotation, angularAcceleration;
    private uint waitBeforeMoving;
    private float maxVelocity, maxSpin;

    private Timeout wait;

    public void SetParameters(
        Vector2 startPosition, Vector2 velocity, Vector2 acceleration, float maxVelocity,
        float startRotation, float startSpin, float angularAcceleration, float maxSpin,
        uint waitBeforeMoving, 
        uint generationColorID, uint bulletColorID, 
        bool moveInGeneration, uint generationTime, bool showGeneration = true) 
    {
        base.SetParameters(startPosition, generationColorID, bulletColorID, 
            moveInGeneration, generationTime, showGeneration);
        this.waitBeforeMoving = waitBeforeMoving; this.maxVelocity = maxVelocity;
        this.velocity = velocity; this.acceleration = acceleration;
        this.spin = startSpin; this.startRotation = startRotation;
        this.angularAcceleration = angularAcceleration;
        this.maxVelocity = maxVelocity; this.maxSpin = maxSpin;
    }

    protected override void Start() {
        base.Start();
        this.transform.localRotation = Quaternion.Euler(0, 0, startRotation);
        wait = new Timeout(waitBeforeMoving);
    }

    protected override void GenerationMove() {
        this.transform.Rotate(0, 0, spin);
        if (!wait.Tick()) return;
        this.transform.Translate(velocity, Space.World);
    }

    protected override void Move() {
        spin += angularAcceleration; spin = Mathf.Clamp(spin, -maxSpin, maxSpin);
        this.transform.Rotate(0, 0, spin);
        if (!wait.Tick()) return;
        velocity += acceleration;
        if (velocity.magnitude > maxVelocity) velocity = velocity.normalized * maxVelocity;
        this.transform.Translate(velocity, Space.World);
    }

    protected override Vector2 GetMoveDirection() {
        return velocity.normalized;
    }

    protected override void OnBulletGenerated() {
        // Debug.LogFormat("rotation triggered: {0}", startRotation);
        this.transform.localRotation = Quaternion.Euler(0, 0, startRotation);
    }

}
