using UnityEngine;

public class BulletLogic5: BulletSimpleLogic1 {

    private const uint DONT_DESTROY_BEFORE = 150;
    private const uint SHOOT_START_DELAY = 60;
    private const uint SHOOT_INTERVAL = 12;

    public GameObject bulletTrailPrefab; 
    
    private bool trail; private Vector2 orthogonalOffset;
    private Timeout dontDestroy;
    private Timeout shootTimer;

    public void SetParameters(
        bool trail, Vector2 orthogonalOffset,
        Vector2 startPos, Vector2 acceleration, float startRotation, float spin, 
        uint waitBeforeMoving, uint genColor, uint bulletColor) 
    {
        this.orthogonalOffset = orthogonalOffset;
        this.trail = trail;
        base.SetParameters(
            startPos, Vector2.zero, acceleration, 1e2f, startRotation, spin, 0, 1e2f, waitBeforeMoving, 
            genColor, bulletColor, false, 12, true
        );
    }

    protected override void Start() {
        base.Start();
        dontDestroy = new Timeout(DONT_DESTROY_BEFORE);
        shootTimer = new Timeout(
            SHOOT_START_DELAY + GameController.random.UInt(SHOOT_INTERVAL)
        );
    }

    protected override void Move() {
        dontDestroy.Tick();
        base.Move();
    }

    protected override bool ShouldDestroy() {
        if (!dontDestroy.Check()) return false;
        return base.ShouldDestroy();
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (this.state == State.MOVING) {
            if (trail && shootTimer.TickReset(SHOOT_INTERVAL)) {
                Vector2 dir = this.velocity * 0.5f + orthogonalOffset * 0.05f + GameController.random.InsideUnit2() * 0.01f;
                Instantiate(bulletTrailPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                    this.transform.position, dir,
                    0, 0, false, 0, 0, false, 0
                );
            }
        }
    }

}