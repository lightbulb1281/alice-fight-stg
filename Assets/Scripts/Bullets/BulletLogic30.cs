using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic30 : BulletAbstractLogic1
{

    public GameObject laserPrefab;
    public GameObject shardPrefab;

    private Vector2 velocity;
    private float bulletSpin; // degrees per frame
    private float startRotation;

    bool shrink; float scale;
    SRandom robert;

    // bulletSpin: degrees
    public void SetParameters(Vector2 startPosition, Vector2 velocity, 
        uint generationColorID, uint bulletColorID, bool moveInGeneration, 
        uint generationTime, float startRotation = 0, bool showGeneration = true, float bulletSpin = 0) 
    {
        base.SetParameters(startPosition, generationColorID, bulletColorID, 
            moveInGeneration, generationTime, showGeneration);
        this.velocity = velocity;
        this.bulletSpin = bulletSpin; this.startRotation = startRotation;
    }

    protected override Vector2 GetMoveDirection() {
        return velocity.normalized;
    }

    protected override void Start() {
        base.Start(); shrink = false;
        this.transform.localRotation = Quaternion.Euler(0, 0, startRotation);
        this.robert = new SRandom(G.random.Int());
    }
    
    protected override void Move() {
        if (!shrink) this.transform.Translate(velocity, Space.World);
        this.transform.Rotate(0, 0, bulletSpin);
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (!shrink && Mathf.Abs(this.transform.position.y) > C.StageVisibleY) {
            shrink = true; scale = 1;
            if (this.transform.position.y < 0) {
                this.transform.position = new Vector3(this.transform.position.x, -C.StageVisibleY, this.transform.position.z);
                Instantiate(laserPrefab).GetComponent<BulletLogic31>().SetParameters(
                    this.transform.position, this.transform.position, 12, 60, 90
                );
            } else {
                this.transform.position = new Vector3(this.transform.position.x, C.StageVisibleY, this.transform.position.z);
            }                              
        }
        if (shrink) {
            if (this.transform.position.y > 0) {
                int n = robert.Int(2);
                G.soundPlayer.Play("kira0");
                for (int i=0; i<n; i++) {
                    Vector2 startv = robert.InsideUnit2() * 0.02f; 
                    Instantiate(shardPrefab).GetComponent<BulletLogic32>().SetParameters(
                        this.transform.position, startv, 0.04f / C.SecondToFrame * Vector2.down, 0.08f, (uint)C.ColorE16.Green
                    );
                }
            }
            scale -= 0.1f; this.transform.localScale = new Vector3(scale, scale, 1);
            if (scale <= 0) Destroy(gameObject);
        }
    }

    protected override bool ShouldDestroy() {
        bool x = Mathf.Abs(this.transform.position.x) > C.StageMaxX + 32 * C.p2u;
        bool y = Mathf.Abs(this.transform.position.y) > C.StageMaxY + 32 * C.p2u;
        return x || y;
    }

    protected override void OnBulletGenerated() {
        this.transform.localRotation = Quaternion.Euler(0, 0, startRotation);
    }
}