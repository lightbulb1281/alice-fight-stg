using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simplest logic: linear uniform motion. 
/// </summary>
public class BulletSimpleLogic0 : BulletAbstractLogic1
{
    protected Vector2 velocity;
    private float bulletSpin; // degrees per frame
    private float startRotation;

    // bulletSpin: degrees
    public void SetParameters(Vector2 startPosition, Vector2 velocity, 
        uint generationColorID, uint bulletColorID, bool moveInGeneration, 
        uint generationTime, float startRotation = 0, bool showGeneration = true, float bulletSpin = 0) 
    {
        base.SetParameters(startPosition, generationColorID, bulletColorID, 
            moveInGeneration, generationTime, showGeneration);
        this.velocity = velocity;
        this.bulletSpin = bulletSpin; this.startRotation = startRotation;
    }

    protected override Vector2 GetMoveDirection() {
        return velocity.normalized;
    }

    protected override void Start() {
        base.Start();
        this.transform.localRotation = Quaternion.Euler(0, 0, startRotation);
    }
    
    protected override void Move() {
        this.transform.Translate(velocity, Space.World);
        this.transform.Rotate(0, 0, bulletSpin);
    }

    protected override void OnBulletGenerated() {
        this.transform.localRotation = Quaternion.Euler(0, 0, startRotation);
    }
}
