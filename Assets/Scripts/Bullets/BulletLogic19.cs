using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic19 : BulletPhonyLogic {

    private const int count = 30;
    private const float totalDeviation = 400 * C.p2u;
    private const float descendSpeed = 0.04f;
    private const float threshold = 30 * C.p2u;


    public GameObject bulletPrefab;

    private bool generateRightwards;
    private SRandom robert;
    private float[] d;
    private int currentIndex;
    private Timeout timeout;
    private uint color;

    public void SetParameters(int seed, bool generateRightwards, uint color) {
        this.generateRightwards = generateRightwards;
        this.robert = new SRandom(seed);
        this.color = color;
    }

    void genTree(int left, int right, float deviation) {
        if (left == right-1) return;
        int mid = (left + right) / 2;
        d[mid] = (d[left] + d[right]) / 2 + robert.FloatPM() * deviation;
        genTree(left, mid, deviation * (mid-left) / (right-left));
        genTree(mid, right, deviation * (right-mid) / (right-left));
    }

    void Start() {
        // generate shape
        d = new float[count+1]; d[0] = d[count] = 0;
        genTree(0, count, totalDeviation/2);
        for (int i=0; i<5; i++) {
            d[robert.Int(count)] += threshold * robert.PM();
        }
        this.transform.position = new Vector3(0, C.StageMaxY, 0);
        timeout = new Timeout(); timeout.Reset(2);
    }

    void FixedUpdate() {
        this.transform.Translate(descendSpeed * Vector3.down);
        if (currentIndex < count && timeout.TickReset(1)) {
            float x = Mathf.Lerp(-C.StageVisibleX, C.StageVisibleX, (float)(currentIndex) / (count-1));
            if (!generateRightwards) x = -x;
            Vector2 pos = new Vector2(x, this.transform.position.y + d[currentIndex]);
            Instantiate(bulletPrefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                pos, Vector3.down * descendSpeed, color, color, true, 12, 0, true, 0
            );
            currentIndex++;
            if (currentIndex == count) Destroy(this.gameObject);
        }
    }

}