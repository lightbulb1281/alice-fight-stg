using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic12 : BulletAbstractLogic1 {

    public GameObject bulletExplodedRicePrefab;
    public GameObject bulletExplodedShardPrefab;
    
    private Vector2 startPos, explodePos;
    private uint moveDuration;
    private float rotation;
    private Timer timer;
    private uint color, explodedColor;

    public enum ExplodeType {
        Rice, Shard
    }
    private ExplodeType explodeType;

    public void SetParameters(Vector2 startPosition, Vector2 explodePosition, uint moveDuration, uint color, ExplodeType explodeType, uint explodedColor) {
        this.startPos = startPosition; this.explodePos = explodePosition;
        this.moveDuration = moveDuration; this.explodeType = explodeType;
        base.SetParameters(this.startPos, color, color, true, 12); this.explodedColor = explodedColor;
    }

    protected override void Start() {
        base.Start(); 
        rotation = G.random.TwoPi(); this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
        timer = new Timer(); timer.Reset();
    }

    protected override void Move() {
        float progress = timer.Progress(moveDuration); progress = Mathf.Clamp(progress, 0, 1);
        Vector2 pos = new Vector2(
            Mathf.Lerp(startPos.x, explodePos.x, progress),
            Mathf.Lerp(startPos.y, explodePos.y, progress*progress)
        );
        this.transform.position = pos.zed(C.DepthBullet);
        rotation += 5 * Mathf.Deg2Rad;
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
    }

    private void generateExplodedBullet(bool shard, Vector2 v, float gravity, float maxv = 0.06f) {
        Instantiate(!shard ? bulletExplodedRicePrefab : bulletExplodedShardPrefab).GetComponent<BulletLogic13>().SetParameters(
            startPosition: this.transform.position, velocity: v, 
            acceleration: Vector2.down * gravity, maxv, explodedColor, explodedColor, shard
        );
    }

    // private static readonly Vector2[] shards = {
    //     new Vector2(0, 1), 
    //     new Vector2(-0.3f, 0.5f),
    //     new Vector2(-0.3f, -0.5f),
    //     new Vector2(0, -1),
    //     new Vector2(0.3f, -0.5f),
    //     new Vector2(0.3f, 0.5f),
    // };
    protected override void FixedUpdate() {
        timer.Tick();
        base.FixedUpdate();
        if (timer >= moveDuration) {
            G.soundPlayer.Play("kira0");
            if (explodeType == ExplodeType.Rice) {
                for (int i=0; i<20; i++) {
                    Vector2 v = new Vector2((explodePos-startPos).x * 0.004f, 0); v += G.random.InsideUnit2() * 0.02f;
                    generateExplodedBullet(false, v, 3e-4f);
                }
            } else {
                SRandom rnd = G.random;
                for (int i=0; i<4; i++) {
                    Vector2 v = new Vector2((explodePos-startPos).x * 0.004f, 0); v += rnd.InsideUnit2() * 0.02f; 
                    float size = rnd.Float(20, 100) * C.p2u; int gran = Mathf.CeilToInt(size / (3*C.p2u));
                    for (int j=0; j<gran; j++) 
                        generateExplodedBullet(true, v + Utils.UnitByTheta(Mathf.PI * 2 / gran * j) * size * 0.004f, 5e-4f, 1e5f);
                    
                    // print($"size={size}, gran={gran}");
                    // for (int j=0; j<gran; j++) for (int k=0; k<6; k++) {
                    //     if (gran >= 2) 
                    //         generateExplodedBullet(v + Vector2.Lerp(shards[k], shards[(k+1)%6], (float)(j) / (gran-1)) * size * 0.004f, C.ColorE16.Orange, 5e-4f);
                    //     else
                    //         generateExplodedBullet(v + shards[k] * size * 0.004f, C.ColorE16.Orange, 5e-4f);
                    // }
                }
            }
            Destroy(gameObject);
        }
    }

}