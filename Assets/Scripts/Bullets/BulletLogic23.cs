using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic23 : BulletAbstractLogic {

    private static readonly float width = 1;

    Vector2 startPosition, velocity; float length; float currentLength;
    uint color;
    public void SetParameters(Vector2 pos, Vector2 velocity, float length, uint color) {
        this.startPosition = pos; this.velocity = velocity; this.length = length; this.color = color;
    }

    void Start() {
        base.Initialize();
        this.transform.position = startPosition.zed(C.DepthBullet);
        this.transform.localRotation = Quaternion.Euler(0, 0, U.Atan2(velocity) * Mathf.Rad2Deg);
        this.transform.localScale = new Vector3(0, width, 1);
        currentLength = 0;
        GetComponent<Collider2D>().enabled = true;
        GetComponentInChildren<SpriteColorPicker>().SetColor(color);
    }

    void FixedUpdate() { 
        float displacement = 0;
        if (currentLength < length) {
            currentLength += velocity.magnitude;
            if (currentLength > length) {displacement = currentLength - length; currentLength = length;}
            this.transform.localScale = new Vector3(currentLength, width, 1);
        } else displacement = velocity.magnitude;
        this.transform.Translate(velocity.normalized * displacement, Space.World);
        if (U.OutsideStage(this.transform.position)) Destroy(gameObject);
    }

}