using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic14 : BulletAbstractLogic1 {

    private Vector2 velocity;
    private float deceleration, minVelocity;
    private float rotation, spin;

    public void SetParameters(
        Vector2 startPosition, Vector2 startVelocity, float deceleration, float minVelocity,
        float startRotation, float spin,
        uint genColor, uint bulletColor) 
    {
        base.SetParameters(startPosition, genColor, bulletColor, true, 12, true);
        this.velocity = startVelocity; this.deceleration = deceleration;
        this.minVelocity = minVelocity;
        this.rotation = startRotation; this.spin = spin;
    }

    protected override void Start() { 
        base.Start(); 
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation);
    }

    protected override void Move() {
        rotation += spin; 
        float mag = velocity.magnitude;
        if (mag - deceleration < minVelocity) mag = minVelocity; else mag -= deceleration;
        velocity = velocity.normalized * mag;
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation);
        this.transform.Translate(velocity, Space.World);
    }

}