using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic31 : BulletLogic17Base {

    protected override float getShootDirection() {
        return G.random.Float(85, 95) * Mathf.Deg2Rad;
    }

}