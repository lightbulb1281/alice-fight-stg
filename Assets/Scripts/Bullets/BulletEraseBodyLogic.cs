using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletEraseBodyLogic: MonoBehaviour {
    
    protected virtual void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Eraser") {
            this.transform.GetComponentInParent<BulletAbstractLogic>().Erase(true);
        }
    }

}