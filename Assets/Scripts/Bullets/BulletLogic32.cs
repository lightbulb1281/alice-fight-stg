using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic32 : BulletAbstractLogic1 {

    Vector2 velocity; Vector2 accleration;
    float maxVelocity;

    public void SetParameters(Vector2 startPos, Vector2 startVel, Vector2 acc, float maxVel, uint color) {
        base.SetParameters(startPos, color, color, false, 0, false);
        velocity = startVel; accleration = acc; maxVelocity = maxVel;
    }

    protected override void Move() {
        velocity += accleration;
        if (velocity.magnitude > maxVelocity) velocity = velocity.normalized * maxVelocity;
        this.transform.Translate(velocity, Space.World);
        this.transform.localRotation = Quaternion.Euler(0, 0, U.Atan2(velocity) * Mathf.Rad2Deg);
    }

    protected override bool ShouldDestroy() {
        var x = Mathf.Abs(this.transform.position.x); var y = this.transform.position.y;
        return (x > Constants.StageMaxX || y < -Constants.StageMaxY);
    }

    protected override Vector2 GetMoveDirection() {
        return velocity.normalized;
    }

    // protected override void Start() { 
    //     base.Start(); 
    // }
    // protected override void OnBulletGenerated() { 
    //     base.OnBulletGenerated();
    // }
    // protected override void GenerationMove() {
    //     base.GenerationMove();
    // }
    // protected override bool ShouldDestroy() {
    //     return base.ShouldDestroy();
    // }
    // protected override Vector2 GetMoveDirection() {
    //     return base.GetMoveDirection();
    // }

}