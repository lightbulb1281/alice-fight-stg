using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides generation and bullet display. Abstracts Move function.
/// </summary>
public abstract class BulletAbstractLogic1 : BulletAbstractLogic
{

    public GameObject bulletDisplayPrefab;
    public GameObject generationDisplayPrefab;

    private GameObject bulletDisplay;
    private GameObject generationDisplay;

    private Vector2 startPosition;
    protected uint generationColorID, bulletColorID;
    private bool moveInGeneration;
    private uint generationTime;
    private bool showGeneration;

    protected enum State {
        GENERATION, MOVING
    }

    protected State state;
    private uint timer;

    // bulletSpin: degrees
    public void SetParameters(Vector2 startPosition, uint generationColorID,
        uint bulletColorID, bool moveInGeneration, uint generationTime, bool showGeneration = true) 
    {
        this.startPosition = startPosition;
        this.generationColorID = generationColorID; this.bulletColorID = bulletColorID;
        this.moveInGeneration = moveInGeneration; this.generationTime = generationTime;
        this.showGeneration = showGeneration;
    }

    protected virtual void Start() {
        base.Initialize();
        this.transform.position = new Vector3(startPosition.x, startPosition.y, Constants.DepthBullet);
        state = State.GENERATION;
        if (showGeneration) {
            generationDisplay = Instantiate(generationDisplayPrefab, this.transform);
            generationDisplay.GetComponent<SpriteColorPicker>().SetColor(generationColorID);
            generationDisplay.transform.localScale = new Vector3(4, 4, 1);
            generationDisplay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        }
        timer = 0;
    }
    
    protected virtual void GenerationMove() {
        Move();
    }
    protected abstract void Move();
    protected virtual void OnBulletGenerated() {}
    
    /// <summary>
    /// This would not be called in Generation State. Only called in Moving State.
    /// Usually this would be to check if the bullet is outside the stage.
    /// </summary>
    /// <returns>If it returns true, the gameObject will be destroyed in this frame.</returns>
    protected virtual bool ShouldDestroy() {
        var x = Mathf.Abs(this.transform.position.x); var y = Mathf.Abs(this.transform.position.y);
        return (x > Constants.StageMaxX || y > Constants.StageMaxY);
    }

    protected virtual Vector2 GetMoveDirection() {
        return Vector2.right;
    }

    protected virtual void FixedUpdate() {
        switch (state) {
            case State.GENERATION: {
                timer++;
                if (showGeneration) generationDisplay.GetComponent<SpriteRenderer>().color = 
                    new Color(1, 1, 1, Mathf.Lerp(0, 0.8f, (float)timer / generationTime));
                float scale = Mathf.Lerp(4, 1.5f, (float)timer / generationTime);
                if (showGeneration) generationDisplay.transform.localScale = new Vector3(scale, scale, 1);
                if (moveInGeneration) GenerationMove();
                if (timer >= generationTime) {
                    if (showGeneration) Destroy(generationDisplay);
                    bulletDisplay = Instantiate(bulletDisplayPrefab, this.transform);
                    bulletDisplay.GetComponent<SpriteRenderer>().sortingOrder = GameController.Instance.GetSpriteIndex();
                    bulletDisplay.GetComponent<SpriteColorPicker>().SetColor(bulletColorID);
                    this.state = State.MOVING;
                    this.GetComponent<Collider2D>().enabled = true;
                    OnBulletGenerated();
                }
                break;
            }
            case State.MOVING: {
                Move();
                if (ShouldDestroy()) Destroy(gameObject);
                break;
            }
        }
    }

    public override void Erase(bool spawnItem) {
        // TODO: check this. Some inherited class does not use generationColorID the same as erasing.
        // GameController.I.InstantiateEffectGeneral(generationDisplayPrefab, generationColorID, GetMoveDirection)
        GameController.I.InstantiateEffectEraseGeneral(this.transform.position, generationDisplayPrefab, generationColorID, Utils.Atan2(GetMoveDirection()));
        // GameController.Instance.InstantiateEffectErase(Utils.VectorXY(this.transform.position), generationColorID);
        base.Erase(spawnItem);
    }

}

/* Template

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class NameHere : BulletAbstractLogic1 {

    public void SetParameters() {

    }

    protected override void Move() {

    }

    // protected override void Start() { 
    //     base.Start(); 
    // }
    // protected override void OnBulletGenerated() { 
    //     base.OnBulletGenerated();
    // }
    // protected override void GenerationMove() {
    //     base.GenerationMove();
    // }
    // protected override bool ShouldDestroy() {
    //     return base.ShouldDestroy();
    // }
    // protected override Vector2 GetMoveDirection() {
    //     return base.GetMoveDirection();
    // }

}

*/