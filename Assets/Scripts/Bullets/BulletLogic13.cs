using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic13 : BulletAbstractLogic1
{

    protected Vector2 velocity, acceleration;
    private float maxVelocity;
    private bool dontRotate;

    public void SetParameters(
        Vector2 startPosition, Vector2 velocity, Vector2 acceleration, float maxVelocity,
        uint generationColorID, uint bulletColorID, bool dontRotate = false) 
    {
        base.SetParameters(startPosition, generationColorID, bulletColorID, 
            true, 12, true);
        this.velocity = velocity; this.acceleration = acceleration;
        this.maxVelocity = maxVelocity; 
        this.dontRotate = dontRotate;
    }

    protected override void Start() {
        base.Start();
        if (!dontRotate) this.transform.localRotation = Quaternion.Euler(0, 0, Utils.Atan2(this.velocity) * Mathf.Rad2Deg);
    }

    protected override void Move() {
        if (!dontRotate) this.transform.localRotation = Quaternion.Euler(0, 0, Utils.Atan2(this.velocity) * Mathf.Rad2Deg);
        velocity += acceleration;
        if (velocity.magnitude > maxVelocity) velocity = velocity.normalized * maxVelocity;
        this.transform.Translate(velocity, Space.World);
    }

    protected override Vector2 GetMoveDirection() {
        return velocity.normalized;
    }

}
