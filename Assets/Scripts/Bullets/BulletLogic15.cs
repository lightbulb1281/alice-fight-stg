using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletLogic15 : BulletAbstractLogic1 {

    private bool moveRightwards;
    private float startY;
    private uint color;

    private Vector2 getVelocity() {
        return -U.UnitByTheta(angle).flipx(moveRightwards) * 0.162f;
    }

    public static readonly float angle = 63 * Mathf.Deg2Rad;
    public static readonly float tan = Mathf.Tan(angle);

    public void SetParameters(bool moveRightwards, float startY, uint color) {
        base.SetParameters(
            startPosition: new Vector2(C.StageMaxX, startY).flipx(moveRightwards),
            color, color, false, 0, false);
        this.moveRightwards = moveRightwards;
    }

    protected override void Start() { 
        base.Start(); 
        this.transform.localRotation = Quaternion.Euler(0, 0, U.Atan2(getVelocity()) * Mathf.Rad2Deg);
    }

    protected override void Move() {
        // print("move derived called");
        this.transform.Translate(getVelocity(), Space.World);
    }

    protected override Vector2 GetMoveDirection() {
        return getVelocity().normalized;
    }

    protected override bool ShouldDestroy() {
        // print("should destroy derived called");
        var x = this.transform.position.x; 
        var y = this.transform.position.y;
        if (moveRightwards) 
            return x > Constants.StageMaxX || y < -Constants.StageMaxY;
        else 
            return x < -Constants.StageMaxX || y < -Constants.StageMaxY;
    }

    public override void Erase(bool spawnItem) {
        if (this.transform.position.y < C.StageMaxY) base.Erase(spawnItem);
        else Destroy(gameObject);
    }

}