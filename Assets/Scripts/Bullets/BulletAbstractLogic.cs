using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAbstractLogic : MonoBehaviour
{
    protected bool grazed; 
    protected virtual void Initialize() {
        grazed = false;
        var collider = GetComponent<Collider2D>();
        if (collider) {
            if (collider.enabled) {
                Debug.LogWarning("Bullet collider2d enabled at generation. Disabling...");
                collider.enabled = false;
            }
        } else {
            Debug.LogWarning("Bullet does not have collider2d.");
        }
    }
    public virtual void Erase(bool spawnItem) {
        if (spawnItem) {
            var t = GameController.Instance.InstantiateItem(Utils.VectorXY(this.transform.position), ItemType.SmallPoint);
            t.GetComponent<ItemLogic>().NotifyAutoCollect();
        }
        Destroy(gameObject);
    } 
    protected virtual void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "PlayerHitpoint") Destroy(gameObject);
        else if (other.tag == "Eraser") Erase(true);
        else if (other.tag == "PlayerGrazebox") {
            if (!grazed) {grazed = true; GameController.Instance.Notify(GameController.Notification.BulletGrazed);}
        }
    }
}

/* Template

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public class BulletNameHere : BulletAbstractLogic {

    public void SetParameters() {

    }

    void Start() {
        base.Initialize();
    }

    void FixedUpdate() {

    }

    public override void Erase() {

    }

}

*/