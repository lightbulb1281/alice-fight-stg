using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{
    [Header("Nonspell 5")]
    public GameObject bulletNonspell5Prefab;

    private const int ns5Health = 650000;
    private const uint ns5Timeout = 45 * C.SecondToFrame;
    private const uint ns5MoveInterval = 180;

    private static readonly uint[] ns5colors = {2, 6, 8, 10, 13};

    void prepareNonspell5() {
        G.I.enemySpellcardCounter = TOTAL_STARS - 4;
        setupSpellcard(ns5Health, ns5Timeout, 1f, 0.25f, true, true);
        healthCircle.SetThresholdRatios(0.25f);
        var taskMoveToCenter = new WaitTask(150) | new MoveToTrigonometricTask(this.transform, new Vector2(0, 200 * C.p2u), 60);
        var taskMove = 0 * new SequencedTask()
            .Do(() => {
                setMoveInRect(-50, 200-50, 50, 200+50);
                moveTimer.Reset(ns5MoveInterval);
            })
            .LoopUntil(() => {
                moveTimer.Tick();
                updateMove(60);
            }, () => {return moveTimer.Check();});
        var taskShootBegin = new ImmediateTask(() => {
            sAngle = robert.TwoPi();
            sTimes = 0; 
        });
        var taskShoot = 0 * new SequencedTask()
            .Do(() => {
                sTimes++;
                if (sTimes % 3 == 0) G.soundPlayer.Play("bullet2");
                int n = 80; float pr = U.RepeatedLinear((float)(sTimes) / n);
                float angleIncrement = 0;
                uint timeout = (uint)(Mathf.CeilToInt(Mathf.Lerp(3, 1, pr)));
                pr = (float)(sTimes) / n; pr = pr - Mathf.Floor(pr);
                float shootDistance = 0; 
                float d = 0.75f;
                if (pr < d) {
                    shootDistance = Mathf.Lerp(0.02f, 1.8f, U.CurvePlateau(pr / d, 0.9f));
                    angleIncrement = Mathf.Lerp(1.5f, 10, pr/d) * Mathf.Deg2Rad;
                } else {
                    shootDistance = Mathf.Lerp(1.8f, 0.02f, (pr-d)/(1-d));
                    angleIncrement = Mathf.Lerp(10, 1.5f, U.CurveNExp((pr-d)/(1-d), 0, 10)) * Mathf.Deg2Rad;
                }
                for (int i=0; i<5; i++) {
                    Vector2 r = U.UnitByAngle(sAngle + Mathf.PI * 2 / 5 * i);
                    Instantiate(bulletNonspell5Prefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                        this.transform.position.xy() + r * shootDistance,
                        robert.Noised(r * 0.025f, 0.05f), ns5colors[i], ns5colors[i], true, 12, robert.TwoPi() * Mathf.Rad2Deg,
                        true, robert.Float(2, 4)
                    );
                    r = U.UnitByAngle(sAngle + Mathf.PI * 2 / 5 * i - 10 * Mathf.Deg2Rad);
                    Instantiate(bulletNonspell5Prefab).GetComponent<BulletSimpleLogic0>().SetParameters(
                        this.transform.position.xy() + r * shootDistance,
                        robert.Noised(r * 0.025f, 0.05f), ns5colors[i], ns5colors[i], true, 12, robert.TwoPi() * Mathf.Rad2Deg,
                        true, robert.Float(2, 4)
                    );
                }
                sAngle += angleIncrement;
                sTimeout.Reset(timeout);
            }).LoopUntil(() => {sTimeout.Tick();}, () => {return sTimeout.Check();});
        var taskTimeout = new LoopInfiniteTask(() => {
            timeoutTimer.Tick(); G.I.SetTimeoutDisplay(timeoutTimer.Get());
            if (timeoutTimer.Check()) finishNonspell5(false);
        });
        task = (taskMoveToCenter + (taskMove | (taskShootBegin + taskShoot))) | taskTimeout;
        task.OnStart();
    }

    void updateNonspell5() {
        task.Update();
    }

    void finishNonspell5(bool healthDrained) {
        G.I.EraseAllBullets();
        task = null;
        switchState(State.Spellcard5);
    }

}