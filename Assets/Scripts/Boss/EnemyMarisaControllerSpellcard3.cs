using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController: EnemyAbstractLogic {

    [Header("Spellcard 3")]
    public GameObject bulletSpellcard3PrefabA;
    public GameObject bulletSpellcard3PrefabB;
    
    private const int sc3Health = 550000;
    private const uint sc3Timeout = 60 * C.SecondToFrame;
    private const uint sc3BonusU = 1500000, sc3BonusL = 500000;

    private const uint sc3GeneratorIntervalMax = 120;
    private const uint sc3GeneratorIntervalMin = 60;

    void prepareSpellcard3() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 2;
        setupSpellcard(sc3Health, sc3Timeout, 0.3f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.3"));
        bonusFailed = false;
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(200); 
        moveTimer.Reset(180);
        timer.Reset(); sBool1 = false;
    }

    void updateSpellcard3() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimeout.Reset(sc3GeneratorIntervalMax); sBool = robert.Bool();
                sTimes = 0; SetAcceptDamage(true);
                sTimer.Reset();
            }
        } else {
            // 波形生成器
            uint nextTimeout = sc3GeneratorIntervalMax - sTimes * 3;
            if (nextTimeout < sc3GeneratorIntervalMin) nextTimeout = sc3GeneratorIntervalMin;
            if (sTimeout.TickReset(nextTimeout)) {
                sTimes++;
                G.soundPlayer.Play("kira0");
                Instantiate(bulletSpellcard3PrefabA).GetComponent<BulletLogic19>().SetParameters(robert.Int(), sBool, (uint)(C.ColorE16.Green));
                sBool = !sBool; sBool1 = true;
            }
            // 自机狙
            if (sBool1) {
                sTimer.TickCheckReset(60);
                for (int i=0; i<3; i++) if (sTimer.Check(i*10+1)) {
                    G.soundPlayer.Play("bullet0");
                    var direction = G.playerPosition - this.transform.position.xy();
                    direction.Normalize();
                    for (int j=0; j<3; j++) for (int k=0; k<5; k++) {
                        Instantiate(bulletSpellcard3PrefabB).GetComponent<BulletSimpleLogic0>().SetParameters(
                            this.transform.position,
                            U.Rotate(direction, Mathf.PI*2/5*k) * (0.09f + j*0.01f),
                            13, 13, true, 12, 0, true, 3
                        );
                    }
                }
            }
            // 移动
            if (moveTimer.TickReset(90)) 
                setMove(100, 100, 100, 150, 250); 
            updateMove(60);
        }
        // 倒计时
        if (timeoutTimer.Tick()) 
            finishSpellcard3(false); 
        else 
            G.I.SetTimeoutDisplay(timeoutTimer.Get()); 
        updateBonusDisplay(timeoutTimer.Progress(), sc3BonusU, sc3BonusL);
    }

    void finishSpellcard3(bool healthDrained) {
        if (healthDrained) {
            spawnItems(15, 10, null);
        }
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc3BonusU, sc3BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, 
            !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        healthCircle.SetRatio(1); 
        switchState(State.Nonspell4);
    }

}