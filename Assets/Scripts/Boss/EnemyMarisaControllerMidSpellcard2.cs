using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{


    [Header("Midboss Spellcard 2")]
    [Tooltip("Bullet: Broomstick Stick")] public GameObject bulletMidSpellcard2PrefabA;
    [Tooltip("Bullet: Broomstick Dustpan")] public GameObject bulletMidSpellcard2PrefabB;
    private const uint msc2ShootInterval = 120; 
    private const uint msc2MoveInterval = 120;
    private const uint msc2Timeout = 45 * C.s2f;
    private const uint msc2BonusU = 1500000;
    private const uint msc2BonusL = 400000;
    private const int msc2Life = 320000;
    private void prepareMidSpellcard2() {
        G.soundPlayer.Play("spellcard");
        setupSpellcard(msc2Life, msc2Timeout, 1f, 0f);
        GameController.Instance.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("MidSpellcard.2"));
        timer.Reset(); waitTimer.Reset(120); moveTimer.Reset(60);
        moveFrom = transform.position; moveTo = new Vector2(0, 250*C.p2u); moveSet = true;
        timeoutTimer.Reset(msc2Timeout); sStarted = false; 
        updateBonusDisplay(0, msc2BonusU, msc2BonusL);
        bonusFailed = false;
    }
    private void updateMidSpellcard2() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60); 
            if (waitTimer.Tick()) {
                sStarted = true; moveSet = false; moveTimer.Reset(40);
                sTimer.Set(msc2ShootInterval-1); sTimes = 0;
                SetAcceptDamage(true);
            }
        } else {
            int k = (int)((float)(msc2ShootInterval) - sTimes * 3.2f); if (k < 60) k = 60;
            if (sTimer.TickCheckReset((uint)k)) {
                sTimes++;
                float originX = robert.Float(0, C.StageVisibleX - 50 * C.p2u); 
                if (robert.Bool()) originX = -originX;
                Vector2 origin = new Vector2(originX, C.StageVisibleY - 60 * C.p2u);
                float targetXMin = originX - 200 * C.p2u, targetXMax = originX + 200 * C.p2u;
                // targetXMin = Mathf.Clamp(targetXMin, -C.StageMaxX, C.StageMaxX);
                // targetXMax = Mathf.Clamp(targetXMax, -C.StageMaxX, C.StageMaxX);
                float targetX = robert.Float(targetXMin, targetXMax);
                Vector2 target = new Vector2(targetX, -C.StageVisibleY);
                sPos0 = origin; sPos1 = target; sDir0 = (sPos1-sPos0).normalized;
                sDir1 = U.Rotate(sDir0, 90 * Mathf.Deg2Rad);
            }
            for (int i=0; i<7; i++) if (sTimer.Check(i*2+3)) {
                if (i==0) G.soundPlayer.Play("kira0");
                var startPosition = sPos0 + (6-i)*20f*C.p2u*sDir0;
                var rot = Utils.Atan2(sDir0) * Mathf.Rad2Deg;
                Instantiate(bulletMidSpellcard2PrefabA).GetComponent<BulletLogic5>().SetParameters(
                    false, Vector2.zero, startPosition + 4 * C.p2u * sDir1, sDir0 * 0.05f * C.f2s, rot, 0, (uint)(70-(i*2+3)), 13, 13
                );
                Instantiate(bulletMidSpellcard2PrefabA).GetComponent<BulletLogic5>().SetParameters(
                    false, Vector2.zero, startPosition - 4 * C.p2u * sDir1, sDir0 * 0.05f * C.f2s, rot, 0, (uint)(70-(i*2+3)), 13, 13
                );
            }
            for (int i=0; i<4; i++) if (sTimer.Check(17+i*3)) {
                var startPosition = sPos0 + i * 20f * C.p2u * (-sDir0);
                float span = 16f * i * C.p2u; int count = i+1;
                for (int j=0; j<count; j++) {
                    var orthogonalOffset = (-0.5f + ((count>=2) ? (1f/(count-1)*j) : (0.5f))) * span * sDir1;
                    var sp = startPosition + orthogonalOffset;
                    var rot = robert.Float() * 360;
                    Instantiate(bulletMidSpellcard2PrefabB).GetComponent<BulletLogic5>().SetParameters(
                        i==3, orthogonalOffset,
                        sp, sDir0 * 0.05f * C.f2s, rot, 3 + robert.Float() * 3, (uint)(70-(17+i*3)), 1, 1
                    );
                }
            }
            if (moveTimer.TickReset(msc2MoveInterval)) setMove(100, 100, 100, 250, 350);
            updateMove(60);
        }
        timeoutTimer.Tick();
        GameController.Instance.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), msc2BonusU, msc2BonusL);
        if (timeoutTimer.Done()) finishMidSpellcard2(false); 
    }
    private void finishMidSpellcard2(bool healthDrained) {
        if (healthDrained) {
            spawnItems(10, 0, new List<ItemType>{ItemType.Life});
        }
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(msc2BonusU, msc2BonusL, timeoutTimer.Progress())));
        GameController.Instance.Notify(GameController.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        GameController.Instance.EraseAllBullets(); 
        GameController.Instance.HideEnemySpellcardInfo();
        healthCircle.Hide();
        SetAcceptDamage(false);
        SetBodyCollider(false);
        switchGeneralPause(60, State.Leave0);
    }

}