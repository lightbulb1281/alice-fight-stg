using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController: EnemyAbstractLogic {

    [Header("Spellcard 4")]
    public GameObject bulletSpellcard4PrefabA;
    public GameObject bulletSpellcard4PrefabB;
    
    private const int sc4Health = 550000;
    private const uint sc4Timeout = 60 * C.SecondToFrame;
    private const uint sc4BonusU = 1500000, sc4BonusL = 500000;

    private const uint sc4Interval = 240;

    void prepareSpellcard4() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 3;
        setupSpellcard(sc4Health, sc4Timeout, 0.25f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.4"));
        bonusFailed = false;
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(250); 
        moveTimer.Reset(sc4Interval - 80);
        timer.Reset();
    }

    void sc4ShootLaser(Vector2 dir) {
        Instantiate(bulletSpellcard4PrefabA).GetComponent<BulletLogic23>().SetParameters(
            this.transform.position, dir * 0.08f, 2, (uint)(C.ColorE16.Cyan)
        );
    }

    void sc4ShootStar(Vector2 startPos, Vector2 vel) {
        Instantiate(bulletSpellcard4PrefabB).GetComponent<BulletSimpleLogic0>().SetParameters(
            startPos, vel, (uint)(C.ColorE16.Blue), (uint)(C.ColorE16.Blue), 
            false, 12, 0, true, 3
        );
    }

    void updateSpellcard4() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset(); 
                SetAcceptDamage(true);
            }
        } else {
            sTimer.TickCheckReset(sc4Interval);
            for (int i=0; i<20; i++) if (sTimer.Check(i*5+10)) {
                G.soundPlayer.Play("laser0");
                Vector2 dir = G.playerPosition - this.transform.position.xy(); dir.Normalize();
                float dtheta = Mathf.Lerp(36, 3, (float)(i)/20) * Mathf.Deg2Rad;
                sc4ShootLaser(dir);
                sc4ShootLaser(U.Rotate(dir, dtheta));
                sc4ShootLaser(U.Rotate(dir, -dtheta));
            }
            for (int i=0; i<24; i++) if (sTimer.Check(i*8+40)) {
                float t = Mathf.LerpUnclamped(0, 90, (float)(i)/9) * Mathf.Deg2Rad;
                Vector2 o = this.transform.position.xy();
                float dtheta = Mathf.Lerp(10, 40, U.RepeatedSin((float)(i)/24)) * Mathf.Deg2Rad;
                G.soundPlayer.Play("bullet0");
                for (int j=0; j<4; j++) {
                    Vector2 sp = o + 0.8f * U.UnitByTheta(t + Mathf.PI * 2 / 4 * j);
                    Vector2 vel = (G.playerPosition - sp).normalized * (0.11f + 0.002f * i);
                    sc4ShootStar(sp, vel);
                    sc4ShootStar(sp, U.Rotate(vel, dtheta));
                    sc4ShootStar(sp, U.Rotate(vel, -dtheta));
                }
            }
            // 移动
            if (moveTimer.TickReset(sc4Interval)) 
                setMove(100, 100, 100, 200, 300); 
            updateMove(60);
        }
        // 倒计时
        if (timeoutTimer.Tick()) 
            finishSpellcard4(false); 
        else 
            G.I.SetTimeoutDisplay(timeoutTimer.Get()); 
        updateBonusDisplay(timeoutTimer.Progress(), sc4BonusU, sc4BonusL);
    }

    void finishSpellcard4(bool healthDrained) {
        if (healthDrained) {
            spawnItems(15, 10, new List<ItemType>{ItemType.Bomb});
        }
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc4BonusU, sc4BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, 
            !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        healthCircle.SetRatio(1); 
        switchState(State.Nonspell5);
    }

}