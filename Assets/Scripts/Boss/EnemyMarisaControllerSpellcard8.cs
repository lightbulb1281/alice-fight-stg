using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 8")]
    public GameObject bulletSpellcard8BulletPrefabA;
    public GameObject bulletSpellcard8BulletPrefabB;

    private const int sc8Health = 700000;
    private const uint sc8Timeout = 70 * C.SecondToFrame;
    private const uint sc8BonusU = 1500000, sc8BonusL = 500000;

    private const uint sc8LoopInterval = 150;

    private void prepareSpellcard8() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 6;
        healthCircle.SetThresholdRatios(0.33f);
        setupSpellcard(sc8Health, sc8Timeout, 0.66f, 0.33f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.8"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(270);
        moveTimer.Reset(120);
    }

    private void updateSpellcard8() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset();
                SetAcceptDamage(true); 
                moveSet = false; moveTimer.Reset(90);
                sTimer.Reset();
                sFloat = robert.TwoPi();
            }
        } else {
            if (sTimer.Check(0) || sTimer.Check(15)) {
                G.soundPlayer.Play("bullet0");
                bool left = sTimer.Check(0);
                uint color = (uint)(left ? C.ColorE16.Cyan : C.ColorE16.Orange);
                int n = 32;
                Vector2 startPos = new Vector2((left ? -175 : 175) * C.p2u, this.transform.position.y) + robert.InsideUnit2() * 24f * C.p2u;
                GameObject prefab = left ? bulletSpellcard8BulletPrefabA : bulletSpellcard8BulletPrefabB;
                for (int i=0; i<n; i++) {
                    float angle = sFloat + Mathf.PI * 2 / n * i;
                    Instantiate(prefab).GetComponent<BulletLogic29>().SetParameters(
                        startPos, angle, left, true, color, left
                    );
                    Instantiate(prefab).GetComponent<BulletLogic29>().SetParameters(
                        startPos, angle, left, false, color, left
                    );
                }
                sFloat += 7f * Mathf.Deg2Rad;
            }
            sTimer.TickCheckReset(50);
            if (moveTimer.TickReset(sc8LoopInterval)) setMove(50, 100, 100, 200, 350); 
            updateMove(60);
        }
        if (timeoutTimer.Tick()) finishSpellcard8(false);
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), sc8BonusU, sc8BonusL);
    }

    private void finishSpellcard8(bool healthDrained) {
        if (healthDrained) 
            spawnItems(25, 10, null);
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc8BonusU, sc8BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        SetAcceptDamage(false); healthCircle.SetRatio(0.33f);
        switchGeneralPause(120, State.Spellcard9);
    }

}