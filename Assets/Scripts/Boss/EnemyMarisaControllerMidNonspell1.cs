using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    
    [Header("Midboss Nonspell 1")]
    public GameObject bulletMidNonspell1Prefab; 
    // private static readonly List<uint> mns1gencolors = new List<uint>() {1, 4, 4, 3, 2};
    private static readonly List<uint> mns1bulcolors = new List<uint>() {2, 14, 13, 10, 6};
    private const uint mns1Timeout = 30 * C.s2f;
    private const int mns1Life = 240000;
    private void prepareMidNonspell1() {
        setupSpellcard(mns1Life, mns1Timeout, 1f, 0.2f);
        healthCircle.SetThresholdRatios(0.2f);
        timer.Reset(); sTimer.Reset(); sStarted = false;
        timeoutTimer.Reset(mns1Timeout);
        moveTimer.Reset(120); moveFrom = transform.position; moveTo = new Vector2(0, 250*C.p2u);
        sAngle = robert.Float(0, Mathf.PI * 2);
    }
    private void updateMidNonspell1() {
        // if (timer.Check(30)) finishMidNonspell1(false); // TODO: Delete this debug statement
        if (timer.TickCheck(30)) {sStarted = true; SetAcceptDamage(true);}
        if (sStarted && sTimer.TickCheckReset(5)) {
            G.soundPlayer.Play("bullet0");
            sAngle += 6 * Mathf.Deg2Rad;
            for (int i=0; i<5; i++) {
                Vector2 genDir = U.UnitByTheta(sAngle + Mathf.PI*2/5*i);
                Vector2 genPos = U.VectorXY(this.transform.position) + genDir * 16 * C.p2u;
                Vector2 velocity = U.Rotate(genDir, -90 * Mathf.Deg2Rad) * 0.05f + robert.InsideUnit2() * 0.003f;
                var logic = Instantiate(bulletMidNonspell1Prefab).GetComponent<BulletSimpleLogic0>();
                logic.SetParameters(genPos, velocity, mns1bulcolors[i], mns1bulcolors[i], true, 12, 
                    robert.Float() * Mathf.PI, true, 3);
                genDir = U.UnitByTheta(sAngle + Mathf.PI*2/5*i + 15*Mathf.Deg2Rad);
                genPos = U.VectorXY(this.transform.position) + genDir * 16 * C.p2u;
                velocity = U.Rotate(genDir, -90 * Mathf.Deg2Rad) * 0.05f + robert.InsideUnit2() * 0.003f;
                logic = Instantiate(bulletMidNonspell1Prefab).GetComponent<BulletSimpleLogic0>();
                logic.SetParameters(genPos, velocity, mns1bulcolors[i], mns1bulcolors[i], true, 12, 
                    robert.Float() * Mathf.PI, true, 3);
            }
        }
        if (moveTimer.TickReset(120)) 
            setMove(100, 70, 70, 200, 270);
        updateMove(60);
        if (timeoutTimer.Tick()) finishMidNonspell1(false); 
        else GameController.Instance.SetTimeoutDisplay(timeoutTimer.Get());
    }
    private void finishMidNonspell1(bool success) {
        if (success) {
            // TODO: add some effect
        }
        GameController.Instance.EraseAllBullets(); 
        switchState(State.MidSpellcard1);
    }
    
}