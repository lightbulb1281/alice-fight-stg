using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{
    [Header("Spellcard 1")]
    public GameObject bulletSpellcard1PrefabA;
    public GameObject bulletSpellcard1PrefabB;
    private const int sc1Health = 360000;
    private const uint sc1Timeout = 48 * Constants.SecondToFrame;
    private const uint sc1MoveInterval1 = 2 * Constants.SecondToFrame;
    private const uint sc1MoveInterval2 = 4 * Constants.SecondToFrame;
    private const uint sc1BonusU = 1200000, sc1BonusL = 400000;
    /// <summary>
    /// Set move for Spellcard1.
    /// </summary>
    /// <param name="moveType">0 for left, 1 to right, 2 anywhere</param>
    void sc1SetMove(uint moveType) {
        if (moveType == 0 || moveType == 1) {
            moveFrom = this.transform.position; moveTo = robert.InsideRect(220, 280, 220, 300) * C.p2u; moveSet = true;
            if (moveType == 0) moveTo = new Vector2(-moveTo.x, moveTo.y);
            sFloat0 = (190f + robert.Float(0, 20)) * C.p2u;
        } else {
            setMove(100, 100, 100, 250, 350);
            sFloat0 = (200f + robert.Float(0, 20)) * C.p2u; sBool = robert.Bool();
        }
    }
    void prepareSpellcard1() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS;
        setupSpellcard(totalHealth: sc1Health, sc1Timeout, 0.25f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.1"));
        timer.Reset(); 
        sc1SetMove(0); sMoveDirection = 0; sStarted = false; moveTimer.Reset(sc1MoveInterval1);
        timeoutTimer.Reset(sc1Timeout); 
        sType = 0; 
        updateBonusDisplay(0, sc1BonusU, sc1BonusL);
        bonusFailed = false;
        sTimer.Set(120);
    }
    void updateSpellcard1() {
        if (timeoutTimer.GetElapsed() == 120) {SetAcceptDamage(true); sStarted = true;}
        if (sStarted) {
            if (sMoveDirection != 2) {
                for (int i=0; i<7; i++) if (moveTimer.GetElapsed() == 60 + i * 8) {
                    float explodeX = Mathf.Lerp(-250f, 250f, (float)(i)/6); if (sMoveDirection == 1) explodeX = -explodeX;
                    explodeX *= C.p2u;
                    float explodeY = sFloat0 + (-180f + i * 50) * C.p2u + 100 * robert.Float() * C.p2u;
                    G.soundPlayer.Play("bullet0");
                    Instantiate(bulletSpellcard1PrefabA).GetComponent<BulletLogic12>().SetParameters(
                        startPosition: this.transform.position,
                        explodePosition: new Vector2(explodeX, explodeY),
                        (uint)(30 - i * 2), sMoveDirection == 0 ? 6u : 5u, 
                        explodeType: BulletLogic12.ExplodeType.Rice, sMoveDirection == 0 ? (uint)(C.ColorE16.Yellow) : (uint)(C.ColorE16.Green)
                    );
                }
            } else {
                for (int i=0; i<7; i++) if (moveTimer.GetElapsed() == 70 + i * 8) {
                    float explodeX = Mathf.Lerp(-200, 200f, (float)(i)/6); if (sBool) explodeX = -explodeX;
                    explodeX *= C.p2u;
                    float explodeY = sFloat0 + 100 * robert.Float() * C.p2u;
                    G.soundPlayer.Play("bullet0");
                    Instantiate(bulletSpellcard1PrefabA).GetComponent<BulletLogic12>().SetParameters(
                        startPosition: this.transform.position, explodePosition: new Vector2(explodeX, explodeY),
                        30u, 4, 
                        explodeType: BulletLogic12.ExplodeType.Shard, (uint)(C.ColorE16.Cyan)
                    );
                }
            }
        }
        // 护身弹 
        sTimer.Tick();
        for (int i=0; i<3; i++) if (sTimer.Check((i+1)*12)) {
            Vector2 d = (i==1) ? (G.playerPosition - transform.position.xy()) : robert.Unit2(); d.Normalize();
            G.soundPlayer.Play("bullet0");
            for (int j=0; j<30; j++) Instantiate(bulletSpellcard1PrefabB).GetComponent<BulletSimpleLogic0>().SetParameters(
                this.transform.position, U.Rotate(d, Mathf.PI*2/30*j) * 0.05f, (uint)(C.ColorE16.Red), (uint)(C.ColorE16.Red), true, 12, 0, true, 3
            );
        }
        if (sTimer >= 120) {
            float distance = (G.playerPosition - this.transform.position.xy()).magnitude; 
            if (distance < 200 * C.p2u) sTimer.Reset(); 
        }

        uint nextMove = (sMoveDirection + 1) % 3;
        if (moveTimer.TickReset(nextMove == 2 ? sc1MoveInterval2 : sc1MoveInterval1)) {
            sMoveDirection = nextMove; sc1SetMove(sMoveDirection); 
        }
        updateMove(60);
        if (timeoutTimer.Tick()) finishSpellcard1(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get()); 
        updateBonusDisplay(timeoutTimer.Progress(), sc1BonusU, sc1BonusL);
    }
    void finishSpellcard1(bool healthDrained) {
        if (healthDrained) {
            spawnItems(10, 10, new List<ItemType>{ItemType.Bomb});
        }
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc1BonusU, sc1BonusL, timeoutTimer.Progress())));
        G.I.Notify(
            G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        healthCircle.SetRatio(1); 
        switchState(State.Nonspell2);
    }

}
