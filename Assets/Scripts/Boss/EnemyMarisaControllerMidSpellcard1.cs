using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Midboss Spellcard 1")]
    public GameObject bulletMidSpellcard1Prefab; 
    private const float msc1ExplodeDistance = 360 * Constants.p2u;
    private const float msc1ExplodeAngleInterval = 12 * Mathf.Deg2Rad; // radians
    private const uint msc1ShootInterval = 80;
    private const uint msc1MoveInterval = 150;
    private const uint msc1Timeout = 40 * C.s2f;
    private const uint msc1BonusUpperbound = 1000000;
    private const uint msc1BonusLowerbound = 500000;
    private const int msc1Life = 250000;
    private void prepareMidSpellcard1() {
        G.soundPlayer.Play("spellcard");
        setupSpellcard(msc1Life, msc1Timeout, 0.2f, 0);
        GameController.Instance.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("MidSpellcard.1"));
        timer.Reset(); sTimer.Set(msc1ShootInterval-10); moveSet = true; waitTimer.Reset(120);
        moveTimer.Reset(60); moveFrom = transform.position; moveTo = new Vector2(0, 250*C.p2u);
        timeoutTimer.Reset(msc1Timeout); sStarted = false;
        sType = 0; 
        updateBonusDisplay(0, msc1BonusUpperbound, msc1BonusLowerbound);
        bonusFailed = false;
    }
    private void updateMidSpellcard1() {
        if (!sStarted) {
            moveTimer.Tick();
            updateMove(60);
            if (waitTimer.Tick()) {sStarted = true; moveTimer.Reset(30); moveSet = false;}
        } else {
            timer.Tick(); 
            if (sTimer.TickCheckReset(msc1ShootInterval)) {sType = 1-sType;}
            for (int i=0; i<5; i++) {
                if (sTimer.Check(35 - i*5)) {
                    uint moveTime = (uint)(30 + 5*i);
                    uint count = (uint)(3+i*6);
                    float expansion = (i+0.5f) * 1.5e-3f;
                    if (i==4) SetAcceptDamage(true);
                    if (i==4) {
                        var game = GameController.Instance;
                        Vector2 dir = GameController.playerPosition - Utils.VectorXY(this.transform.position);
                        float distance = dir.magnitude;
                        dir.Normalize(); 
                        sPos1 = distance < msc1ExplodeDistance ? (GameController.playerPosition - dir * msc1ExplodeDistance) : (U.VectorXY(transform.position) + dir * (distance - msc1ExplodeDistance) * 0.5f);
                        sPos0 = sPos1 + dir * msc1ExplodeDistance; sPos2 = this.transform.position;
                    }
                    Vector2 s2pd = sPos0 - sPos2; s2pd.Normalize();
                    {
                        Vector2 sp = sPos0 + msc1ExplodeDistance * Utils.Rotate(-s2pd, msc1ExplodeAngleInterval*i);
                        var logic = Instantiate(bulletMidSpellcard1Prefab).GetComponent<BulletLogic4>();
                        logic.SetParameters(this.transform.position, sp, sPos1, expansion, sType == 0 ? BulletLogic4.ColorSet.Orange : BulletLogic4.ColorSet.Red, moveTime, count);
                    }
                    if (i==0) {
                        G.soundPlayer.SchedulePlay("bullet0", moveTime);
                    }
                    if (i!=0) {
                        Vector2 sp = sPos0 + msc1ExplodeDistance * Utils.Rotate(-s2pd, -msc1ExplodeAngleInterval*i);
                        var logic = Instantiate(bulletMidSpellcard1Prefab).GetComponent<BulletLogic4>();
                        logic.SetParameters(this.transform.position, sp, sPos1, expansion, sType == 0 ? BulletLogic4.ColorSet.Orange : BulletLogic4.ColorSet.Red, moveTime, count);
                    }
                }
            }
            if (moveTimer.TickReset(msc1MoveInterval)) 
                setMove(100, 100, 100, 250, 350);
            updateMove(60);
        }
        timeoutTimer.Tick();
        GameController.Instance.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), msc1BonusUpperbound, msc1BonusLowerbound);
        if (timeoutTimer.Done()) finishMidSpellcard1(false); 
    }
    private void finishMidSpellcard1(bool healthDrained) {
        if (healthDrained) {
            spawnItems(10, 6, new List<ItemType>{ItemType.BigPower});
        }
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(msc1BonusUpperbound, msc1BonusLowerbound, timeoutTimer.Progress())));
        GameController.Instance.Notify(GameController.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        GameController.Instance.EraseAllBullets(); 
        GameController.Instance.HideEnemySpellcardInfo();
        SetAcceptDamage(false); healthCircle.SetRatio(1);
        switchGeneralPause(120, State.MidSpellcard2); 
    }
}