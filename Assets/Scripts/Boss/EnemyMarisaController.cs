using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    private static readonly Vector2 ENTER_FROM = new Vector2(150*C.p2u, C.StageVisibleY+48*C.p2u);
    private static readonly Vector2 ENTER_TO = new Vector2(0, 250*C.p2u);
    private static readonly Vector2 LEAVE_TO = new Vector2(-400*C.p2u, C.StageVisibleY+48*C.p2u);
    private const uint ENTER_TIME = 90; // frames
    private const uint TOTAL_STARS = 8;

    private enum State {
        Enter, Wait0, // TODO: change wait0 to generalpause
        MidNonspell1, MidSpellcard1, MidSpellcard2, Leave0,
        GeneralPause, 
        Nonspell1, Spellcard1, Nonspell2, Spellcard2,
        Nonspell3, Spellcard3, Nonspell4, Spellcard4,
        Nonspell5, Spellcard5, Nonspell6, Spellcard6,
        Spellcard7, Spellcard8, Spellcard9, Spellcard10,
        Spellcard11, EpilogueIdle,
        Idle, 
    };

    private Transform player;
    private EnemyHealthCircleLogic healthCircle;
    private bool healthCircleShown;
    private SRandom robert;
    private State state;
    private TMPro.TMP_Text bonusDisplay;
    private bool bonusFailed;
    
    private Vector2 moveFrom, moveTo; 
    private bool moveSet;
    private Timer timer;
    private Timeout moveTimer, waitTimer, timeoutTimer; 
    private Timer sTimer, sTimer0, sTimer1;
    private Timeout sTimeout, sTimeout0, sTimeout1;

    private uint sType, sTimes, sMoveDirection; 
    int sInt, sInt0, sInt1, sInt2;
    private Vector2 sDir0, sPos0, sPos1, sPos2, sDir1;
    private float sAngle, sFloat, sFloat0, sFloat1, sFloat2;
    private bool sStarted, sBool, sBool1;
    
    private Task task;

    private int totalHealth; 
    private float healthCircleUpperbound, healthCircleLowerbound; // the interval of current spellcard, in [0, 1], 1 means full, 0 means drained
    
    private State nextState;

    // all are pixels
    private void setMove(float horizontalMargin, float horizontalPlayerDistance, float horizontalSelfDistance, float verticalMin, float verticalMax) {
        moveFrom = transform.position;
        float leftMost = Mathf.Max(
            -C.StageVisibleX + horizontalMargin*C.p2u, 
            player.position.x - horizontalPlayerDistance*C.p2u, 
            transform.position.x - horizontalSelfDistance*C.p2u);
        float rightMost = Mathf.Min(
            C.StageVisibleX - horizontalMargin*C.p2u, 
            player.position.x + horizontalPlayerDistance*C.p2u, 
            transform.position.x + horizontalSelfDistance*C.p2u);
        moveTo = new Vector2(robert.Float(leftMost, rightMost), robert.Float(verticalMin, verticalMax) *C.p2u);
        moveSet = true;
    }
    private void setMove() {moveTo = moveFrom = transform.position; moveSet = true;}
    private void setMoveInRect(float x0, float y0, float x1, float y1) {
        float x = robert.Float(x0, x1); float y = robert.Float(y0, y1); 
        moveFrom = this.transform.position; moveTo = new Vector2(x, y) * C.p2u; moveSet = true;
    }
    private void setMoveTo(Vector2 k) {
        moveFrom = this.transform.position; moveTo = k; moveSet = true;
    }
    private void setMoveToCenter(float yPixels) {
        setMoveTo(new Vector2(0, yPixels * C.p2u));
    }
    private void updateMove(uint moveDuration) {
        uint e = moveTimer.GetElapsed();
        if (moveSet && e < moveDuration) {
            Vector2 pos = Vector2.Lerp(moveFrom, moveTo, U.CurveSin((float)(e) / (moveDuration)));
            this.transform.position = U.VectorZ(pos, C.DepthEnemy);
        }
    }

    private void updateBonusDisplay(float progress, uint upper, uint lower) {
        if (bonusFailed) {
            bonusDisplay.text = "Bonus Failed";
        } else {
            int bonus = Mathf.CeilToInt(Mathf.Lerp(upper, lower, progress));
            bonusDisplay.text = string.Format("Bonus {0}", bonus); // TODO: localization
        }
    }
    private void spawnItems(uint points, uint powers, List<ItemType> specials, float radius = 96) {
        var game = GameController.Instance; 
        for (int i=0; i<points; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * radius * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Point);
        }
        for (int i=0; i<powers; i++) {
            Vector2 pos = this.transform.position; pos += GameController.random.InsideUnit2() * radius * Constants.PixelToUnit;
            game.InstantiateItem(pos, ItemType.Power);
        }
        if (specials != null) {
            foreach (var item in specials) {
                Vector2 pos = this.transform.position; 
                if (specials.Count > 1) pos += GameController.random.InsideUnit2() * radius * Constants.PixelToUnit;
                game.InstantiateItem(pos, item);
            }
        }
    }

    private void setupSpellcard(
        int totalHealth, uint timeout, 
        float healthCircleUpperbound, float healthCircleLowerbound, 
        bool bodyCollider = true, bool acceptDamage = false
    ) {
        GameController.Instance.ShowTimeoutDisplay();
        this.healthCircleUpperbound = healthCircleUpperbound;
        this.healthCircleLowerbound = healthCircleLowerbound;
        healthCircle.SetRatio(healthCircleUpperbound);
        healthCircle.GetComponent<EnemyHealthCircleLogic>().Show();
        healthCircleShown = true; timeoutTimer.Reset(timeout);
        this.totalHealth = health = totalHealth;
        SetBodyCollider(bodyCollider); SetAcceptDamage(acceptDamage);
    }

    #region Leave0
    private void prepareLeave0() {
        moveFrom = this.transform.position; moveTo = LEAVE_TO; moveSet = true;
        moveTimer.Reset(60);
    }
    private void updateLeave0() {
        if (moveTimer.Tick()) {
            GameController.Instance.Notify(GameController.Notification.BossLeft);
            G.I.enemySpellcardCounterShown = false;
            Destroy(gameObject);
        } else {
            updateMove(60);
        }
    }
    #endregion

    void Start() {
        base.Initialize(0);
        base.SetAcceptDamage(false); base.SetBodyCollider(false);
        timer = new Timer(); sTimer = new Timer(); moveTimer = new Timeout(); timeoutTimer = new Timeout(); waitTimer = new Timeout();
        sTimer0 = new Timer(); sTimer1 = new Timer(); sTimeout = new Timeout(); sTimeout0 = new Timeout(); sTimeout1 = new Timeout();
        robert = new SRandom(GameController.random.Int());
        player = GameController.player.transform;
        healthCircle = GetComponentInChildren<EnemyHealthCircleLogic>();
        healthCircleShown = false;
        bonusDisplay = GameController.Instance.GetEnemySpellcardBonusDisplay();
        GameController.bossIndicator.BindTarget(this.transform);
        GameController.I.delegateOnBombUsed += NotifyBonusFailed;
        GameController.I.delegateOnMissed += NotifyBonusFailed;
        string practice = StaticStorage.Instance.practice;
        if (practice == "None") {
            state = State.Enter;
            this.transform.position = U.VectorZ(ENTER_FROM, C.DepthEnemy);
        } else {
            this.transform.position = new Vector2(0, 200) * C.p2u;
            switch (practice) {
                case "Midboss Nonspell 1": switchGeneralPause(60, State.MidNonspell1); break;
                case "Midboss Spellcard 1": switchGeneralPause(60, State.MidSpellcard1); break;
                case "Midboss Spellcard 2": switchGeneralPause(60, State.MidSpellcard2); break;
            
                case "Boss Nonspell 1": switchGeneralPause(60, State.Nonspell1); break;
                case "Boss Spellcard 1": switchGeneralPause(60, State.Spellcard1); break;
                case "Boss Nonspell 2": switchGeneralPause(60, State.Nonspell2); break;
                case "Boss Spellcard 2": switchGeneralPause(60, State.Spellcard2); break;
                case "Boss Nonspell 3": switchGeneralPause(60, State.Nonspell3); break;
                case "Boss Spellcard 3": switchGeneralPause(60, State.Spellcard3); break;
                case "Boss Nonspell 4": switchGeneralPause(60, State.Nonspell4); break;
                case "Boss Spellcard 4": switchGeneralPause(60, State.Spellcard4); break;
                case "Boss Nonspell 5": switchGeneralPause(60, State.Nonspell5); break;
                case "Boss Spellcard 5": switchGeneralPause(60, State.Spellcard5); break;
                case "Boss Nonspell 6": switchGeneralPause(60, State.Nonspell6); break;
                case "Boss Spellcard 6": switchGeneralPause(60, State.Spellcard6); break;
                case "Boss Spellcard 7": switchGeneralPause(60, State.Spellcard7); break;
                case "Boss Spellcard 8": switchGeneralPause(60, State.Spellcard8); break;
                case "Boss Spellcard 9": switchGeneralPause(60, State.Spellcard9); break;
                case "Boss Spellcard 10": switchGeneralPause(60, State.Spellcard10); break;
                case "Boss Spellcard 11": switchGeneralPause(60, State.Spellcard11); break;
            }
        }
    }

    private void switchGeneralPause(uint pauseTime, State nextState) {
        waitTimer.Reset(pauseTime); this.nextState = nextState;
        switchState(State.GeneralPause);
    }

    private void switchState(State state) {
        Debug.Log(string.Format("Marisa switchState: {0}", state));
        switch (state) {
            // Midboss
            case (State.Enter): {timer.Reset(); break;}
            case (State.Wait0): {timer.Reset(); break;}
            case (State.MidNonspell1): {prepareMidNonspell1(); break;}
            case (State.MidSpellcard1): {prepareMidSpellcard1(); break;}
            case (State.MidSpellcard2): {prepareMidSpellcard2(); break;}
            case (State.Leave0): {prepareLeave0(); break;}
            // Final-stage boss
            case (State.Nonspell1): {G.I.PlayMusic(1); prepareNonspell1(); break;}
            case (State.Spellcard1): {G.I.PlayMusic(1); prepareSpellcard1(); break;}
            case (State.Nonspell2): {G.I.PlayMusic(1); prepareNonspell2(); break;}
            case (State.Spellcard2): {G.I.PlayMusic(1); prepareSpellcard2(); break;}
            case (State.Nonspell3): {G.I.PlayMusic(1); prepareNonspell3(); break;}
            case (State.Spellcard3): {G.I.PlayMusic(1); prepareSpellcard3(); break;}
            case (State.Nonspell4): {G.I.PlayMusic(1); prepareNonspell4(); break;}
            case (State.Spellcard4): {G.I.PlayMusic(1); prepareSpellcard4(); break;}
            case (State.Nonspell5): {G.I.PlayMusic(1); prepareNonspell5(); break;}
            case (State.Spellcard5): {G.I.PlayMusic(1); prepareSpellcard5(); break;}
            case (State.Nonspell6): {G.I.PlayMusic(1); prepareNonspell6(); break;}
            case (State.Spellcard6): {G.I.PlayMusic(1); prepareSpellcard6(); break;}
            case (State.Spellcard7): {G.I.PlayMusic(2); prepareSpellcard7(); break;}
            case (State.Spellcard8): {G.I.PlayMusic(2); prepareSpellcard8(); break;}
            case (State.Spellcard9): {G.I.PlayMusic(2); prepareSpellcard9(); break;}
            case (State.Spellcard10): {G.I.PlayMusic(2); prepareSpellcard10(); break;}
            case (State.Spellcard11): {G.I.PlayMusic(2); prepareSpellcard11(); break;}
            case (State.EpilogueIdle): {G.I.PlayMusic(2); prepareEpilogueIdle(); break;}
            // General
            case (State.GeneralPause): {break;}
            case (State.Idle): {break;}
            default: Debug.LogWarning("EnemyMarisa: switchState default block."); break;

        }
        this.state = state;
    }

    public enum Notification {
        GameDialogue0Done, GameBonusFailed, GameDialogue1Done,
        GameDialogue2Done,
    }
    public void NotifyBonusFailed() {bonusFailed = true;}
    public void Notify(Notification n) {
        switch (n) {
            case Notification.GameDialogue0Done: switchState(State.Wait0); break;
            case Notification.GameDialogue1Done: switchState(State.Nonspell1); break;
            case Notification.GameBonusFailed: bonusFailed = true; break;
            case Notification.GameDialogue2Done: switchState(State.Spellcard7); break;
        }
    }

    void FixedUpdate() {
        switch (this.state) {
            // Midboss
            case (State.Enter): {
                timer.Tick(); float progress = timer.Progress(ENTER_TIME);
                Vector2 pos = Vector2.Lerp(ENTER_FROM, ENTER_TO, U.CurveSin(progress));
                this.transform.position = U.VectorZ(pos, C.DepthEnemy);
                if (timer.Check(ENTER_TIME)) switchState(State.Idle);
                break;
            }
            case (State.Wait0): {
                if (timer.TickCheck(60)) {switchState(State.MidNonspell1);} break;
            }
            case (State.MidNonspell1): {updateMidNonspell1(); break;}
            case (State.MidSpellcard1): {updateMidSpellcard1(); break;}
            case (State.MidSpellcard2): {updateMidSpellcard2(); break;}
            case (State.Leave0): {updateLeave0(); break;}
            // Final-stage boss
            case (State.Nonspell1): {updateNonspell1(); break;}
            case (State.Spellcard1): {updateSpellcard1(); break;}
            case (State.Nonspell2): {updateNonspell2(); break;}
            case (State.Spellcard2): {updateSpellcard2(); break;}
            case (State.Nonspell3): {updateNonspell3(); break;}
            case (State.Spellcard3): {updateSpellcard3(); break;}
            case (State.Nonspell4): {updateNonspell4(); break;}
            case (State.Spellcard4): {updateSpellcard4(); break;}
            case (State.Nonspell5): {updateNonspell5(); break;}
            case (State.Spellcard5): {updateSpellcard5(); break;}
            case (State.Nonspell6): {updateNonspell6(); break;}
            case (State.Spellcard6): {updateSpellcard6(); break;}
            case (State.Spellcard7): {updateSpellcard7(); break;}
            case (State.Spellcard8): {updateSpellcard8(); break;}
            case (State.Spellcard9): {updateSpellcard9(); break;}
            case (State.Spellcard10): {updateSpellcard10(); break;}
            case (State.Spellcard11): {updateSpellcard11(); break;}
            case (State.EpilogueIdle): {updateEpilogueIdle(); break;}
            // General
            case (State.GeneralPause): {if (waitTimer.Tick()) switchState(nextState); break;}
            case (State.Idle): {break;}
            default: Debug.LogWarning("EnemyMarisa: FixedUpdate default block."); break;
        }
        // Hide HealthCircle when the player is too close to the boss
        if ((GameController.playerPosition - U.VectorXY(this.transform.position)).magnitude < 64f * C.p2u) {
            if (healthCircleShown) healthCircle.GetComponent<EnemyHealthCircleLogic>().SetPlayerNear(true);
        } else {
            if (healthCircleShown) healthCircle.GetComponent<EnemyHealthCircleLogic>().SetPlayerNear(false);
        }
    }

    protected override void OnHealthChanged(int delta) {
        if (delta < 0) {
            if (health < 100000) G.soundPlayer.Play("damage1");
            else G.soundPlayer.Play("damage0");
        }
        float healthPercent = Mathf.Clamp((float)(health) / totalHealth, 0, 1);
        float anglePercent = Mathf.Lerp(healthCircleLowerbound, healthCircleUpperbound, healthPercent);
        healthCircle.SetRatio(anglePercent);
    }

    protected override void OnHealthDrain() {
        switch (state) {
            case State.MidNonspell1: {finishMidNonspell1(true); break;}
            case State.MidSpellcard1: {finishMidSpellcard1(true); break;}
            case State.MidSpellcard2: {finishMidSpellcard2(true); break;}
            case State.Nonspell1: {finishNonspell1(true); break;}
            case State.Spellcard1: {finishSpellcard1(true); break;}
            case State.Nonspell2: {finishNonspell2(true); break;}
            case State.Spellcard2: {finishSpellcard2(true); break;}
            case State.Nonspell3: {finishNonspell3(true); break;}
            case State.Spellcard3: {finishSpellcard3(true); break;}
            case State.Nonspell4: {finishNonspell4(true); break;}
            case State.Spellcard4: {finishSpellcard4(true); break;}
            case State.Nonspell5: {finishNonspell5(true); break;}
            case State.Spellcard5: {finishSpellcard5(true); break;}
            case State.Nonspell6: {finishNonspell6(true); break;}
            case State.Spellcard6: {finishSpellcard6(true); break;}
            case State.Spellcard7: {finishSpellcard7(true); break;}
            case State.Spellcard8: {finishSpellcard8(true); break;}
            case State.Spellcard9: {finishSpellcard9(true); break;}
            case State.Spellcard10: {
                Debug.LogWarning("Spellcard 10 should never drain its health.");
                finishSpellcard10(); 
                break;
            }
            case State.Spellcard11: {finishSpellcard11(true); break;}
        }
    }

    void OnDestroy() {
        GameController.bossIndicator?.UnbindTarget();
        if (GameController.I != null) {
            GameController.I.delegateOnBombUsed -= NotifyBonusFailed;
            GameController.I.delegateOnMissed -= NotifyBonusFailed;
        }
    }
}
