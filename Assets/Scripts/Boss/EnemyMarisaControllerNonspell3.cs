using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Nonspell 3")]
    public GameObject bulletNonspell3PrefabA;
    public GameObject bulletNonspell3PrefabB;

    private const int ns3Health = 550000;
    private const uint ns3Timeout = 45 * C.SecondToFrame;
    private const uint ns3MoveInterval = 240;

    void prepareNonspell3() {
        G.I.enemySpellcardCounter = TOTAL_STARS - 2;
        setupSpellcard(ns3Health, ns3Timeout, 1f, 0.3f, true, true);
        healthCircle.SetThresholdRatios(0.3f);
        timer.Reset(); sTimer.Reset(); sStarted = false; moveTimer.Reset(ns3MoveInterval); 
        moveFrom = this.transform.position; moveTo = new Vector2(0, 200 * C.p2u); moveSet = true;
        waitTimer.Reset(150); sAngle = robert.TwoPi(); sInt = 0; 
    }

    private static readonly uint[] ns3colorsA = {2, 6, 8, 10, 13};
    private static readonly uint[] ns3colorsB = {1, 3, 4, 5, 6};
    
    void ns3Shoot(bool isB, float angleRadians, int colorSet) {
        bool p = isB;
        GameObject prefab = p ? bulletNonspell3PrefabB : bulletNonspell3PrefabA;
        uint color = p ? ns2colorsB[colorSet] : ns2colorsA[colorSet];
        Vector2 v = U.UnitByTheta(angleRadians) * 0.05f;
        // v = robert.Noised(v, 0.04f);
        Instantiate(prefab).GetComponent<BulletSimpleLogic0>().SetParameters(
            startPosition: this.transform.position, 
            velocity: v, 
            color, color, true, 12, 0, true, 3
        );
    }

    // return k <= t < k+d (mod m)
    bool ns3predicate(int t, int k, int d, int m) {
        t = t%m; k = k%m; 
        if (k+d <= m) {
            if (k <= t && t < k+d) return true;
            return false;
        } else {
            if (t < k && t >= (k+d)%m) return false;
            return true;
        }
    }
    
    void updateNonspell3() {
        if (!sStarted) {
            if (waitTimer.Tick()) {sStarted = true;}
        } else {
            if (sTimer.TickCheckReset(3)) {
                sInt++; 
                if (sInt % 3 == 0) G.soundPlayer.Play("bullet1");
                sAngle += 3f * Mathf.Deg2Rad;
                for (int i=0; i<5; i++) {
                    ns3Shoot(ns3predicate(sInt, i*6 + 3, 6, 12), sAngle + Mathf.PI * 2 / 5 * i, i);
                    ns3Shoot(ns3predicate(sInt, i*6, 6, 12), sAngle + Mathf.PI * 2 / 5 * i + 9f * Mathf.Deg2Rad, i);
                }
            }
        }
        if (moveTimer.TickReset(ns3MoveInterval)) setMoveInRect(-50, 200 - 50, 50, 200 + 50); updateMove(60);
        if (timeoutTimer.Tick()) finishNonspell3(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get());
    }
    
    void finishNonspell3(bool healthDrained) {
        G.I.EraseAllBullets();
        switchState(State.Spellcard3);
    }

}