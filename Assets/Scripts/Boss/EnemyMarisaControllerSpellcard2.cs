using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController: EnemyAbstractLogic {
    [Header("Spellcard 2")]
    public GameObject bulletSpellcard2PrefabA; // amulet generator
    public GameObject bulletSpellcard2PrefabB; // laser

    private const int sc2Health = 600000;
    private const uint sc2Timeout = 60 * C.SecondToFrame;
    private const uint sc2BonusU = 1500000, sc2BonusL = 500000;

    void prepareSpellcard2() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 1;
        setupSpellcard(totalHealth: sc2Health, sc2Timeout, 0.2f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.2"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(200); moveTimer.Reset(120);
        timer.Reset(); 
    }

    private const uint sc2ShootAmuletInterval = 240;
    private const uint sc2ShootLaserInterval = 240;
    private const int sc2AmuletN = 6;
    private const int sc2LaserN = 8;
    void updateSpellcard2() {
        if (!sStarted) {
            // 等一会儿
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer0.Reset(); 
                sBool = robert.Bool(); 
                sFloat = robert.Float() * C.StageHeight / sc2AmuletN; 
                sInt1 = sInt2 = sc2AmuletN;
                sTimer1.Set(0);
                SetAcceptDamage(true);
            }
        } else {
            // 射击
            { // 札弹生成器
                float dy = (C.StageHeight + C.StageWidth * BulletLogic15.tan) / sc2AmuletN;
                for (int i=0; i<sc2AmuletN; i++) {
                    int t1 = (int)((i*36 + 2) % sc2ShootAmuletInterval);
                    int t2 = (int)((i*36 + 2 + sc2ShootAmuletInterval/2) % sc2ShootAmuletInterval);
                    if ((i==0 || sInt1 == i-1) && sTimer0.Check(t1)) {
                        if (i==0) sFloat1 = robert.Float() * dy;
                        Instantiate(bulletSpellcard2PrefabA).GetComponent<BulletLogic16>().SetParameters(
                            robert.Int(), C.StageMaxY - dy * i - sFloat1, !sBool, (uint)(C.ColorE16.Red));                    
                        sInt1 = i;
                    }
                    if ((i==0 || sInt2 == i-1) && sTimer0.Check(t2)) {
                        if (i==0) sFloat2 = robert.Float() * dy;
                        Instantiate(bulletSpellcard2PrefabA).GetComponent<BulletLogic16>().SetParameters(
                            robert.Int(), C.StageMaxY - dy * i - sFloat2, sBool, (uint)(C.ColorE16.White) );
                        sInt2 = i;
                    }
                }
                sTimer0.Tick();
                sTimer0.CheckReset(sc2ShootAmuletInterval);
            }
            { // 激光
                sTimer1.Tick();
                for (int i=0; i<sc2LaserN; i++) if (sTimer1.Check(5*i + 5)) {
                    float dx = C.StageWidth / (sc2LaserN + 1);
                    float x = -C.StageVisibleX + dx * (i+1);
                    Vector2 t = new Vector2(x, 380 * C.p2u); 
                    t += robert.InsideUnit2() * 30 * C.p2u;
                    G.soundPlayer.Play("bullet0");
                    Instantiate(bulletSpellcard2PrefabB).GetComponent<BulletLogic17>().SetParameters(
                        this.transform.position, t, 90, 90, 90
                    );
                }
                sTimer1.CheckReset(sc2ShootLaserInterval);
            }
            // 移动
            if (moveTimer.TickReset(120)) setMove(100, 100, 100, 150, 250); updateMove(60);
        }
        // 倒计时
        if (timeoutTimer.Tick()) finishSpellcard2(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get()); 
        updateBonusDisplay(timeoutTimer.Progress(), sc2BonusU, sc2BonusL);
    }

    void finishSpellcard2(bool healthDrained) {
        if (healthDrained) {
            spawnItems(15, 10, new List<ItemType>{ItemType.Life});
        }
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc2BonusU, sc2BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, 
            !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        healthCircle.SetRatio(1); 
        switchState(State.Nonspell3);
    }

}