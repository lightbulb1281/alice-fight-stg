using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 10")]
    public GameObject enemySpellcard10PrefabA;
    public GameObject enemySpellcard10PrefabB;
    public GameObject bulletSpellcard10PrefabB;
    public GameObject enemySpellcard10PrefabC;
    public GameObject enemySpellcard10PrefabD;
    
    private const uint sc10Timeout = 91 * C.SecondToFrame;
    private const uint sc10Bonus = 2400000;


    private void prepareSpellcard10() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 7;
        setupSpellcard(1000, sc10Timeout, 1, 0f, bodyCollider: false);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.10"));
        healthCircle.Hide(); healthCircleShown = false;
        bonusFailed = false; 
        GetComponent<Collider2D>().enabled = false; // cannot be hit by player shots.
        updateBonusDisplay(0, sc10Bonus, sc10Bonus);
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(150);
        moveTimer.Reset(120); sInt = 0;
    }

    private void updateSpellcard10() {
        switch (sInt) {
            case 0: {
                moveTimer.Tick(); updateMove(60);
                waitTimer.Tick(); if (waitTimer.Check()) {
                    sInt = 1; sTimer.Reset();
                }
                break;
            }
            case 1: {
                sTimer.Tick();
                if (sTimer.Check(10)) {
                    for (int i=0; i<5; i++)
                        Instantiate(enemySpellcard10PrefabA).GetComponent<EnemyLogic9>().SetParameters(
                            this.transform.position, (-90f + 360f / 5 * i) * Mathf.Deg2Rad
                        );
                } 
                if (timeoutTimer.GetElapsed() == 23 * C.SecondToFrame) {
                    sInt = 2;
                    sTimer.Reset();
                }
                break;
            }
            case 2: {
                if (sTimer.Check(0)) {
                    sTimer1.Reset(); sBool = false;
                }
                sTimer.Tick();
                if (sTimer.Check(10)) {
                    for (int i=0; i<8; i++) {
                        Instantiate(enemySpellcard10PrefabB).GetComponent<EnemyLogic10>().SetParameters(
                            (-300 + 600f * i / 7) * C.p2u, i
                        );
                    }
                }
                sTimer1.Tick();
                if (sBool) for (int k=0; k<3; k++) if (sTimer1.Check(5 + k * 15)) {
                    for (int i=0; i<15; i++) {
                        float dir = robert.Float(0, 180) * Mathf.Deg2Rad;
                        float v0 = Mathf.Sqrt(robert.Float());
                        Vector2 vel = 0.04f * v0 * U.UnitByAngle(dir);
                        G.soundPlayer.Play("kira0");
                        Instantiate(bulletSpellcard10PrefabB).GetComponent<BulletSimpleLogic1>().SetParameters(
                            this.transform.position, vel, Vector2.down * 0.0007f, 0.05f, robert.TwoPi(), 3, 0, 3, 0, 
                            (uint)(C.ColorE8.Yellow), (uint)(C.ColorE8.Yellow), true, 12, true
                        );
                    }
                }
                if (sTimer1.CheckReset(210)) {
                    sBool = true;
                }
                if (timeoutTimer.GetElapsed() == 44 * C.SecondToFrame) {
                    sInt = 3;
                    sTimer.Reset();
                }
                break;
            }
            case 3: {
                if (sTimer.Check(0)) {
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(-300, 0) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 1000, false);
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(300, 0) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 1000, true);
                }
                if (sTimer.Check(360)) {
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(-150, 400) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 640, false);
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(150, 400) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 640, true);
                }
                if (sTimer.Check(720)) {
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(-50, 200) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 280, false);
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(50, 200) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 280, true);
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(200, 100) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 280, false);
                    Instantiate(enemySpellcard10PrefabC).GetComponent<EnemyLogic11>().SetParameters(
                        new Vector2(-200, 100) * C.p2u + robert.InsideUnit2() * 20 * C.p2u, 280, true);
                }
                sTimer.Tick();
                if (timeoutTimer.GetElapsed() == 65 * C.SecondToFrame) {
                    Debug.Log("Switched to 4");
                    sInt = 4;
                    sTimer.Reset();
                }
                break;
            }
            case 4: {
                if (sTimer.Check(60)) {
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(0, 0) * C.p2u, true, (uint)(C.ColorE16.Red), false, 13
                    );
                }
                if (sTimer.Check(420)) {
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(-250, 100) * C.p2u, false, (uint)(C.ColorE16.Cyan), false, 15
                    );
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(250, 100) * C.p2u, false, (uint)(C.ColorE16.Cyan), true, 15
                    );
                }
                if (sTimer.Check(780)) {
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(-250, 200) * C.p2u, true, (uint)(C.ColorE16.Blue), false, 13
                    );
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(250, 200) * C.p2u, true, (uint)(C.ColorE16.Blue), false, 13
                    );
                }
                if (sTimer.Check(1120)) {
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(-250, 100) * C.p2u, false, (uint)(C.ColorE16.Yellow), false, 15
                    );
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(250, 100) * C.p2u, false, (uint)(C.ColorE16.Yellow), true, 15
                    );
                    Instantiate(enemySpellcard10PrefabD).GetComponent<EnemyLogic12>().SetParameters(
                        new Vector2(0, 270) * C.p2u, true, (uint)(C.ColorE16.Red), false, 13
                    );
                }
                sTimer.Tick();
                break;
            }
        }
        updateBonusDisplay(timeoutTimer.Progress(), sc10Bonus, sc10Bonus);
        if (timeoutTimer.Tick()) finishSpellcard10();
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
    }

    private void finishSpellcard10() {
        uint bonus = bonusFailed ? 0 : sc10Bonus;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        switchGeneralPause(120, State.Spellcard11);
    }
}
