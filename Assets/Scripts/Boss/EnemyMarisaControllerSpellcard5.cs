using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 5")]
    public GameObject bulletSpellcard5BulletDustPrefabA;
    public GameObject bulletSpellcard5BulletDustPrefabB;
    public GameObject bulletSpellcard5BulletDustPrefabC;
    public GameObject bulletSpellcard5BulletDustPrefabD;

    private const int sc5Health = 700000;
    private const uint sc5Timeout = 70 * C.SecondToFrame;
    private const uint sc5BonusU = 1500000, sc5BonusL = 500000;

    private uint sc5PresentDustCount = 0;
    private const uint sc5MaxDustCount = 320;
    private Queue<float> sc5RespawnDistances;
    public void sc5PresentDustErased(float distance) {
        if (sc5PresentDustCount > 0) {
            sc5PresentDustCount--;
            sc5RespawnDistances.Enqueue(distance);
        } else {
            Debug.LogWarning("SC5 present bullet less than 1.");
        }
    }

    private void prepareSpellcard5() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 4;
        setupSpellcard(sc5Health, sc5Timeout, 0.25f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.5"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(50);
        moveTimer.Reset(120);
        sc5PresentDustCount = 0;
        sc5RespawnDistances = new Queue<float>();
    }

    private void sc5SpawnDust(float r) {
        int k = 0;
        while (true) {
            G.soundPlayer.Play("bullet0");
            float p = robert.TwoPi();
            var startpos = this.transform.position.xy() + U.UnitByAngle(p) * r;
            if ((startpos - G.playerPosition).magnitude < 120 * C.p2u) {
                k++;
                if (k<10) continue;
            }
            float v = 1/Mathf.Sqrt(r) * 0.06f;
            GameObject prefab = bulletSpellcard5BulletDustPrefabA;
            float rt = r / (600 * C.p2u);
            if (rt < 0.1f) prefab = bulletSpellcard5BulletDustPrefabA;
            else if (rt < 0.5f) prefab = robert.Bernoulli((rt-0.1f) / 0.4f) ? bulletSpellcard5BulletDustPrefabA : bulletSpellcard5BulletDustPrefabB;
            else if (rt < 0.8f) prefab = robert.Bernoulli((rt-0.5f) / 0.3f) ? bulletSpellcard5BulletDustPrefabB : bulletSpellcard5BulletDustPrefabC;
            else if (rt < 1.0f) prefab = robert.Bernoulli((rt-0.8f) / 0.2f) ? bulletSpellcard5BulletDustPrefabC : bulletSpellcard5BulletDustPrefabD;
            Instantiate(prefab).GetComponent<BulletLogic26>().SetParameters(
                this, this.transform.position.xy(), startpos, v
            );
            sc5PresentDustCount++; break;
        }
    }

    private void updateSpellcard5() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset();
                SetAcceptDamage(true); 
                sTimeout.Reset(0);
            }
        } else {
            if (sc5PresentDustCount < sc5MaxDustCount) {
                uint t = sc5PresentDustCount + 2; if (t > sc5MaxDustCount) t = sc5MaxDustCount;
                while (sc5PresentDustCount < t) {
                    float r = 0;
                    if (sc5RespawnDistances.Count > 0) {
                        r = sc5RespawnDistances.Dequeue();
                    } else {
                        do {r = Mathf.Pow(robert.Float(), 0.7f);} while (r<0.04f);
                        r = r * 600 * C.p2u;
                    }
                    sc5SpawnDust(r);
                }
            }
        }
        if (timeoutTimer.Tick()) finishSpellcard5(false);
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), sc7BonusU, sc7BonusL);
    }

    private void finishSpellcard5(bool healthDrained) {
        if (healthDrained) 
            spawnItems(25, 10, new List<ItemType>{ItemType.Life});
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc5BonusU, sc5BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        healthCircle.SetRatio(1); 
        switchState(State.Nonspell6);
    }

}