using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 7")]
    public GameObject bulletSpellcard7BulletPrefabA;
    public GameObject bulletSpellcard7BulletPrefabB;

    private const int sc7Health = 450000;
    private const uint sc7Timeout = 70 * C.SecondToFrame;
    private const uint sc7BonusU = 1500000, sc7BonusL = 500000;

    private const uint sc7LoopInterval = 240;

    private void prepareSpellcard7() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 6;
        healthCircle.SetThresholdRatios(0.66f, 0.33f);
        setupSpellcard(sc7Health, sc7Timeout, 1f, 0.66f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.7"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(200);
        moveTimer.Reset(120);
    }

    private void updateSpellcard7() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset();
                SetAcceptDamage(true); 
                moveSet = false; moveTimer.Reset(90);
                sTimeout.Reset(0);
                sBool = false;
            }
        } else {
            sTimeout.TickReset(sc7LoopInterval); 
            uint timeElasped = sTimeout.GetElapsed();
            if (timeElasped == 0) {
                sFloat = U.Atan2(G.playerPosition - this.transform.position.xy()); // player direction
                sBool = !sBool;
                sPos0 = this.transform.position;
            }
            for (int i=0; i<8; i++) {
                if (timeElasped == 10 * i + 10) {
                    G.soundPlayer.Play("bullet0");
                    uint color = (i%2)==0 ? (uint)(sBool ? C.ColorE16.Red : C.ColorE16.Purple) : (uint)(C.ColorE16.White);
                    float baseDirAngle = sFloat - (10 + 10*i) * U.Sign(sBool) * Mathf.Deg2Rad;
                    for (int j=0; j<5; j++) {
                        float axisAngle = baseDirAngle + 360f/5*j*Mathf.Deg2Rad;
                        Vector2 u = U.UnitByAngle(axisAngle);
                        Vector2 v = U.UnitByAngle(axisAngle + 90 * Mathf.Deg2Rad * U.Sign(sBool));
                        for (int k=0; k<20; k++) {
                            float p = (float)(k) / 20;
                            float vr = Mathf.Sin(0.2f + p * Mathf.PI * 0.45f) * Mathf.Tan(25f*Mathf.Deg2Rad);
                            float ul = Mathf.Lerp(0.10f, 0.04f, p);
                            var vel = ul * (u + v * vr);
                            Instantiate(bulletSpellcard7BulletPrefabA).GetComponent<BulletSimpleLogic0>().SetParameters(
                                startPosition: this.transform.position,
                                velocity: vel,
                                color, color, true, 12, U.Atan2(vel) * Mathf.Rad2Deg, true, 0
                            );
                        }
                    } 
                }
            }
            for (int i=0; i<8; i++) {
                if (timeElasped == 10*i + 10) {
                    uint color = (sBool) ? (uint)(C.ColorE16.Red) : (uint)(C.ColorE16.Magenta);
                    float angle = sBool ? Mathf.Lerp(0, 180f, (float)(i)/7) : Mathf.Lerp(180f, 0, (float)(i)/7); angle*= Mathf.Deg2Rad;
                    Vector2 targetPosition = this.transform.position.xy() + U.UnitByAngle(angle) * 80 * C.p2u;
                    Instantiate(bulletSpellcard7BulletPrefabB).GetComponent<BulletLogic25>().SetParameters(
                        this.transform.position, targetPosition, 30, 90, 50, color
                    );
                }
            }
            if (moveTimer.TickReset(sc7LoopInterval)) setMove(50, 100, 100, 200, 350); 
            updateMove(60);
        }
        if (timeoutTimer.Tick()) finishSpellcard7(false);
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), sc7BonusU, sc7BonusL);
    }

    private void finishSpellcard7(bool healthDrained) {
        if (healthDrained) 
            spawnItems(25, 10, null);
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc7BonusU, sc7BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        SetAcceptDamage(false); healthCircle.SetRatio(0.66f);
        switchGeneralPause(120, State.Spellcard8);
    }

}