using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{
    [Header("Nonspell 2")]
    public GameObject bulletNonspell2PrefabA;
    public GameObject bulletNonspell2PrefabB;

    private const int ns2Health = 600000;
    private const uint ns2Timeout = 39 * C.SecondToFrame;
    private const uint ns2MoveInterval = 240;

    void prepareNonspell2() {
        G.I.enemySpellcardCounter = TOTAL_STARS - 1;
        setupSpellcard(ns2Health, ns2Timeout, 1f, 0.2f, true, true);
        healthCircle.SetThresholdRatios(0.2f);
        timer.Reset(); sTimer.Reset(); sStarted = false; moveTimer.Reset(ns2MoveInterval); 
        moveFrom = this.transform.position; moveTo = new Vector2(0, 200 * C.p2u); moveSet = true;
        waitTimer.Reset(150); sAngle = robert.TwoPi(); sInt0 = 0;
    }

    private static readonly uint[] ns2colorsA = {2, 6, 8, 10, 13};
    private static readonly uint[] ns2colorsB = {1, 3, 4, 5, 6};

    void ns2Shoot(float angleRadians, int colorSet) {
        bool p = robert.Bernoulli(0.15f);
        GameObject prefab = p ? bulletNonspell2PrefabB : bulletNonspell2PrefabA;
        uint color = p ? ns2colorsB[colorSet] : ns2colorsA[colorSet];
        Vector2 v = U.UnitByTheta(angleRadians) * 0.06f;
        v = robert.Noised(v, 0.04f);
        Instantiate(prefab).GetComponent<BulletLogic14>().SetParameters(
            startPosition: this.transform.position, 
            startVelocity: v, 
            deceleration: 0.020f / C.SecondToFrame, 
            minVelocity: 0.01f, 
            robert.TwoPi(), 3, color, color
        );
    }

    void updateNonspell2() {
        if (!sStarted) {
            if (waitTimer.Tick()) {sStarted = true;}
        } else {
            if (sTimer.TickCheckReset(5)) {
                sInt0++;
                if (sInt0 % 2 == 0) G.soundPlayer.Play("bullet2");
                sAngle -= 6f * Mathf.Deg2Rad;
                for (int i=0; i<5; i++) {
                    ns2Shoot(sAngle + Mathf.PI * 2 / 5 * i, i);
                    ns2Shoot(sAngle + Mathf.PI * 2 / 5 * i + 13f * Mathf.Deg2Rad, i);
                }
            }
        }
        if (moveTimer.TickReset(ns2MoveInterval)) setMoveInRect(-50, 200 - 50, 50, 200 + 50); updateMove(60);
        if (timeoutTimer.Tick()) finishNonspell2(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get());
    }

    void finishNonspell2(bool healthDrained) {
        G.I.EraseAllBullets();
        switchState(State.Spellcard2);
    }
    
}
