using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 9")]
    public GameObject bulletSpellcard9BulletPrefab;

    private const int sc9Health = 800000;
    private const uint sc9Timeout = 70 * C.SecondToFrame;
    private const uint sc9BonusU = 1500000, sc9BonusL = 500000;

    private const uint sc9LoopInterval = 330;

    private void prepareSpellcard9() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 6;
        healthCircle.SetThresholdRatios();
        setupSpellcard(sc9Health, sc9Timeout, 0.33f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.9"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(0);
        moveTimer.Reset(120);
    }

    private void updateSpellcard9() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset();
                SetAcceptDamage(true); 
                moveSet = false; moveTimer.Reset(90);
                sTimer.Reset();
            }
        } else {
            if (sTimer.Check(0)) {
                sBool = robert.Bool(); 
            }
            int n = 20;
            for (int i=0; i<n; i++) {
                if (sTimer.Check(i + 20)) {
                    G.soundPlayer.Play("bullet0");
                    float angle = sBool ? (Mathf.PI - Mathf.PI * 2 / n * i) : (Mathf.PI * 2 / n * i);
                    Instantiate(bulletSpellcard9BulletPrefab).GetComponent<BulletLogic30>().SetParameters(
                        this.transform.position, U.UnitByAngle(angle) * (0.04f + i * 0.05f / n), 1, 1, true, 12
                    );
                }
            }
            sTimer.TickCheckReset(sc9LoopInterval);
        }
        if (timeoutTimer.Tick()) finishSpellcard9(false);
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), sc9BonusU, sc9BonusL);
    }

    private void finishSpellcard9(bool healthDrained) {
        if (healthDrained) 
            spawnItems(25, 10, null);
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc9BonusU, sc9BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        switchGeneralPause(120, State.Spellcard10);
    }

}