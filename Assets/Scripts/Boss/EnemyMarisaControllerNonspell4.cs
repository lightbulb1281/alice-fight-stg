using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{
    [Header("Nonspell 4")]
    public GameObject bulletNonspell4PrefabA;
    public GameObject bulletNonspell4PrefabB;

    private const int ns4Health = 600000;
    private const uint ns4Timeout = 45 * C.SecondToFrame;
    private const uint ns4MoveInterval = 120;

    void prepareNonspell4() {
        G.I.enemySpellcardCounter = TOTAL_STARS - 3;
        setupSpellcard(ns4Health, ns4Timeout, 1f, 0.25f, true, true);
        healthCircle.SetThresholdRatios(0.25f);
        timer.Reset(); sTimer.Reset(); sStarted = false; moveTimer.Reset(ns4MoveInterval); 
        moveFrom = this.transform.position; moveTo = new Vector2(0, 200 * C.p2u); moveSet = true;
        waitTimer.Reset(150); sAngle = robert.TwoPi(); sInt = 0;
    }

    private static readonly uint[] ns4colorsA = {2, 6, 8, 10, 13};
    private static readonly uint[] ns4colorsB = {1, 3, 4, 5, 6};

    void updateNonspell4() {
        if (!sStarted) {
            if (waitTimer.Tick()) {sStarted = true;}
        } else {
            if (sTimer.TickCheckReset(5)) {
                sInt++; 
                // if (sInt0 % 2 == 0) G.soundPlayer.Play("bullet1");
                sAngle += 5f * Mathf.Deg2Rad;
                for (int i=0; i<5; i++) {
                    Vector2 vel;
                    bool f = (sInt % 10 == i * 2);
                    bool r0 = f && robert.Bool();
                    bool r1 = f && !r0;
                    vel = robert.Noised(U.UnitByTheta(sAngle + Mathf.PI * 2 / 5 * i) * 0.024f, 0.05f);
                    Instantiate(r0 ? bulletNonspell4PrefabB : bulletNonspell4PrefabA).GetComponent<BulletLogic21>().
                        SetParameters(this.transform.position, vel, r0 ? ns4colorsB[i] : ns4colorsA[i]);
                    vel = robert.Noised(U.UnitByTheta(sAngle + Mathf.PI * 2 / 5 * i + 15 * Mathf.Deg2Rad) * 0.024f, 0.05f);
                    Instantiate(r1 ? bulletNonspell4PrefabB : bulletNonspell4PrefabA).GetComponent<BulletLogic21>().
                        SetParameters(this.transform.position, vel, r1 ? ns4colorsB[i] : ns4colorsA[i]);
                }
            }
        }
        if (moveTimer.TickReset(ns4MoveInterval)) setMoveInRect(-50, 200 - 50, 50, 200 + 50); updateMove(60);
        if (timeoutTimer.Tick()) finishNonspell4(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get());
    }

    void finishNonspell4(bool healthDrained) {
        G.I.EraseAllBullets();
        switchState(State.Spellcard4);
    }
}