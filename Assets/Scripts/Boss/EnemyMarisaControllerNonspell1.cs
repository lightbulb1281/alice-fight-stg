using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{
    [Header("Nonspell 1")] public GameObject bulletNonspell1Prefab; 
    private const int ns1Health = 400000;
    private const uint ns1Timeout = 40 * C.SecondToFrame;
    private const uint ns1MoveInterval = 240;
    void prepareNonspell1() {
        G.I.enemySpellcardCounter = TOTAL_STARS;
        setupSpellcard(totalHealth: ns1Health, timeout: ns1Timeout, 1, 0.25f); healthCircle.SetThresholdRatios(0.25f); // setup displays
        timer.Reset(); sTimer.Reset(); sStarted = false; moveTimer.Reset(ns1MoveInterval); moveSet = false;
        sTimes = 0; waitTimer.Reset(0); sInt = 0;
    }
    void ns1Shoot(float angle, float vel, uint col) {
        Instantiate(bulletNonspell1Prefab).GetComponent<BulletSimpleLogic0>().SetParameters(
            startPosition: this.transform.position, 
            velocity: U.UnitByTheta(angle) * vel, 
            generationColorID: col, bulletColorID: col, 
            moveInGeneration: true, generationTime: 12, startRotation: angle * Mathf.Rad2Deg, showGeneration: true, bulletSpin: 3 + G.random.Float() * 2
        );
    }
    void updateNonspell1() {
        if (timer.TickCheck(30)) {sStarted = true; SetAcceptDamage(true);}
        if (sStarted) {
            int n = 150;
            if (waitTimer.Tick()) if (sTimer.TickCheckReset(2)) {
                sInt++;
                if (sInt % 3 == 0)
                    G.soundPlayer.Play("bullet1");
                if (sTimes == 0) sAngle = U.Atan2(G.playerPosition - this.transform.position.xy());
                float vel = Mathf.Lerp(0.04f, 0.08f, (float)(sTimes) / n);
                ns1Shoot(sAngle + 0 * Mathf.Deg2Rad, vel, (uint)C.ColorE16.Blue);
                ns1Shoot(sAngle + 20 * Mathf.Deg2Rad, vel, (uint)C.ColorE16.Blue);
                ns1Shoot(sAngle + 120 * Mathf.Deg2Rad, vel, (uint)C.ColorE16.DarkBlue);
                ns1Shoot(sAngle + 140 * Mathf.Deg2Rad, vel, (uint)C.ColorE16.DarkBlue);
                ns1Shoot(sAngle + 240 * Mathf.Deg2Rad, vel, (uint)C.ColorE16.Cyan);
                ns1Shoot(sAngle + 260 * Mathf.Deg2Rad, vel, (uint)C.ColorE16.Cyan);
                sAngle += Mathf.Lerp(2, 5, U.CurveNSin((float)(sTimes) / n)) * Mathf.Deg2Rad;
                sTimes++; if (sTimes == n) {sTimes = 0; waitTimer.Reset(90);}
            }
        }
        if (moveTimer.TickReset(ns1MoveInterval)) setMove(100, 70, 70, 100, 240); updateMove(60);
        if (timeoutTimer.Tick()) finishNonspell1(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get());
    }
    void finishNonspell1(bool healthDrained) {
        G.I.EraseAllBullets();
        switchState(State.Spellcard1);
    }
}
