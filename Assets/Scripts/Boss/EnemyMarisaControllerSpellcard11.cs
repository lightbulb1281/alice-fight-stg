using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 11")]
    public GameObject bulletSpellcard11PrefabA;

    private const int sc11Health = 2500000;
    private const uint sc11Timeout = 120 * C.SecondToFrame;
    private const uint sc11BonusU = 2500000, sc11BonusL = 1500000;

    private void prepareSpellcard11() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 8;
        healthCircle.SetThresholdRatios(0.75f, 0.50f, 0.25f);
        setupSpellcard(sc11Health, sc11Timeout, 1f, 0f);
        GetComponent<Collider2D>().enabled = true; // cannot be hit by player shots.
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.11"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(300);
        moveTimer.Reset(120);
        sInt = 0;
    }

    private void updateSpellcard11() {
        switch (sInt) {
            case 0: {
                moveTimer.Tick(); updateMove(60);
                if (waitTimer.Tick()) {
                    sInt = 1;
                    // health = totalHealth / 4;
                    sTimer.Reset(); SetAcceptDamage(true); 
                }
                break;
            }
            case 1: {
                if (timeoutTimer.GetElapsed() < 25 * C.SecondToFrame) {
                    float progress = Mathf.Clamp01(sTimer.Progress(sc11Timeout / 4));
                    float nf = Mathf.Lerp(0.1f, 0.28f, U.CurveNSquare(progress));
                    int n = Mathf.FloorToInt(nf);
                    if (robert.Bernoulli(nf-n)) n++;
                    float totaly = C.StageVisibleY * 2;
                    float length = Mathf.Lerp(totaly, totaly * 0.24f, Mathf.Clamp01(progress * 1.4f));
                    // this.transform.position = new Vector3(0, C.StageVisibleY - length / 2, this.transform.position.z);
                    for (int i=0; i<n; i++) {
                        float x = robert.Float(-C.StageVisibleX, C.StageVisibleX);
                        int c = Mathf.FloorToInt(robert.Float() * 2);
                        C.ColorE16 color;
                        if (c == 0) color = C.ColorE16.Blue;
                        else color = C.ColorE16.Cyan;
                        Instantiate(bulletSpellcard11PrefabA).GetComponent<BulletLogic40>().SetParameters(
                            new Vector2(x, C.StageVisibleY), Vector2.down * 0.05f, 15, length, (uint)(color)
                        );
                    }
                }
                sTimer.Tick();
                if (timeoutTimer.GetElapsed() > 30 * C.SecondToFrame || health < totalHealth * 0.75f) {
                    G.I.EraseAllBullets(false);
                    sInt = 2; sTimer.Reset();
                }
                break;
            }
            case 2: {
                if (timeoutTimer.GetElapsed() < 55 * C.SecondToFrame) {
                    float progress = Mathf.Clamp01(sTimer.Progress(sc11Timeout / 4));
                    float nf = Mathf.Lerp(0.1f, 0.42f, U.CurveNSquare(progress));
                    int n = Mathf.FloorToInt(nf);
                    if (robert.Bernoulli(nf-n)) n++;
                    float totalx = C.StageVisibleX * 2;
                    float length = Mathf.Lerp(totalx, totalx * 0.6f, Mathf.Clamp01(progress * 1.6f));
                    bool oFromLeft = Mathf.FloorToInt((float)(sTimer.Get()) / (sc11Timeout / 4) * 15) % 2 == 0;
                    for (int i=0; i<n; i++) {
                        bool fromLeft = robert.Bernoulli(progress * 0.5f) ? oFromLeft : !oFromLeft;
                        float y = robert.Float(-C.StageVisibleY, C.StageVisibleY);
                        C.ColorE16 color = fromLeft ? C.ColorE16.Red : C.ColorE16.Yellow;
                        Vector2 pos = new Vector2(C.StageVisibleX * U.Sign(fromLeft), y);
                        Vector2 vel = (fromLeft ? Vector2.right : Vector2.left) * robert.Float(0.04f, 0.06f);
                        Instantiate(bulletSpellcard11PrefabA).GetComponent<BulletLogic40>().SetParameters(
                            pos, vel, 15, length, (uint)(color)
                        );
                    }
                }
                sTimer.Tick();
                if (timeoutTimer.GetElapsed() > 60 * C.SecondToFrame || health < totalHealth * 0.50f) {
                    G.I.EraseAllBullets(false);
                    sInt = 3; sTimer.Reset();
                }
                break;
            }
            case 3: {
                if (timeoutTimer.GetElapsed() < 85 * C.SecondToFrame) {
                    float progress = Mathf.Clamp01(sTimer.Progress(sc11Timeout / 4));
                    float nf = Mathf.Lerp(0.2f, 0.60f, U.CurveNSquare(progress));
                    int n = Mathf.FloorToInt(nf);
                    if (robert.Bernoulli(nf-n)) n++;
                    bool fromLeft = robert.Bernoulli(0.5f);
                    float remainingLength = Mathf.Lerp((C.StageVisibleY * 2 - C.StageVisibleX) * Mathf.Sqrt(2), 0, Mathf.Clamp01(progress * 1.6f));
                    for (int i=0; i<n; i++) {
                        float p = robert.Float(0, C.StageVisibleX * 4);
                        float length;
                        Vector2 pos;
                        Vector2 vel = new Vector2(-1, -1) / Mathf.Sqrt(2) * robert.Float(0.04f, 0.06f);
                        if (p > C.StageVisibleX * 2) {
                            float r = p - C.StageVisibleX * 2;
                            pos = new Vector2(C.StageVisibleX, C.StageMaxY - r);
                            length = p / Mathf.Sqrt(2) + remainingLength - r * Mathf.Sqrt(2);
                        } else {
                            pos = new Vector2(p - C.StageVisibleX, C.StageVisibleY);
                            length = p / Mathf.Sqrt(2) + remainingLength;
                        }
                        if (fromLeft) {pos = U.flipx(pos); vel = U.flipx(vel);}
                        Instantiate(bulletSpellcard11PrefabA).GetComponent<BulletLogic40>().SetParameters(
                            pos, vel, 8, length, (uint)(fromLeft ? C.ColorE16.Blue : C.ColorE16.Yellow), 2
                        );
                    }
                }
                sTimer.Tick();
                if (timeoutTimer.GetElapsed() > 90 * C.SecondToFrame || health < totalHealth * 0.25f) {
                    G.I.EraseAllBullets(false);
                    sInt = 4; sTimer.Reset();
                }
                break;
            }
            case 4: {
                if (sTimer.Check(0)) {
                    SetBodyCollider(false);
                    sFloat0 = 1e6f;
                }
                {
                    float progress = Mathf.Clamp01(sTimer.Progress(sc11Timeout / 4));
                    float dist = (G.playerPosition - this.transform.position.xy()).magnitude / C.StageVisibleY;
                    float nf = 0.4f + dist * 0.5f;
                    int n = Mathf.FloorToInt(nf);
                    if (robert.Bernoulli(nf-n)) n++;
                    float length = Mathf.Lerp(C.StageVisibleY * 2 * 0.9f, C.StageVisibleY * 2 * 0.05f, U.CurveSin(Mathf.Clamp01(progress * 1f)));
                    for (int i=0; i<n; i++) {
                        bool colorb = robert.Bool();
                        float r = robert.TwoPi();
                        if (dist < 0.6f) {
                            float k = (0.6f - dist) / 0.6f; k = U.CurveNSin(k);
                            r = robert.Float(-90 + k * 40, 270 - k * 40) * Mathf.Deg2Rad;
                        }
                        Instantiate(bulletSpellcard11PrefabA).GetComponent<BulletLogic40>().SetParameters(
                            this.transform.position, U.UnitByAngle(r) * 0.05f, 8, length, (uint)(colorb ? C.ColorE16.Blue : C.ColorE16.Cyan), 2, (float k) => {
                                sFloat0 = k;
                            }
                        );
                    }
                    Vector2 displacement = G.playerPosition - this.transform.position.xy();
                    if (displacement.magnitude > sFloat0) {
                        G.player.transform.position = this.transform.position + displacement.normalized.zed(0) * sFloat0;
                    }
                    // var p = sTimer.Get();
                    // float angle = (-30f + 1.8f * p) * Mathf.Deg2Rad;
                    // if (p % 6 == 0) Instantiate(bulletSpellcard11PrefabA).GetComponent<BulletLogic40>().SetParameters(
                    //     this.transform.position, U.UnitByAngle(angle) * 0.03f, 12, length, (uint)(C.ColorE16.Red), 2
                    // );
                    // angle = (-150f - 1.1f * p) * Mathf.Deg2Rad;
                    // if (p % 6 == 3) Instantiate(bulletSpellcard11PrefabA).GetComponent<BulletLogic40>().SetParameters(
                    //     this.transform.position, U.UnitByAngle(angle) * 0.02f, 12, length, (uint)(C.ColorE16.Cyan), 2
                    // );
                }
                sTimer.Tick();
                break;
            }
        }
        if (timeoutTimer.Tick()) finishSpellcard11(false);
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), sc11BonusU, sc11BonusL);
    }

    private void finishSpellcard11(bool healthDrained) {
        G.soundPlayer.Play("bossbreak");
        if (healthDrained) 
            spawnItems(40, 10, null, radius: 50);
        bonusFailed = bonusFailed || !healthDrained;
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc9BonusU, sc9BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        switchState(State.EpilogueIdle);
        healthCircle.Hide();
        healthCircleShown = false;
        G.stage.StartDialogue(3);
        G.I.playerControlEnabled = false;
        G.I.uiTimeout.GetComponent<UIVisibilityController>().Hide();
    }

    private void prepareEpilogueIdle() {
        sStarted = false;
    }

    private void updateEpilogueIdle() {
        if (!sStarted) return;
    }

    protected override void OnTriggerEnter2D(Collider2D other) {
        if (state == State.Spellcard11) {
            if (other.tag == "SlowBomb" || other.tag == "FastBomb") return;
        }
        var damage = Utils.GetEnterDamageToEnemy(other.tag);
        tryImposeDamage(damage);
    }

    protected override void OnTriggerStay2D(Collider2D other) {
        if (state == State.Spellcard11) {
            if (other.tag == "SlowBomb" || other.tag == "FastBomb") return;
        }
        var damage = Utils.GetStayDamageToEnemy(other.tag);
        tryImposeDamage(damage);
    }

}