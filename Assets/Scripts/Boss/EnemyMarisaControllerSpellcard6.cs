using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{

    [Header("Spellcard 6")]
    public GameObject bulletSpellcard6PrefabA;
    public GameObject bulletSpellcard6PrefabB;

    private const int sc6Health = 650000;
    private const uint sc6Timeout = 70 * C.SecondToFrame;
    private const uint sc6BonusU = 1500000, sc6BonusL = 500000;

    private static readonly int[] sc6Hakke0 = {1, 1, 0, 1, 0, 1, 0, 0};
    private static readonly int[] sc6Hakke1 = {0, 1, 0, 0, 1, 1, 1, 0};
    private static readonly int[] sc6Hakke2 = {1, 0, 1, 0, 0, 1, 1, 0};

    private void prepareSpellcard6() {
        G.soundPlayer.Play("spellcard");
        G.I.enemySpellcardCounter = TOTAL_STARS - 5;
        setupSpellcard(sc6Health, sc6Timeout, 0.25f, 0f);
        G.I.PlayEnemySpellcardAnimation(StaticStorage.GetLocaleString("Spellcard.6"));
        bonusFailed = false; 
        waitTimer.Reset(120); sStarted = false; setMoveToCenter(0);
        moveTimer.Reset(120);
    }

    private void updateSpellcard6() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60);
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset();
                SetAcceptDamage(true); 
                sTimer.Reset();
                sInt = robert.Int(8);
            }
        } else {
            const float posRatio = 30 * C.p2u;
            const float velRatio = 0.016f;
            const uint n = 21;
            const float l = 2f * 0.9238795325112867f; //1.5f * Mathf.Sin((180f-45f)/2*Mathf.Deg2Rad);
            sTimer.Tick();
            for (int k=0; k<n; k++) if (sTimer.Check(k*2 + 10)) for (int i=0; i<8; i++) {
                if (k==0 && i==0) {
                    G.soundPlayer.Play("laser0");
                    G.soundPlayer.SchedulePlay("bullet0", 120-sTimer.Get());
                }
                float p = (float)k/n; bool mid = Mathf.Abs(p-0.5f) <= 0.08f;
                Vector2 p0 = U.UnitByAngle((-22.5f + 45f * i) * Mathf.Deg2Rad);
                Vector2 p1 = U.UnitByAngle((22.5f + 45f * i) * Mathf.Deg2Rad);
                // inner circle
                Vector2 pos = Vector2.Lerp(p0*1f, p1*1f, p);
                if (!mid) Instantiate(bulletSpellcard6PrefabA).GetComponent<BulletLogic28>().SetParameters(
                    pos * posRatio, pos * velRatio, (uint)(C.ColorE16.Yellow), 120 - sTimer.Get(), 0
                );
                // outer circle
                pos = Vector2.Lerp(p1*3, p0*3, p);
                if (!mid) Instantiate(bulletSpellcard6PrefabA).GetComponent<BulletLogic28>().SetParameters(
                    pos * posRatio, pos * velRatio, (uint)(C.ColorE16.Yellow), 120 - sTimer.Get(), 0
                );
                // middle edge
                if (i%2==0) pos = Vector2.Lerp(p0*1f, p0*3, p);
                else pos = Vector2.Lerp(p0*3, p0*1f, p);
                Instantiate(bulletSpellcard6PrefabA).GetComponent<BulletLogic28>().SetParameters(
                    pos * posRatio, pos * velRatio, (uint)(C.ColorE16.Yellow), 120 - sTimer.Get(), 0
                );
                // hakke
                bool end = p<=0.1f || p>=0.9f;
                if (!end) {
                    int r = (i+sInt)%8;
                    float properTheta = 45f*i*Mathf.Deg2Rad;
                    Vector2 proper = U.UnitByAngle(properTheta);
                    Vector2 s0 = p0*1f + proper * l / 4; Vector2 s1 = p1*1f + proper * l / 4;
                    Vector2 hvel = U.UnitByAngle(properTheta + Mathf.PI/2) * (p-0.5f) * 0.2f * velRatio;
                    pos = Vector2.Lerp(s0, s1, p);
                    if (!mid || sc6Hakke0[r]==1) Instantiate(bulletSpellcard6PrefabB).GetComponent<BulletLogic28>().SetParameters(
                        pos * posRatio, pos * velRatio + hvel * 1.5f, (uint)(C.ColorE16.White), 120 - sTimer.Get(), properTheta
                    );
                    s0 = p0*1f + proper * l / 4 * 2; s1 = p1*1f + proper * l / 4 * 2;
                    pos = Vector2.Lerp(s1, s0, p);
                    if (!mid || sc6Hakke1[r]==1) Instantiate(bulletSpellcard6PrefabB).GetComponent<BulletLogic28>().SetParameters(
                        pos * posRatio, pos * velRatio - hvel * 2.5f, (uint)(C.ColorE16.Red), 120 - sTimer.Get(), properTheta
                    );
                    s0 = p0*1f + proper * l / 4 * 3; s1 = p1*1f + proper * l / 4 * 3;
                    pos = Vector2.Lerp(s0, s1, p);
                    if (!mid || sc6Hakke2[r]==1) Instantiate(bulletSpellcard6PrefabB).GetComponent<BulletLogic28>().SetParameters(
                        pos * posRatio, pos * velRatio + hvel * 3.5f, (uint)(C.ColorE16.White), 120 - sTimer.Get(), properTheta
                    );
                }
            }
            if (sTimer.Check(240)) {
                sTimer.Reset();
                sInt = (sInt + 1) % 8;
            }
        }
        if (timeoutTimer.Tick()) finishSpellcard6(false);
        else G.I.SetTimeoutDisplay(timeoutTimer.Get());
        updateBonusDisplay(timeoutTimer.Progress(), sc6BonusU, sc6BonusL);
    }

    private void finishSpellcard6(bool healthDrained) {
        if (healthDrained) 
            spawnItems(25, 10, null);
        bonusFailed = bonusFailed || !healthDrained;
        G.soundPlayer.Play(bonusFailed ? "scfailed" : "scbonus");
        uint bonus = bonusFailed ? 0 : (uint)(Mathf.CeilToInt(Mathf.Lerp(sc6BonusU, sc6BonusL, timeoutTimer.Progress())));
        G.I.Notify(G.Notification.BossSpellcardFinished, !bonusFailed, bonus, timeoutTimer.GetElapsed());
        G.I.EraseAllBullets(); 
        G.I.HideEnemySpellcardInfo();
        healthCircle.SetRatio(1); 
        switchState(State.Idle);
        G.stage.StartDialogue(2);
    }

}