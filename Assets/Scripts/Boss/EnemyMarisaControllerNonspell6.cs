using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;
using G = GameController;

public partial class EnemyMarisaController : EnemyAbstractLogic
{
    [Header("Nonspell 6")]
    public GameObject bulletNonspell6Prefab;

    private const int ns6Health = 650000;
    private const uint ns6Timeout = 45 * C.SecondToFrame;
    private const uint ns6MoveInterval = 120;

    void prepareNonspell6() {
        G.I.enemySpellcardCounter = TOTAL_STARS - 5;
        setupSpellcard(ns6Health, ns6Timeout, 1f, 0.25f, true, true);
        healthCircle.SetThresholdRatios(0.25f);
        setMoveToCenter(200); moveTimer.Reset(120); sStarted = false;
        waitTimer.Reset(150); sInt = 0;
    }

    private static readonly uint[] ns6Colors = {
        (uint)(C.ColorE16.Red),
        (uint)(C.ColorE16.Orange),
        (uint)(C.ColorE16.Yellow),
        (uint)(C.ColorE16.Green),
        (uint)(C.ColorE16.Cyan),
        (uint)(C.ColorE16.Blue),
        (uint)(C.ColorE16.Purple)
    };

    void updateNonspell6() {
        if (!sStarted) {
            moveTimer.Tick(); updateMove(60); 
            if (waitTimer.Tick()) {
                sStarted = true; sTimer.Reset();
                SetAcceptDamage(true); sTimer.Reset(); sAngle = robert.TwoPi();
                sFloat = 0;
            }
        } else {
            if (sTimer.TickCheckReset(3)) {
                int n = ns6Colors.Length;
                sFloat += 6e-3f;
                sInt++;
                if (sInt % 3 == 0) G.soundPlayer.Play("bullet0");
                for (int i=0; i<n; i++) {
                    float angle = sAngle + i*60f/n*Mathf.Deg2Rad;
                    Instantiate(bulletNonspell6Prefab).GetComponent<BulletLogic27>().SetParameters(
                        this.transform.position, U.UnitByAngle(angle + robert.FloatPM() * sFloat * Mathf.Deg2Rad), ns6Colors[i]
                    );
                    Instantiate(bulletNonspell6Prefab).GetComponent<BulletLogic27>().SetParameters(
                        this.transform.position, U.UnitByAngle(angle + robert.FloatPM() * sFloat * Mathf.Deg2Rad + Mathf.PI), ns6Colors[i]
                    );
                }
                sAngle -= (60f/n*1.05f) * Mathf.Deg2Rad;
            }
            if (moveTimer.TickReset(ns6MoveInterval)) setMove(100, 70, 70, 100, 240); updateMove(60);
        }
        if (timeoutTimer.Tick()) finishNonspell1(false); else G.I.SetTimeoutDisplay(timeoutTimer.Get());
    }

    void finishNonspell6(bool healthDrained) {
        G.I.EraseAllBullets();
        switchState(State.Spellcard6);
    }

}