using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using C = Constants;
using U = Utils;

public enum ItemType {
    Power = 0, Point = 1, BigPower = 2, SmallPoint = 3,
    Bomb = 4, Life = 5,
};

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    [Header("Prefabs")]
    public GameObject itemPrefab;
    public GameObject effectErasePrefab;
    public GameObject effectScorePrefab;
    public GameObject effectGrazePrefab;
    public GameObject effectBreak0Prefab;

    [Header("Stages, although I have only one now")]
    public GameObject stage0Prefab;

    [Header("GameObject references")]
    public UIScoreLogic uiScoreTextLogic;
    public UIBombLogic uiBombDisplayLogic;
    public UILifeLogic uiLifeDisplayLogic;
    public UIEnemySpellcardCounter uiEnemySpellcardCounter;
    public TMPro.TMP_Text uiGraze;
    public TMPro.TMP_Text uiTimeout;
    public UIDialogueSystemController dialogueSystem;
    public TMPro.TMP_Text uiEnemySpellcardBonus;
    public UILoadingLogic uiLoading;
    public UIBossIndicatorLogic uiBossIndicator;
    public TMPro.TMP_Text uiBossName;
    public GameObject uiPausePanel;
    public UIAliceFaces uiAliceFaces;
    public MusicPlayer musicPlayer;

    [Header("Subordinate GameControllers")]
    public GameconPlayerSpellcardAnimation gameconPlayerSpellcardAnim;
    public GameconEnemySpellcardAnimation gameconEnemySpellcardAnim;
    public GameconSpellcardCapturedAnimation gameconSpellcardCapturedAnim;

    #region Properties

    public delegate void DelegateNoArgument();
    public DelegateNoArgument delegateOnBombUsed;
    public DelegateNoArgument delegateOnMissed;

    private uint bombs_;
    public uint bombs {
        get {return bombs_;}
        set {bombs_ = value; uiBombDisplayLogic.SetBombs(value);}
    }

    private uint lives_;
    public uint lives {
        get {return lives_;}
        set {
            lives_ = value; uiLifeDisplayLogic.SetLives(value);
            uiAliceFaces.SetCount(value);
        }
    }

    private long score_;
    public long score {
        get {return score_;}
        set {score_ = value; uiScoreTextLogic.SetScore(value);}
    }

    private uint graze_;
    public uint graze {
        get {return graze_;}
        set {graze_ = value; if (uiGraze) uiGraze.text = value.ToString();}
    }

    public bool enemySpellcardCounterShown {
        get {return uiEnemySpellcardCounter.gameObject.activeSelf;}
        set {
            uiBossName.gameObject.SetActive(value);
            uiEnemySpellcardCounter.gameObject.SetActive(value);
        }
    }

    public uint enemySpellcardCounter {
        get {return uiEnemySpellcardCounter.currentCount;}
        set {
            enemySpellcardCounterShown = true;
            uiEnemySpellcardCounter.SetCount(value);
        }
    }

    public bool escapeEnabled {get; set;}

    public static GameController I {get {return Instance;}}
    private SRandom random_;
    public static SRandom random {get {return Instance?.random_;}}

    private PlayerController player_;
    public static PlayerController player {get {return Instance?.player_;}}
    public static Vector2 playerPosition { get {
        if (Instance != null) return Instance.player_.transform.position;
        Debug.LogWarning("Player does not exist."); return new Vector2(0, 0);
    }}

    private bool paused_;
    private List<GameObject> pausedObjects;
    public static bool paused {
        get {return Instance.paused_;}
        set {
            Instance.paused_ = value;
            if (value) {
                List<string> interested = new List<string>{
                    "Player", "GameController", "Bullets", "SlowShot", "FastShot",
                    "Effect", "Enemy", "Item", "FastBomb", "SlowBomb", "Eraser",
                };
                Instance.pausedObjects = new List<GameObject>();
                // int k = 0;
                foreach (var tag in interested) 
                    foreach (var g in GameObject.FindGameObjectsWithTag(tag)) {
                        // k++;
                        // Debug.LogWarning(string.Format("{0}: {1}", k, g.tag));
                        Instance.pausedObjects.Add(g);
                        g.SetActive(!value);
                    }
                Instance.stage_.gameObject.SetActive(false);
                Instance.gameObject.SetActive(true);
                Instance.uiPausePanel.SetActive(true);
            } else {
                // int k = 0;
                foreach (var g in Instance.pausedObjects) {
                    // k++;
                    if (g) g.SetActive(true);
                    else {
                        // Debug.LogWarning(string.Format("GameObject has been destroyed while pause: {0}", k));
                        // print(g);
                    }
                }
                Instance.stage_.gameObject.SetActive(true);
                Instance.uiPausePanel.SetActive(false);
            }
        }
    }

    public static UIBossIndicatorLogic bossIndicator {get {return Instance?.uiBossIndicator;}}

    public SoundPlayer soundPlayer_;
    public static SoundPlayer soundPlayer {get {return Instance.soundPlayer_;}}
    
    private StageAbstractLogic stage_;
    public static StageAbstractLogic stage {
        get {return Instance.stage_;}
    }

    private bool playerControlEnabled_;
    public bool playerControlEnabled {
        get {return playerControlEnabled_;}
        set {playerControlEnabled_ = value;}
    }

    #endregion

    // private Timer timer;
    private int spriteIndexAllocator;
    private SRandom robert;
    private bool inDialogue;
    private EnemyMarisaController marisa;

    #region GlobalGameRelatedLogic

    public int GetSpriteIndex() {
        return spriteIndexAllocator++;
    }

    public void EraseAllBullets(bool spawnItem = true) {
        // soundPlayer.Play("eraseall");
        var bullets = GameObject.FindGameObjectsWithTag("Bullets");
        foreach (var bullet in bullets) {
            var logic = bullet.GetComponent<BulletAbstractLogic>();
            if (logic) logic.Erase(spawnItem);
        }
    }

    public void StartDialogue(List<List<UIDialogueSystemController.DialogueSystemCommand>> dialogue) {
        dialogueSystem.Show(dialogue); inDialogue = true;
    }

    public enum Notification {
        // from boss
        BossLeft, BossSpellcardFinished,
        // from player
        PlayerUsedBomb, PlayerMissed, 
        // from bullet
        BulletGrazed,
        // from dialogue system
        DialogueDone, DialogueSignalled,
        // from item
        ItemPointCollected,
    }
    public void Notify(Notification n, params object[] args) {
        switch (n) {
            case Notification.BossLeft: {
                uiTimeout.GetComponent<UIVisibilityController>().Hide();
                stage_.NotifyBossLeft(); break;
            }
            case Notification.BossSpellcardFinished: {
                if (args.Length >= 3 && args[0] is bool captured && args[1] is uint score && args[2] is uint timeInFrames) {
                    gameconSpellcardCapturedAnim.Show(captured, score, timeInFrames);
                    if (captured) this.score += score;
                }
                else Debug.Log("Notify BossSpellcardCaptured need args as bool, uint, uint.");
                break;
            }
            case Notification.PlayerUsedBomb: 
                if (bombs > 0) bombs--; else Debug.LogError("Received BombUsed notification but there is no bomb.");
                delegateOnBombUsed?.Invoke(); 
                Debug.Log("Bomb used.");
                break;
            case Notification.PlayerMissed:
                // erase bullets
                EraseAllBullets(true);
                // cancel autocollecting items
                var items = GameObject.FindGameObjectsWithTag("Item");
                foreach (var item in items) {
                    var logic = item.GetComponent<ItemLogic>(); 
                    if (logic) logic.NotifyAutoCollect(false);
                    else Debug.LogWarning("Item GameObject does not have a ItemLogic");
                }
                // set bombs to 3
                if (lives == 0) {
                    paused = !paused;
                    uiPausePanel.GetComponent<UIPausePanelController>().setGameoverTexts();
                    StaticStorage.Instance.showScoreBoard = true;
                    StaticStorage.Instance.currentScore = score;
                    StaticStorage.Instance.hasCurrentScore = true;
                } else {
                    lives = lives-1;
                }
                delegateOnMissed?.Invoke();
                bombs = 3;
                Debug.Log("Life used.");
                break;
            case Notification.BulletGrazed: {
                graze++;
                for (int _=0; _<3; _++) InstantiateEffectGraze(player.transform.position);
                this.score += 1000;
                soundPlayer.Play("graze");
                break;
            }
            case Notification.DialogueDone: {
                inDialogue = false; stage_.NotifyDialogueDone();
                break;
            }
            case Notification.DialogueSignalled: {
                if (args.Length >= 1 && args[0] is int i) stage_.NotifyDialogueSignal(i);
                break;
            }
            case Notification.ItemPointCollected: {
                if (args.Length >= 1 && args[0] is int i) this.score += i; break;
            }
        }
    }

    public void PlayPlayerSpellcardTachie(bool slow) {
        gameconPlayerSpellcardAnim.Play(slow);
    }
    public void PlayEnemySpellcardAnimation(string spellcardName) {
        gameconEnemySpellcardAnim.Show(spellcardName);
    }
    public void HideEnemySpellcardInfo() {
        gameconEnemySpellcardAnim.HideImmediate();
    }
    public TMPro.TMP_Text GetEnemySpellcardBonusDisplay() {
        return uiEnemySpellcardBonus;
    }
    public bool IsInDialogue() {return inDialogue;}
    public void ShowTimeoutDisplay() {
        uiTimeout.GetComponent<UIVisibilityController>().Show();
    }
    public void HideTimeoutDisplay() {
        uiTimeout.GetComponent<UIVisibilityController>().Hide();
    }
    public void SetTimeoutDisplay(uint timeInFrames) {
        float c = timeInFrames * Constants.f2s; 
        if (c<0) c=0;
        uiTimeout.text = string.Format("{0:00.00}", c);
        if (c<=10) {
            if (timeInFrames % C.SecondToFrame == 0 && timeInFrames > 0) soundPlayer.Play("timeout");
            float r = c - Mathf.Floor(c);
            if (r > 0.8f) uiTimeout.GetComponent<RectTransform>().localScale = new Vector3(1+(r-0.8f)*0.5f, 1+(r-0.8f)*0.5f, 1);
            else uiTimeout.GetComponent<RectTransform>().localScale = Vector3.one;
            uiTimeout.colorGradient = new TMPro.VertexGradient(Color.white, Color.white, new Color(1, 0.5f, 0.5f, 1), new Color(1, 0.5f, 0.5f, 1));
        } else {
            uiTimeout.colorGradient = new TMPro.VertexGradient(Color.white, Color.white, new Color(1, 1f, 0.5f, 1), new Color(1, 1f, 0.5f, 1));
        }
    }

    public void ClearedReturnToTitle() {
        StopMusic();
        uiLoading.Restart(true);
        StartCoroutine(uiPausePanel.GetComponent<UIPausePanelController>().loadTitleScene());
    }

    public void Restart() {
        Debug.Log("Restart called.");
        paused = false;
        List<string> interested = new List<string>{
            "Bullets", "SlowShot", "FastShot",
            "Effect", "Enemy", "Item", "FastBomb", "SlowBomb", "Eraser"
        };
        foreach (var tag in interested) 
            foreach (var g in GameObject.FindGameObjectsWithTag(tag)) {
                Destroy(g);
            }
        gameconEnemySpellcardAnim.HideImmediate();
        Destroy(stage_.gameObject);
        stage_ = Instantiate(stage0Prefab).GetComponent<StageAbstractLogic>();
        score = 0; graze = 0; 
        lives = 2; bombs = 3;
    }

    public void PlayMusic(int index) {
        musicPlayer.Play(index);
    }

    public void StopMusic() {
        musicPlayer.Stop();
    }

    #endregion

    #region PublicInstantiations

    public Transform InstantiateItem(Vector2 position, ItemType type) {
        var obj = Instantiate(itemPrefab, Utils.VectorZ(position, Constants.DepthItem), Quaternion.identity);
        var logic = obj.GetComponent<ItemLogic>();
        logic.SetParameters(type);
        return obj.transform;
    }

    public void InstantiateEffectErase(Vector2 position, uint colorID, float baseScale = 1) {
        var obj = Instantiate(effectErasePrefab);
        var logic = obj.GetComponent<EffectEraseLogic>();
        logic.SetParameters(position, baseScale, colorID);
    }

    public void InstantiateEffectEraseGeneral(Vector2 position, GameObject display, uint color, float rotation) {
        var effect = new GameObject("EffectErase");
        Instantiate(display, effect.transform); 
        var logic = effect.AddComponent<EffectEraseLogic>();
        logic.SetParameters(position, 1, color, rotation);
    }

    public void InstantiateEffectScore(Vector2 position, uint score, Color color) {
        var obj = Instantiate(effectScorePrefab);
        var logic = obj.GetComponent<EffectScoreLogic>();
        logic.SetParameters(position, score, color);
    }

    public void InstantiateEffectBreak0(Vector2 position, uint color) {
        var effect = Instantiate(effectBreak0Prefab, Utils.VectorZ(position, Constants.DepthEffect), Quaternion.identity);
        effect.GetComponent<EffectBreakLogic0>().SetParameters(color);
    }

    public void InstantiateEffectGraze(Vector2 position) {
        Instantiate(effectGrazePrefab, Utils.VectorZ(position, Constants.DepthEffect), Quaternion.identity);
    }

    #endregion

    #region PrivateProcedures

    #endregion

    #region UnityPredefinedProcedures
    
    void Awake() {
        if (Instance != null) {
            Debug.LogError("More than one GameController instance.");
            return;
        }
        // CrossSceneStorage.TryInitialize();
        Instance = this;
        Dialogue.LoadDialogues();
        playerControlEnabled_ = true;
        spriteIndexAllocator = 0;
        score = 0; graze = 0; 
        random_ = new SRandom((int)(System.DateTime.Now.ToBinary()));
        paused_ = false;
        escapeEnabled = true;
        // timer = new Timer();
    }

    void Start() {
        stage_ = Instantiate(stage0Prefab).GetComponent<StageAbstractLogic>();
        player_ = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        uiLoading.Hide(); uiLoading.LoadOffsets();
        uiLoading.DisableAfter(120);
        lives = 2;
    }

    void Update() {
        if (escapeEnabled && Input.GetKeyDown(KeyCode.Escape)) {
            if (!paused) soundPlayer.Play("pause");
            else soundPlayer.Play("cancel");
            paused = !paused;
            uiPausePanel.GetComponent<UIPausePanelController>().setPauseTexts();
            Debug.LogFormat("Paused = {0}", paused);
        }
    }

    void FixedUpdate() {
        if (paused) return;
        if (inDialogue) {
            dialogueSystem.ProcessInput(Input.GetKey(KeyCode.Z), Input.GetKey(KeyCode.LeftControl));
        }
    }

    void OnDestroy() {
        if (Instance == this) Instance = null;
    }

    #endregion

}
