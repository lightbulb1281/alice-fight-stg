using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour
{

    public List<AudioClip> clips;
    public List<Vector2> repeatPositions;
    public int transitionFrames;

    private AudioSource source;
    private int playingIndex;
    private int nextIndex;

    private float relativeVolume;
    bool musicEnabled;

    private enum State {
        Idle, Normal, FadeIn, FadeOut
    }
    State state;

    void Awake() {
        bool bgm = PlayerPrefs.GetInt("BGM", 1) != 0;
        musicEnabled = bgm;
        source = this.GetComponent<AudioSource>();
        relativeVolume = 1;
        volume_ = 1;
        playingIndex = -1;
        state = State.Idle;
        if (clips.Count != repeatPositions.Count) {
            Debug.LogWarning("Clips cound does not equal to repeatPositions count.");
        }
    }

    private float volume_;
    public float volume {
        get {
            return volume_;
        }
        set {
            volume_ = value;
            source.volume = Mathf.Clamp01(volume_ * relativeVolume);
        }
    }

    public void Play(int index, bool force = false) {
        if (index < 0 || index >= clips.Count) {
            Debug.LogWarning("Invalid clip index."); return;
        }
        if (!force) if (index == playingIndex) return;
        if (state == State.Idle) {
            state = State.Normal; 
            source.clip = clips[index];
            playingIndex = index;
            if (musicEnabled) source.Play();
        } else {
            state = State.FadeOut;
            nextIndex = index;
        }
    }

    public void Stop() {
        if (state != State.Idle) {
            state = State.FadeOut;
            nextIndex = -1;
        }
    }

    void FixedUpdate() {
        switch (state) {
            case State.FadeOut:
                relativeVolume -= 1.0f/transitionFrames;
                if (relativeVolume <= 0) {
                    playingIndex = nextIndex;
                    if (nextIndex != -1) {
                        source.clip = clips[nextIndex];
                        state = State.FadeIn;
                        relativeVolume = 0;
                        if (musicEnabled) source.Play();
                        source.time = 0;
                    } else {
                        relativeVolume = 1;
                        state = State.Idle;
                        source.Stop();
                    }
                }
                source.volume = Mathf.Clamp01(volume * relativeVolume);
                break;
            case State.FadeIn:
                relativeVolume += 1.0f/transitionFrames;
                if (relativeVolume >= 1) {
                    relativeVolume = 1;
                    state = State.Normal;
                }
                source.volume = Mathf.Clamp01(volume * relativeVolume);
                break;
        }
        if (state != State.Idle) {
            if (source.time >= repeatPositions[playingIndex].y) {
                source.time = repeatPositions[playingIndex].x;
            }
        }
    }

    public void SetEnabled(bool b) {
        musicEnabled = b;
        if (source.isPlaying && !b) source.Stop();
        if (b) Play(playingIndex, true);
    }

}
