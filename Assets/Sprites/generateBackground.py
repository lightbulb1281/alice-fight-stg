import math
from PIL import Image, ImageDraw
import numpy as np
import colorsys

# a = Image.open("pic.jpg")
# a = np.asarray(a) / 255
# print(a.dtype, a.shape)

height = 60
width = 40
tries = 50
unit = 20
image = Image.new('RGB', (width * unit, height * unit), (0, 0, 0))
gdraw = ImageDraw.Draw(image)
margin = 2

shapes = [
    (5, 0.1),
    (4, 0.1),
    (3, 0.2),
    (2, 0.4),
]

m = np.zeros((height, width))

def check(s, y, x):
    for i in range(s):
        for j in range(s):
            if (m[(y+i)%height, (x+j)%width] != 0):
                return False
    return True

def draw(y, x, h, w, color):
    x0 = x * unit + margin
    x1 = (x+w) * unit - margin
    y0 = y * unit + margin
    y1 = (y+h) * unit - margin
    gdraw.rectangle([x0, y0, x1, y1], color)
    if (x+w>width and y+h>height):
        gdraw.rectangle([x0 - width*unit, y0 - height*unit, x1 - width*unit, y1 - height*unit], color)
    if (x+w>width):
        gdraw.rectangle([x0 - width*unit, y0, x1 - width*unit, y1], color)
    if (y+h>height):
        gdraw.rectangle([x0, y0 - height*unit, x1, y1 - height*unit], color)

def getcolor(s, y, x):
    ys = (y+s/2) 
    if ys > height: ys -= height
    ys = ys / height * 2
    ys = (ys - math.floor(ys)) * 2
    if ys > 1: ys = 2 - ys
    hue = (180 + ys * (220 - 180)) / 360 + (np.random.rand() * 2 - 1) * 0.05
    rgb = colorsys.hsv_to_rgb(hue, 0.4, 0.5)
    rgb = (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
    return rgb

def fill(s, y, x):
    for i in range(s):
        for j in range(s):
            m[(y+i)%height, (x+j)%width] = 1
    draw(y, x, s, s, getcolor(s, y, x))

for s, ratio in shapes:
    count = int(height * width * ratio / (s*s))
    for _ in range(count):
        for _ in range(tries):
            y = np.random.randint(0, height)
            x = np.random.randint(0, width)
            if check(s, y, x): 
                fill(s, y, x)
                break
for y in range(height):
    for x in range(width):
        if check(1, y, x): fill(1, y, x)

image.save("stage_background.png")
        
