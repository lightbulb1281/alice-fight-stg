# player bombs
BombSlow=咒符「上海人形」
BombFast=魔符「アーティフルサクリファイス」

# mid spellcards
MidSpellcard.1=星符「綻びる星座」
MidSpellcard.2=衝符「駆けだすホウキ」

# spellcards
Spellcard.1=墜符「隕石インパクト」
Spellcard.2=霊符「炎光封印」
Spellcard.3=示符「危ないハートビート」
Spellcard.4=涙符「ウンディネの贈り物」
Spellcard.5=引符「降着円盤」
Spellcard.6=卦符「エイシェントメイズ」
Spellcard.7=秩序「蓮華境内」
Spellcard.8=知恵「不老不死の呪い」
Spellcard.9=科学「光電効果」
Spellcard.10=「人形師の恋」
Spellcard.11=星寂「エントロピー終幕」

# dialogue 0
Dialogue0.0=魔理沙いるのかしら
Dialogue0.1=誰だと思ったら
Dialogue0.2=アリスだったか
Dialogue0.3=お邪魔します
Dialogue0.4=すまん、今家が散らかっているので
Dialogue0.5=ちょっと待っててもらいたいぜ
Dialogue0.6=なら手伝ってもかまわないよ
Dialogue0.7=でも片づけられるのは
Dialogue0.8=勝手に人んちに潜り込むやつだぜ
Dialogue0.9=潜り込むって、
Dialogue0.10=あんただけには言われたくないわ

# dialogue 1
Dialogue1.0=懲りないぜ、アリスは
Dialogue1.1=あんたこそ
Dialogue1.2=いつものように逃げ足が早いわ
Dialogue1.3=そりゃそうだぜ
Dialogue1.4=速さは泥棒の基本なんだから
Dialogue1.5=それ誇るもの？
Dialogue1.6=細かいこと気にしないでくれ
Dialogue1.7=そうね、今日来たのは
Dialogue1.8=ちゃんとした用事が－－
Dialogue1.9=さっき装備が揃えていない
Dialogue1.10=今ミニ八卦炉を手にした以上、
Dialogue1.11=もう負けはしないぜ！
Dialogue1.12=人の話を聞きなさいよ、もう！
Dialogue1.13=でもそこまで言うなら
Dialogue1.14=ちょっと遊んでやってもいいわ

# dialogue 2
Dialogue2.0=くっ、うっとうしいぜ
Dialogue2.1=降参してどうだ？
Dialogue2.2=そんなわけないぜ
Dialogue2.3=今から恋の魔法使いの力を見せてやる！

# dialogue 3
Dialogue3.0=ち、近いよ！
Dialogue3.1=あなたのスペルが迫ってくるからでしょ
Dialogue3.2=何、私を求めているの？
Dialogue3.3=そ、そんなわけないだろ…
Dialogue3.4=ただ、近すぎると、なんかドキドキするぜ
Dialogue3.5=かわいいわね
Dialogue3.6=頬が赤くなっちゃって
Dialogue3.7=そんな話はもういい！
Dialogue3.8=今日は参ったぜ
Dialogue3.9=ええ、いい運動になったわ
Dialogue3.10=で、用事は何？
Dialogue3.11=霊夢がさ、
Dialogue3.12=宴を行おうと言って誘いに来たの
Dialogue3.13=行けばいいんじゃない？
Dialogue3.14=誰かの寂しがり屋さんが仲間外れに
Dialogue3.15=なっちゃったら悪いかなと思って
Dialogue3.16=誰が寂しがり屋さんだよ
Dialogue3.17=スペルで人を近づかせる
Dialogue3.18=甘えん坊さんは誰かな、ふふ
Dialogue3.19=ぐぬぬ
Dialogue3.20=僕を誘うなら早く言えよ
Dialogue3.21=酷い目にあったぜ
Dialogue3.22=人の話を聞かず、
Dialogue3.23=いきなり攻撃に出すのは一体誰だよ
Dialogue3.24=知らない知らない、アリスが悪い！
Dialogue3.25=はいはい、お嬢様
Dialogue3.26=けがを負わせるのはわたくしの過ちでした
Dialogue3.27=宴会の時はたっぷり甘えますから
Dialogue3.28=一緒に来てくれませんか？
Dialogue3.29=そこまでいうなら、
Dialogue3.30=行ってもかまわないぜ
Dialogue3.31=ふふ、
Dialogue3.32=魔理沙はまだまだ子供だね
Dialogue3.33=からかわないでよ
Dialogue3.34=さあ早くホウキに乗って
Dialogue3.35=よろこんで
Dialogue3.36=行くぜ！

# practice names
PracticeNames.0=前半#0
PracticeNames.1=前半#1
PracticeNames.2=前半#2
PracticeNames.3=前半#3
PracticeNames.4=前半#4
PracticeNames.5=前半#5
PracticeNames.6=中ボス通1
PracticeNames.7=中ボススペル1
PracticeNames.8=中ボススペル2
PracticeNames.9=後半#6
PracticeNames.10=後半#7
PracticeNames.11=後半#8
PracticeNames.12=ボス通1
PracticeNames.13=ボススペル1
PracticeNames.14=ボス通2
PracticeNames.15=ボススペル2
PracticeNames.16=ボス通3
PracticeNames.17=ボススペル3
PracticeNames.18=ボス通4
PracticeNames.19=ボススペル4
PracticeNames.20=ボス通5
PracticeNames.21=ボススペル5
PracticeNames.22=ボス通6
PracticeNames.23=ボススペル6
PracticeNames.24=ボススペル7
PracticeNames.25=ボススペル8
PracticeNames.26=ボススペル9
PracticeNames.27=ボススペル10
PracticeNames.28=ボススペル11

# title UIs
# Title.Menu.Start=スタート
# Title.Menu.Practice=練習
# Title.Menu.Scoreboard=スコアボード
# Title.Menu.Quit=やめる
Title.Practice=練習モード
# Title.Scoreboard.Date=日付
# Title.Scoreboard.Score=スコア
Title.Options.Language=言語
Title.Options.SE=ＳＥ
Title.Options.BGM=ＢＧＭ

# Pause
Pause.Title1=一時停止
Pause.Continue1=続く
Pause.Restart1=リスタート
Pause.ReturnToTitle1=タイトルに戻る
Pause.Title2=満身創痍
Pause.Continue2=コンティニューする
Pause.Restart2=再挑戦
Pause.ReturnToTitle2=タイトルに戻る
